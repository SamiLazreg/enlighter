package enlighter;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import application.Alternatives;
import application.Application;
import application.DataSource;
import application.Path;
import application.Task;
import application.DataSource.Size;
import application.Element;
import application.Element.Type;
import deployment.Mapping;
import deployment.MappingAlgorithm;
import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Resource.EnergyMode;
import platform.Storage.Capacity;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;
import reasoning.Reasoning;
import reasoning.ToFPTA;
import reasoning.ToPML;
import reasoning.ToSXFM;
import reasoning.ToTVL;

class HelloWorld6 {

	@Test
	void test() {
		
		Application app = new Application("WarpwWhat");
		
		Path p1 = app.addPath("P1"); Path p2 = app.addPath("P2"); 
		
		DataSource d1 = (DataSource) app.addDataSource("D1").addSize("", 512, 0).connect("o", p1);
		DataSource d2 = (DataSource) app.addDataSource("D2").addSize("", 512, 0).connect("o", p2);
	
		Task tc = (Task) app.addTask("TC", "C").connect(p2, "i0").connect(p1, "i1");
		
		Component plt = new Component("SoC");
		
		Storage ram = plt.addStorage("RAM", Storage.Type.READ_AND_WRITE);
		ram.addCapacity("ME", 1024, 150);
		ram.setBytesPerCycle(4).setFrequencies(100, 200);
		
		Storage rom = plt.addStorage("ROM", Storage.Type.READ_ONLY);
		rom.addCapacity("ME", 1024, 150);
		rom.setBytesPerCycle(4).setFrequencies(100, 200);
		
		
		Component dcu = plt.addComponent("DCU");
		Component dcu_1 = dcu.addComponent("1");
		dcu_1.setCost(100).setFrequencies(100, 200);		
		
		Processor pcdcu = dcu_1.addProcessor("C", "C");
		pcdcu.setBytesPerCycle(4);
		pcdcu.connectToInputPort("i0", rom, ram);
		pcdcu.connectToInputPort("i1", rom, ram);
		
		
		MappingAlgorithm mapAlg = new MappingAlgorithm();
		Mapping mapping = mapAlg.map(app, plt);
	
		DesignSpace ds = new DesignSpace(app, mapping, plt);
		
		NonFunctionalCst nfr = 
				NonFunctionalCst.AND(
						/*NonFunctionalCst.QUALITY(">=", 1),
						NonFunctionalCst.COST("<", 200),*/
						NonFunctionalCst.RUN_TIME("<", 1800));
		
		FunctionCost cfct = new FunctionCost().QUALITY(0).COST(0).RUN_TIME(1);
		
		try {
			new Reasoning().reason("HelloWorld6", ds, nfr, cfct, true, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
