package enlighter;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;

import application.Alternatives;
import application.Application;
import application.DataSource;
import application.Path;
import application.Task;
import application.DataSource.Size;
import application.Element;
import application.Element.Type;
import deployment.Mapping;
import deployment.MappingAlgorithm;
import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Resource.EnergyMode;
import platform.Storage.Capacity;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;
import xpgenerator.FunctionalDomain;
import xpgenerator.GeneratedApplication;
import xpgenerator.GeneratedApplication.ApplicationParameters;
import xpgenerator.GeneratedApplication.ApplicationParameters.DataSizeParameter;
import xpgenerator.GeneratedPlatform;
import xpgenerator.GeneratedPlatform.PlatformParameters;
import xpgenerator.GeneratedPlatform.PlatformParameters.CapacityParameter;
import xpgenerator.Graph;
import xpgenerator.Graph.Statistic;
import reasoning.Reasoning;
import reasoning.ToFPTA;
import reasoning.ToPML;
import reasoning.ToSXFM;
import reasoning.ToTVL;

class HelloWorldGen {

	private static void _getNbResource(int nbNodes, PlatformParameters plt_param) {
		
		/*if(nbNodes <= 20) {
			plt_param.sNbIntStorages = 2;
			plt_param.sNbExtStorages = 1;
			plt_param.sNbProcessors = 2;
			plt_param.sNbCores = 2;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 12;
			plt_param.sPipelineDepth = 6;
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sExternalCap = 4096;
			plt_param.nbIntRom = 1;
			plt_param.nbIntRam = 1;
			plt_param.nbExtRom = 1;
			plt_param.sNbBytesPerCycle = 2;
			plt_param.sNbLatency = 2;
			plt_param.mainFreq = 200;
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);*/
			
			//low end high, mid end low
		
		if(nbNodes <= 8) {
			plt_param.sNbIntStorages = 1;
			plt_param.sNbExtStorages = 0;
			plt_param.sNbProcessors = 1;
			plt_param.sNbCores = 1;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 24;
			plt_param.sPipelineDepth = 10;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 200)));
			
			plt_param.costProc = 400;
			
			plt_param.sExternalCap = 4096;
			plt_param.nbIntRom = 0;
			plt_param.nbIntRam = 1;
			plt_param.nbExtRom = 0;
			plt_param.sNbBytesPerCycle = 2;
			plt_param.sNbLatency = 2;
			plt_param.mainFreq = 200;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			
		} else if (nbNodes <= 12) {
			
			plt_param.sNbIntStorages = 2;
			plt_param.sNbExtStorages = 0;
			plt_param.sNbProcessors = 1;
			plt_param.sNbCores = 1;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 36;
			plt_param.sPipelineDepth = 12;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 200)));
			
			plt_param.costProc = 400;
			
			plt_param.sExternalCap = 4096;
			plt_param.nbIntRom = 1;
			plt_param.nbIntRam = 1;
			plt_param.nbExtRom = 0;
			plt_param.sNbBytesPerCycle = 2;
			plt_param.sNbLatency = 2;
			plt_param.mainFreq = 200;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			
		} else if (nbNodes <= 16) {
			
			plt_param.sNbIntStorages = 2;
			plt_param.sNbExtStorages = 0;
			plt_param.sNbProcessors = 1;
			plt_param.sNbCores = 2;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 48;
			plt_param.sPipelineDepth = 10;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 200)));
			
			plt_param.costProc = 400;
			
			plt_param.sExternalCap = 4096;
			plt_param.nbIntRom = 0;
			plt_param.nbIntRam = 1;
			plt_param.nbExtRom = 1;
			plt_param.sNbBytesPerCycle = 2;
			plt_param.sNbLatency = 2;
			plt_param.mainFreq = 200;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			
		} else if (nbNodes <= 20) {
			
			plt_param.sNbIntStorages = 2;
			plt_param.sNbExtStorages = 1;
			plt_param.sNbProcessors = 2;
			plt_param.sNbCores = 2;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 60;
			plt_param.sPipelineDepth = 10;
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sExternalCap = 4096;
			plt_param.nbIntRom = 1;
			plt_param.nbIntRam = 1;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(512, new CapacityParameter(512, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 200)));
			
			plt_param.costProc = 400;
			
			plt_param.nbExtRom = 1;
			plt_param.sNbBytesPerCycle = 2;
			plt_param.sNbLatency = 2;
			plt_param.mainFreq = 200;
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			
			//low end high, mid end low
		
		} else if (nbNodes <= 40) {
			plt_param.sNbIntStorages = 2;
			plt_param.sNbExtStorages = 2;
			
			plt_param.sNbProcessors = 4;
			plt_param.sNbCores = 2;
			plt_param.sNbCoreCache = 0;
			plt_param.sNbTaskDomain = 120;
			plt_param.sPipelineDepth = 10;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(1024, new CapacityParameter(1024, 50)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(4096, new CapacityParameter(4096, 200)));
			
			plt_param.costProc = 400;
			
			plt_param.sExternalCap = 8192;
			
			plt_param.nbIntRom = 1;
			plt_param.nbIntRam = 1;
			plt_param.nbExtRom = 1;
			plt_param.nbExtRam = 1;
			
			plt_param.sNbBytesPerCycle = 4;
			plt_param.sNbLatency = 4;
			plt_param.mainFreq = 400;
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			//low end high, mid end mid
		
		/* close architectures difference in terms of size and nb resources
		 * task domain similar Fixed Function graphics rendering GL/VG1.1*/	
			
		} else if (nbNodes <= 80) {
			plt_param.sNbIntStorages = 4;
			plt_param.sNbExtStorages = 2;
			
			plt_param.nbIntRom = 2;
			plt_param.nbIntRam = 2;
			plt_param.nbExtRom = 1;
			plt_param.nbExtRam = 1;
			
			
			plt_param.sNbProcessors = 4;
			plt_param.sNbCores = 4;
			plt_param.sNbCoreCache = 1;
			plt_param.sNbTaskDomain = 240;
			plt_param.sPipelineDepth = 24;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(2048, new CapacityParameter(2048, 100)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(4096, new CapacityParameter(4096, 200)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(8192, new CapacityParameter(8192, 400)));
			
			plt_param.costProc = 600;
			
			
			plt_param.sExternalCap = 16384;
			
			plt_param.sNbLatency = 8;
			plt_param.sNbBytesPerCycle = 8;
			plt_param.mainFreq = 800;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			
			//mid end high, high end low
			
		/* new cheap architectures but similar in storages
		 * task domain programmable Function graphics rendering GL/VG2*/	
			
		} else if (nbNodes <= 160) {
			plt_param.sNbIntStorages = 4;
			plt_param.sNbExtStorages = 2;
			
			plt_param.nbIntRom = 2;
			plt_param.nbIntRam = 2;
			plt_param.nbExtRom = 1;
			plt_param.nbExtRam = 1;
			
			plt_param.costProc = 1000;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(4096, new CapacityParameter(4096, 200)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(8192, new CapacityParameter(8192, 400)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(16384, new CapacityParameter(16384, 800)));
			
			plt_param.sExternalCap = 32768;
			
			
			plt_param.sNbProcessors = 8;
			plt_param.sNbCores = 8;
			plt_param.sNbCoreCache = 2;
			plt_param.sNbTaskDomain = 480;
			plt_param.sPipelineDepth = 32;
			plt_param.sNbLatency = 16;
			plt_param.sNbBytesPerCycle = 16;
			plt_param.mainFreq = 1600;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			plt_param.sFreqs.add(plt_param.mainFreq/4);
			//high end mid
			
		/* new cheap architectures but similar in storages
		 * task domain similar between Fixed and programmable Function graphics rendering GL/VG3*/	
			
		} else if (nbNodes <= 320) {
			plt_param.sNbIntStorages = 6;
			plt_param.sNbExtStorages = 2;
			
			plt_param.nbIntRom = 2;
			plt_param.nbIntRam = 4;
			plt_param.nbExtRom = 2;
			
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(8192, new CapacityParameter(8192, 400)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(16384, new CapacityParameter(16384, 800)));
			plt_param.sInternalCaps.add(new AbstractMap.SimpleEntry<Integer, CapacityParameter>(32768, new CapacityParameter(32768, 1600)));
			
			plt_param.sExternalCap = 65536;
			
			plt_param.costProc = 1400;
			
			
			plt_param.sNbProcessors = 8;
			plt_param.sNbCores = 16;
			plt_param.sNbCoreCache = 4;
			plt_param.sNbTaskDomain = 128;
			plt_param.sPipelineDepth = 48;
			plt_param.sNbLatency = 16;
			plt_param.sNbBytesPerCycle = 16;
			plt_param.mainFreq = 1600;
			
			plt_param.sFreqs.add(plt_param.mainFreq);
			plt_param.sFreqs.add(plt_param.mainFreq/2);
			plt_param.sFreqs.add(plt_param.mainFreq/4);
			
			assert(false);
			//high end high*
			
		/* newest high end cheap architecture and memory system
		 * ultra rich task domain with new graphics functions */
			
		}
	}

	public static void _genDataSizes(ApplicationParameters p) {
		
		int nbNodes = (int) p.sNbNodes;
		assert(nbNodes > 0);
		
		if(nbNodes <= 20) {
			
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(256, new DataSizeParameter(256, 0.7f, 2)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(512, new DataSizeParameter(512, 0.9f, 1)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(1024, new DataSizeParameter(1024, 1.f, 0)));
			//p.sNbSize = 1.5f;
			p.sNbSize = 1.f;
			
			//low end high, mid end low
		
			//lot of small images, few majors images, one background
			
		} else if (nbNodes <= 40) {
			
			//low end high, mid end mid
		
			
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(256, new DataSizeParameter(256, 0.3f, 3)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(512, new DataSizeParameter(512, 0.6f, 2)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(1024, new DataSizeParameter(1024, 0.85f, 1)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(2048, new DataSizeParameter(2048, 1.f, 0)));
			p.sNbSize = 1.5f;
			//lot of small and medium images, few backgrounds
			
		} else if (nbNodes <= 80) {
			
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(256, new DataSizeParameter(256, 0.2f, 3)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(512, new DataSizeParameter(512, 0.5f, 2)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(1024, new DataSizeParameter(1024, 0.80f, 1)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(2048, new DataSizeParameter(2048, 1.f, 0)));
			p.sNbSize = 1f;
			//mid end high, high end low
		
		
			//lot of small and medium images, few backgrounds (with highest probability of bigger sizes)
			
		} else if (nbNodes <= 160) {
		
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(512, new DataSizeParameter(512, 0.4f, 4)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(1024, new DataSizeParameter(1024, 0.7f, 2)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(2048, new DataSizeParameter(2048, .85f, 1)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(4096, new DataSizeParameter(4096, 1.f, 0)));
			p.sNbSize = 1;
			//high end mid
		
		
			//lot of medium images, few HQ images
			
			
		} else if (nbNodes <= 320) {
			
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(512, new DataSizeParameter(512, 0.25f, 8)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(1024, new DataSizeParameter(1024, 0.5f, 4)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(2048, new DataSizeParameter(2048, .7f, 2)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(4096, new DataSizeParameter(4096, .85f, 1)));
			p.sSizes.add(new AbstractMap.SimpleEntry<Integer, DataSizeParameter>(8192, new DataSizeParameter(8192, 1.f, 0)));
			p.sNbSize = 0.5f;
			
			assert(false);
			//high end high*
		
			
			//lot of medium and big images, HQ+ images with highest probabilities
			
		} else
			assert(false);
	}

	@Test
	void test() throws IOException, ClassNotFoundException {
		
		int nbNodes = 8;
		int oldCx1 = 2; int newCx1 = 0;
		int oldCx2 = 16; int newCx2 = 0;
		int oldCx3 = 2; int newCx3 = 0;
		int oldCx4 = 16; int newCx4 = 0;
		
		do {
			
			ApplicationParameters p0 = new ApplicationParameters();
			p0.sNbNodes = nbNodes;
			p0.sNbInputs = 1.8f;
			p0.sNbOutputs = 0.9f;
			p0.pVariableFlow = -.1f;
			p0.sNodeQuality = 1.f;
			p0.pMappingRate = .4f;
			p0.pAltSize = .01f;
			_genDataSizes(p0);
			
			ApplicationParameters p1 = new ApplicationParameters();
			p1.sNbNodes = nbNodes;
			//p1.sNbInputs = 1.3f;
			//p1.sNbOutputs = .8f;
			p1.pVariableFlow = .0f;
			p1.sNodeQuality = 1.f;
			p1.pMappingRate = .4f;
			p1.pAltSize = .8f;
			_genDataSizes(p1);
			
			
			ApplicationParameters p2 = new ApplicationParameters();
			p2.sNbNodes = nbNodes;
			//p2.sNbInputs = 2.1f;
			//p2.sNbOutputs = 1.6f;
			p2.pVariableFlow = 1.f;
			p2.sNodeQuality = 1.f;
			p2.pMappingRate = .4f;
			p2.pAltSize = .01f;
			_genDataSizes(p2);
			
			ApplicationParameters p3 = new ApplicationParameters();
			p3.sNbNodes = nbNodes;
			//p2.sNbInputs = 2.1f;
			//p2.sNbOutputs = 1.6f;
			p3.pVariableFlow = 1.f;
			p3.sNodeQuality = 1.f;
			p3.pMappingRate = .4f;
			p3.pAltSize = .8f;
			_genDataSizes(p3);
			
				
			Statistic stat0 = new Statistic();
			stat0.nbNodes = (int) nbNodes;
			stat0.avgChild = 0.0f;
			stat0.avgJoin = 0.0f;
			stat0.avgLeaf = 0.0f;
			//stat.avgLeaf = 0.1f; 21/08/18
			stat0.avgMultiChild = 0.0f;
			stat0.avgMultiParents = 0.0f;
			stat0.avgOrphan = 0.0f;
			//stat.avgOrphan = 0.1f; 21/08/18
			stat0.avgParents = 0.0f;
			stat0.avgSplit = 0.0f;
			stat0.avgVariableTask = 0.0f;
			
			Statistic stat1 = new Statistic();
			stat1.nbNodes = (int) nbNodes;
			stat1.avgChild = 0.0f;
			stat1.avgJoin = 0.00f;
			stat1.avgLeaf = 0.0f;
			//stat.avgLeaf = 0.1f; 21/08/18
			stat1.avgMultiChild = 0.0f;
			stat1.avgMultiParents = 0.0f;
			stat1.avgOrphan = 0.0f;
			//stat.avgOrphan = 0.1f; 21/08/18
			stat1.avgParents = 0.0f;
			stat1.avgSplit = 0.00f;
			stat1.avgVariableTask = 0.0f;
			
			Statistic stat2 = new Statistic();
			stat2.nbNodes = (int) nbNodes;
			stat2.avgChild = 0.0f;
			stat2.avgJoin = 0.00f;
			stat2.avgLeaf = 0.f;
			//stat.avgLeaf = 0.1f; 21/08/18
			stat2.avgMultiChild = 0.0f;
			stat2.avgMultiParents = 0.0f;
			stat2.avgOrphan = 0.f;
			//stat.avgOrphan = 0.1f; 21/08/18
			stat2.avgParents = 0.f;
			stat2.avgSplit = 0.00f;
			stat2.avgVariableTask = 0.75f;
			
			Statistic stat3 = new Statistic();
			stat3.nbNodes = (int) nbNodes;
			stat3.avgChild = 0.0f;
			stat3.avgJoin = 0.00f;
			stat3.avgLeaf = 0.f;
			//stat.avgLeaf = 0.1f; 21/08/18
			stat3.avgMultiChild = 0.0f;
			stat3.avgMultiParents = 0.0f;
			stat3.avgOrphan = 0.f;
			//stat.avgOrphan = 0.1f; 21/08/18
			stat3.avgParents = 0.f;
			stat3.avgSplit = 0.00f;
			stat3.avgVariableTask = 0.75f;
		
			PlatformParameters plt_param0 = new PlatformParameters();
			plt_param0.sAltCapacities = .001f;
			plt_param0.pOptResources = -.1f;
			plt_param0.pAltFreq = .001f;
			_getNbResource((int) nbNodes, plt_param0);
	
			PlatformParameters plt_param1 = new PlatformParameters();
			plt_param1.sAltCapacities = 0.8f;
			plt_param1.pOptResources = 0.0f;
			plt_param1.pAltFreq = 0.001f;
			_getNbResource((int) nbNodes, plt_param1);
	
			PlatformParameters plt_param2 = new PlatformParameters();
			plt_param2.sAltCapacities = 0.001f;
			plt_param2.pOptResources = 0.f;
			plt_param2.pAltFreq = 0.001f;
			_getNbResource((int) nbNodes, plt_param2);
			
			PlatformParameters plt_param3 = new PlatformParameters();
			plt_param3.sAltCapacities = 0.8f;
			plt_param3.pOptResources = 0.8f;
			plt_param3.pAltFreq = 0.001f;
			_getNbResource((int) nbNodes, plt_param3);
			
			FunctionalDomain domain = new FunctionalDomain(plt_param0.sNbTaskDomain);
				
			GeneratedApplication app0 = new GeneratedApplication (domain, p0, stat0);
			GeneratedApplication app1 = new GeneratedApplication(domain, p1, stat1, app0.save);
			GeneratedApplication app2 = new GeneratedApplication(domain, p2, stat2, app0.save);
			GeneratedApplication app3 = new GeneratedApplication(domain, p3, stat3, app0.save);
			
			
			GeneratedPlatform plt0 = new GeneratedPlatform(domain, p0, plt_param0, app0.stats);
			GeneratedPlatform plt1 = new GeneratedPlatform(domain, p1, plt_param1, app1.stats);
			GeneratedPlatform plt2 = new GeneratedPlatform(domain, p2, plt_param2, app2.stats);
			GeneratedPlatform plt3 = new GeneratedPlatform(domain, p3, plt_param1, app3.stats);
			
					
			Mapping mapping0 = new MappingAlgorithm().map(app0.app, plt0.soc);
			Mapping mapping1 = new MappingAlgorithm().map(app1.app, plt1.soc);
			Mapping mapping2 = new MappingAlgorithm().map(app2.app, plt2.soc);
			Mapping mapping3 = new MappingAlgorithm().map(app3.app, plt3.soc);
			
			System.out.println("***************************************************");
			System.out.println("******************** MAPPING **********************");
			System.out.println("***************************************************");
			System.out.println("********************** DONE ************************");
			System.out.println("***************************************************");
			System.out.println("***************************************************");
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			DesignSpace ds0 = new DesignSpace(app0.app, mapping0, plt0.soc);
			DesignSpace ds1 = new DesignSpace(app1.app, mapping1, plt1.soc);
			DesignSpace ds2 = new DesignSpace(app2.app, mapping2, plt2.soc);
			DesignSpace ds3 = new DesignSpace(app3.app, mapping3, plt3.soc);
			
			
			NonFunctionalCst nfRequirements = 
					NonFunctionalCst.AND(
							NonFunctionalCst.QUALITY_LOSS("<=", 5),
							NonFunctionalCst.COST("<=", 200),
							NonFunctionalCst.RUN_TIME("<", nbNodes*(120*100)));
			
			FunctionCost cfct = new FunctionCost().QUALITY(100).COST(10).RUN_TIME(1);
			
			try {
				new Reasoning().reason("HelloWorldGenMapping"+(nbNodes+8), ds0, nfRequirements, cfct);
				new Reasoning().reason("HelloWorldGenNode"+(nbNodes+8), ds1, nfRequirements, cfct);
				new Reasoning().reason("HelloWorldGenFlow"+(nbNodes+8), ds2, nfRequirements, cfct);
				new Reasoning().reason("HelloWorldGenAll"+(nbNodes+8), ds3, nfRequirements, cfct);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			newCx1 = (int) ds0.nbDesigns;
			newCx2 = (int) ds1.nbDesigns;
			newCx3 = (int) ds2.nbDesigns;
			newCx4 = (int) ds3.nbDesigns;
			
			if(newCx1 > oldCx1 && oldCx1 * 10 > newCx1 && 
					newCx2 > oldCx2 && oldCx2 * 10 > newCx2 &&
					newCx3 > oldCx3 && oldCx3 * 10 > newCx3 &&
					newCx4 > oldCx4 && oldCx4 * 10 > newCx4) {
				++nbNodes;
				oldCx1 = newCx1;
				oldCx2 = newCx2;
				oldCx3 = newCx3;
				oldCx4 = newCx4;
			}
				
			
			
			
		} while(true);
	}
}
