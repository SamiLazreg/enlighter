package enlighter;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import application.Alternatives;
import application.Application;
import application.DataSource;
import application.Path;
import application.Task;
import application.DataSource.Size;
import application.Element;
import application.Element.Type;
import deployment.Mapping;
import deployment.MappingAlgorithm;
import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Resource.EnergyMode;
import platform.Storage.Capacity;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;
import reasoning.Reasoning;
import reasoning.ToFPTA;
import reasoning.ToPML;
import reasoning.ToSXFM;
import reasoning.ToTVL;

class HelloWorld3 {

	@Test
	void test() {
		
		Application app = new Application("WarpwWhat");
		
		Path p2 = app.addPath("P2"); 
		Path p3 = app.addPath("P3"); Path p4 = app.addPath("P4");
		
		DataSource d1 = (DataSource) app.addDataSource("D1").addSize("", 512, 0).connect("o", p2);
		
		DataSource d2 = app.addDataSource("D2").addSize("LD", 256, 0).addSize("MD", 512, 1).addSize("HD", 1024, 3);
		d2.connect("o", p4);
		Task tc = (Task) app.addTask("TC", "C").connect(p2, "i0").connect(p4, "i1");
		
		
		/*assertEquals(p1.inputs.size(), 1);
		assertEquals(p1.outputs.size(), 2);
		assertEquals(p2.inputs.size(), 2);
		assertEquals(p2.outputs.size(), 1);
		assertEquals(p3.inputs.size(), 1);
		assertEquals(p3.outputs.size(), 1);
		assertEquals(p4.inputs.size(), 1);
		assertEquals(p4.outputs.size(), 1);

		assertEquals(d1.outputs.size(), 1);
		assertEquals(d1.inputs.size(), 0);
		assertEquals(ta.inputs.size(), 1);
		assertEquals(ta.outputs.size(), 1);
		assertEquals(tb.inputs.size(), 1);
		assertEquals(tb.outputs.size(), 1);
		assertEquals(d2.outputs.size(), 1);
		assertEquals(d2.inputs.size(), 0);
		assertEquals(td.inputs.size(), 1);
		assertEquals(td.outputs.size(), 1);
		assertEquals(tc.inputs.size(), 2);
		assertEquals(tc.outputs.size(), 0);*/
		
		Component plt = new Component("SoC");
		Storage rom = plt.addStorage("ROM", Storage.Type.READ_ONLY).setAccessLatency(8).addCapacity("LE", 4096, 0);
		rom.setCost(75).setBytesPerCycle(1).setEnergyMode(2,4).setFrequencies(100, 200);
		
		Storage ram = plt.addStorage("RAM", Storage.Type.READ_AND_WRITE);
		ram.addCapacity("HE", 2048, 275).setAccessLatency(4);
		ram.setBytesPerCycle(2).setFrequencies(100, 200).setEnergyMode(10, 15).setOptional();
		
		/*Component gpu = plt.addComponent("GPU");
		Component gpu_1 = gpu.addComponent("1");
		Component gpu_2 = gpu.addComponent("2");
		gpu.setCost(100).setOptional().setEnergyMode(10, 15).setFrequencies(100, 200);		
		
		Processor pa0gpu = gpu_1.addProcessor("A", "A");
		Memory r0gpu = gpu_1.addFIFOBuffer("R0");
		pa0gpu.connectToIntputPort("i", rom, ram).connectToOutputPort("o", r0gpu, ram).setCyclesPerByte(1);
		Processor pbgpu = gpu_1.addProcessor("B", "B");
		pbgpu.connectToIntputPort("i", r0gpu, ram, rom).connectToOutputPort("o", ram).setCyclesPerByte(2);
		
		Processor pdgpu = gpu_2.addProcessor("D", "D");
		pdgpu.connectToIntputPort("i", ram, rom).connectToOutputPort("o", ram).setCyclesPerByte(1);
		*/
		
		Component dcu = plt.addComponent("DCU");
		dcu.setCost(50).setEnergyMode(5, 5).setFrequencies(100);
		
		/*Processor padcu = dcu.addProcessor("A", "A");
		Memory r0dcu = dcu.addFIFOBuffer("R0");
		padcu.connectToIntputPort("i", ram, rom).connectToOutputPort("o", r0dcu).setCyclesPerByte(4);
		Processor pddcu = dcu.addProcessor("D", "D");
		Memory r1dcu = dcu.addFIFOBuffer("R1");
		pddcu.connectToIntputPort("i", ram, rom).connectToOutputPort("o", r1dcu).setCyclesPerByte(4);
		*/
		Processor pc = dcu.addProcessor("C", "C");
		pc.connectToInputPort("i0", rom, ram).connectToInputPort("i1", rom, ram).setBytesPerCycle(4);
		
		/*
		assertEquals(ram.consumers.size(),7);
		assertEquals(ram.producers.size(),3);
		assertEquals(rom.consumers.size(),7);
		assertEquals(r0gpu.consumers.size(),1);
		assertEquals(r0dcu.consumers.size(),1);
		assertEquals(r1dcu.consumers.size(),1);
		assertEquals(r0gpu.producers.size(),1);
		assertEquals(r0dcu.producers.size(),1);
		assertEquals(r1dcu.producers.size(),1);*/
		
		MappingAlgorithm mapAlg = new MappingAlgorithm();
		Mapping mapping = mapAlg.map(app, plt);
		
		/*assertEquals(mapping.pms.get(p1).choices.size(),2);
		assertEquals(mapping.tms.get(ta).choices.size(),2);
		assertEquals(mapping.tms.get(tb).choices.size(),1);
		assertEquals(mapping.pms.get(p3).choices.size(),2);
		assertEquals(mapping.tms.get(td).choices.size(),2);
		assertEquals(mapping.pms.get(p2).choices.size(),2);
		assertEquals(mapping.tms.get(tc).choices.size(),1);
		assertEquals(mapping.pms.get(p4).choices.size(),2);*/
		
		DesignSpace ds = new DesignSpace(app, mapping, plt);
		VariabilityTree tree = new VariabilityTree(ds);
		
		NonFunctionalCst nfr = 
				NonFunctionalCst.AND(
						NonFunctionalCst.QUALITY(">=", 0),
						NonFunctionalCst.COST("<", 300),
						NonFunctionalCst.RUN_TIME("<", 600)
				);
		
		FunctionCost cfct = new FunctionCost().QUALITY(0).COST(0).RUN_TIME(1);
		
		try {
			new Reasoning().reason("HelloWorld3", ds, nfr, cfct, true, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
