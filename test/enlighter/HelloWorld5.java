package enlighter;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;

import application.Alternatives;
import application.Application;
import application.DataSource;
import application.Path;
import application.Task;
import application.DataSource.Size;
import application.Element;
import application.Element.Type;
import deployment.Mapping;
import deployment.MappingAlgorithm;
import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Resource.EnergyMode;
import platform.Storage.Capacity;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;
import reasoning.Reasoning;
import reasoning.ToFPTA;
import reasoning.ToPML;
import reasoning.ToSXFM;
import reasoning.ToTVL;

class HelloWorld5 {

	@Test
	void test() {
		
		Application app = new Application("WarpwWhat");
		
		Path p1 = app.addPath("P1"); Path p2 = app.addPath("P2"); 
		
		DataSource d1 = (DataSource) app.addDataSource("D1").addSize("", 512, 0).connect("o", p1);
		Task ta = (Task) app.addTask("TA", "A").connect(p1, "i").connect("o", p2);
		Task tb = (Task) app.addTask("TB", "B").connect(p1, "i").connect("o", p2);
		tb.setQuality(2);
	
		Task tc = (Task) app.addTask("TC", "C").connect(p2, "i");
		
		app.split(p1, ta, ta).join(p2, ta, ta);
		app.split(p1, tb, tb).join(p2, tb, tb);
		
		
		Component plt = new Component("SoC");
		
		Storage ram = plt.addStorage("RAM", Storage.Type.READ_AND_WRITE);
		ram.addCapacity("LE", 512, 100).addCapacity("ME", 1024, 150).addCapacity("HE", 2048, 275);
		ram.setBytesPerCycle(8).setFrequencies(100, 200).setEnergyMode(10, 15).setOptional();
		
		Component gpu = plt.addComponent("GPU");
		Component gpu_1 = gpu.addComponent("1");
		gpu.setCost(100).setOptional().setEnergyMode(10, 15).setFrequencies(100, 200);		
		
		Processor pa0gpu = gpu_1.addProcessor("A", "A");
		Memory r0gpu = gpu_1.addFIFOBuffer("R0");
		Memory r1gpu = gpu_1.addFIFOBuffer("R1");

		pa0gpu.connectToInputPort("i", ram).connectToOutputPort("o", r0gpu, ram).setBytesPerCycle(1);
		Processor pbgpu = gpu_1.addProcessor("B", "B");
		pbgpu.connectToInputPort("i", r0gpu, ram).connectToOutputPort("o", r1gpu, ram).setBytesPerCycle(2);
		
		Processor pcgpu = gpu_1.addProcessor("C", "C");
		pcgpu.connectToInputPort("i", r1gpu, ram).setBytesPerCycle(4);
		
		gpu.requires.add(ram);
		
		
		MappingAlgorithm mapAlg = new MappingAlgorithm();
		Mapping mapping = mapAlg.map(app, plt);
	
		DesignSpace ds = new DesignSpace(app, mapping, plt);
		VariabilityTree tree = new VariabilityTree(ds);
		
		NonFunctionalCst nfr = 
				NonFunctionalCst.AND(
						NonFunctionalCst.QUALITY(">=", 1),
						NonFunctionalCst.COST("<", 200),
						NonFunctionalCst.RUN_TIME("<", 2400));
		
		FunctionCost cfct = new FunctionCost().QUALITY(0).COST(0).RUN_TIME(1);
		
		try {
			new Reasoning().reason("HelloWorld5", ds, nfr, cfct, true, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
