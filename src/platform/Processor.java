package platform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class Processor extends Resource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2101964968437214019L;
	public static class Port implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 4142269483711885845L;
		public Port(Processor p, String id) {
			this.p = p;
			this.id = id;
			memories = new ArrayList<Memory>();
		}
		
		public Processor p;
		public String id;
		public Collection<Memory> memories;
	}
	
	public Processor(String id, Resource parent, String function) {
		super(id, Type.PROCESSOR, parent);
		this.function = function;
		inputs = new HashMap<String, Port>();
		outputs = new HashMap<String, Port>();
	}
	
	public Processor connectToInputPort(String port, Memory... memories) {
		if(port.contains("_o_"))
			assert(false);
		if(!inputs.containsKey(port))
			inputs.put(port, new Port(this, port));
		for(Memory memory : memories) {
			inputs.get(port).memories.add(memory);
			memory.consumers.add(this);
		}
		return this;
	}
	
	public Processor connectToInputPort(String port, Collection<Memory> memories) {
		for(Memory m : memories)
			connectToInputPort(port, m);
		return this;
	}
	
	public void connectToInputPorts(Memory... memories) {
		for(String p : inputs.keySet())
			connectToInputPort(p, memories);
	}
	
	public void connectToInputPorts(Collection<Memory> memories) {
		for(String p : inputs.keySet())
			connectToInputPort(p, memories);
	}
	
	public Collection<Memory> getInputMemories(String id) {
		return inputs.get(id).memories;
	}
	
	public Collection<Memory> getInputStorages(String id) {
		Collection<Memory> res = inputs.get(id).memories.stream().filter(m -> m.type == Memory.Type.STORAGE).collect(Collectors.toSet());
		return (res != null)? res : new ArrayList<>();
	}
	
	public Collection<Memory> getInputFIFObuffers(String id) {
		Collection<Memory> res = inputs.get(id).memories.stream().filter(m -> m.type == Memory.Type.FIFO).collect(Collectors.toSet());
		return (res != null)? res : new ArrayList<>();
	}
	
	public Collection<Memory> getInputFIFOBuffers() {
		Collection<Memory> res = new HashSet<>();
		for(String p : inputs.keySet())
			res.addAll(getInputFIFObuffers(p));
		return res;
	}
	
	public Collection<Memory> getInputFIFOBuffersPath(String id) {
		Collection<Memory> ms = inputs.get(id).memories.stream().filter(m->m.type == Memory.Type.FIFO).collect(Collectors.toSet());
		Collection<Memory> res = new HashSet<>();
		for(Memory m : ms) {
			res.add(m);
			for(Processor producer : m.producers)
				res.addAll(producer.getInputFIFOBuffersPath());
		}
		return res;
	}
	
	public Collection<Memory> getInputFIFOBuffersPath() {
		Collection<Memory> res = new HashSet<>();
		for(String p : inputs.keySet())
			res.addAll(getInputFIFOBuffersPath(p));
		return res;
			
	}
	
	public Processor connectToOutputPort(String port, Memory... memories) {
		if(port.contains("_i_"))
			assert(false);
		if(!outputs.containsKey(port))
			outputs.put(port, new Port(this, port));
		for(Memory memory : memories) {
			outputs.get(port).memories.add(memory);
			memory.producers.add(this);
		}
		return this;
	}
	
	public Processor connectToOutputPort(String port, Collection<Memory> memories) {
		for(Memory m : memories)
			connectToOutputPort(port, m);
		return this;
	}
	
	public void connectToOutputPorts(Memory... memories) {
			for(String p : outputs.keySet())
					connectToOutputPort(p, memories);
	}
	
	public void connectToOutputPorts(Collection<Memory> memories) {
		for(String p : outputs.keySet())
				connectToOutputPort(p, memories);
	}
	
	public Collection<Memory> getOutputMemories(String id) {
		return outputs.get(id).memories;
	}
	
	public Collection<Memory> getOutputStorages(String id) {
		Collection<Memory> res = outputs.get(id).memories.stream().filter(m -> m.type == Memory.Type.STORAGE).collect(Collectors.toSet());
		return (res != null)? res : new ArrayList<>();
	}
	
	public Collection<Memory> getOutputStorages() {
		Collection<Memory> res = new HashSet<Memory>();
		for(String p : outputs.keySet())
			res.addAll(getOutputStorages(p));
		return res;
	}
	
	public Collection<Memory> getOutputFIFOBuffers(String id) {
		Collection<Memory> res = outputs.get(id).memories.stream().filter(m -> m.type == Memory.Type.FIFO).collect(Collectors.toSet());
		return (res != null)? res : new ArrayList<>();
	}
	
	public Collection<Memory> getOutputFIFOBuffers() {
		Collection<Memory> res = new HashSet<>();
		for(String p : outputs.keySet())
			res.addAll(getOutputFIFOBuffers(p));
		return res;
	}
	
	public Collection<Memory> getOutputFIFOBuffersPath(String id) {
		Collection<Memory> ms = outputs.get(id).memories.stream().filter(m->m.type == Memory.Type.FIFO).collect(Collectors.toSet());
		Collection<Memory> res = new ArrayList<>();
		for(Memory m : ms) {
			res.add(m);
			for(Processor consumer : m.consumers)
				res.addAll(consumer.getOutputFIFOBuffersPath());
		}
		return res;
	}
	
	public Collection<Memory> getOutputFIFOBuffersPath() {
		Collection<Memory> res = new HashSet<>();
		for(String p : outputs.keySet())
			res.addAll(getOutputFIFOBuffersPath(p));
		return res;
			
	}
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String function;
	public Map<String, Port> inputs;
	public Map<String, Port> outputs;
	
}
