package platform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

import platform.Resource.EnergyMode;

public class Component extends Resource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6481677134136776873L;

	public Component(String id, Resource parent) {
		super(id, Type.COMPONENT, parent);
		children = new ArrayList<Resource>();
	}

	public Component(String id) {
		this(id, null);
	}

	public Component addComponent(String id) {
		Component res = new Component(this.id+"_"+id, this);
		children.add(res);
		return res;
	}
	
	public int getMainCLock() {
		int mainCLock = super.getMainClock();
		
		for(Resource r : children) {
			int tmp = r.getMainClock();
			if(tmp > mainCLock)
				mainCLock = tmp;
		}
		
		return mainCLock;
	}
	
	public Memory addFIFOBuffer(String id) {
		Memory res = new Memory(this.id+"_"+id, this, Memory.Type.FIFO);
		children.add(res);
		return res;	
	}
	
	public Storage addStorage(String id, Storage.Type type) {
		Storage res = new Storage(this.id+"_"+id, this, type);
		children.add(res);
		return res;
	}
	
	public Processor addProcessor(String id, String function) {
		Processor res = new Processor(this.id+"_"+id, this, function);
		children.add(res);
		return res;
	}
	
	public Collection<Processor> getProcessors() {
		Collection<Processor> res = new ArrayList<Processor>();
		for(Resource r : children)
			if(r.type == Type.PROCESSOR)
				res.add((Processor)r);
			else if(r.type == Type.COMPONENT)
				res.addAll(((Component)r).getProcessors());
		return res;
	}
	
	public Collection<Component> getProcessingEngine(){
		Collection<Component> res = new HashSet<>();
		for(Resource r : children) {
			if(r.type == Type.PROCESSOR)
				res.add(this);
			else if(r.type == Type.COMPONENT)
				res.addAll(((Component)r).getProcessingEngine());
		}
		return res;
	}
	
	public Collection<Memory> getMemories(){
		Collection<Memory> res = new ArrayList<Memory>();
		for(Resource r : children)
			if(r.type == Type.MEMORY)
				res.add((Memory)r);
			else if(r.type == Type.COMPONENT)
				res.addAll(((Component)r).getMemories());
		return res;
	}
	
	public Collection<Memory> getFIFOBuffers() {
		return getMemories().stream().filter(r -> r.type == Memory.Type.FIFO).collect(Collectors.toList());
	}
	
	public Collection<Memory> getStorages() {
		return getMemories().stream().filter(r -> r.type == Memory.Type.STORAGE).collect(Collectors.toList());
	}
	
	public Collection<Resource> getConfigurableResources(){
		return new HashSet<Resource>(children).stream().filter(r -> r.isConfigurable()).collect(Collectors.toList());
	}

	public Collection<Resource> getOptionalResources() {
		Collection<Resource> res = new HashSet<Resource>();
		if(isOptional())
			res.add(this);
		for(Resource r : children) {
			if(r.type == Type.COMPONENT)
				res.addAll(((Component)r).getOptionalResources());
			else if(r.isOptional())
					res.add(r);
		}
		return res;
	}

	public void print() {
		if(getStorages().size() > 0)
			System.out.println("***** STORAGES *****");
		for(Memory s : getStorages())
			System.out.println(s.id);
			
		for(Component c : getProcessingEngine()) {
			System.out.println("***** CORES *****");
			System.out.print("\t\t");
			for(Memory m : getMemories())
				System.out.print(m.id+":\t");
			System.out.println();
			
			for(Processor p : c.getProcessors()) {
				System.out.print(p.id+"("+p.inputs.size()+","+p.outputs.size()+"):\t");
				for(Memory m : getMemories()) {
					String tmp = "";
					for(String in : p.inputs.keySet())
						if(p.getInputMemories(in).contains(m))
							tmp+="(IN)";
					for(String out : p.outputs.keySet())
						if(p.getOutputMemories(out).contains(m))
							tmp+="(OUT)";
					System.out.print(tmp.equals("")? "\t\tX" : "\t\t"+tmp);
				}
				System.out.println();
			}
			System.out.println();
		}
		System.out.println("***** CHILD *****");
		for(Resource c : children) {
			if(c instanceof Component)
				((Component)c).print();
		}
				
	}
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
		
	}

	public Collection<? extends Resource> getResources() {
		return children;
	}
	
	public Collection<Resource> children;
}
