package platform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Storage extends Memory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1735272433370807829L;
	public static class Capacity  implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 5368966073291423291L;
		public Capacity(String id, int value, int cost) {
			this.id = id;
			this.value = value;
			this.cost = cost;
		}
		
		public String id;
		public int value;
		public int cost;
	}
	
	public enum Type{
		READ_ONLY,
		READ_AND_WRITE;
	}
	
	public Storage(String id, Resource parent, Type type) {
		super(id, parent, Memory.Type.STORAGE);
		this.type = type;
		capacities = new ArrayList<Capacity>();
	}
	
	public Storage addCapacity(String id, int value, int cost) {
		this.capacities.add(new Capacity(id, value, cost));
		return this;
	}
	
	public Storage setAccessLatency(int latency) {
		this.latency = latency;
		return this;
	}
	
	public boolean isConfigurable() {
		return super.isConfigurable() || capacities.size() > 1;
	}
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public Type type;
	public Collection<Capacity> capacities;
	public int latency;
}
