package platform;

public interface Visitor {

	public void visit(Component c);
	public void visit(Memory m);
	public void visit(Processor p);
	public void visit(Storage s);
}
