package platform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Memory extends Resource implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1279100424912998843L;

	public enum Type {
		STORAGE,
		FIFO;
	}
	
	public Memory(String id, Resource parent, Type type) {
		super(id, Resource.Type.MEMORY, parent);
		this.type = type;
		producers = new ArrayList<Processor>();
		consumers = new ArrayList<Processor>();
	}
	
	public Type type;
	
	public Collection<Processor> producers;
	public Collection<Processor> consumers;
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
