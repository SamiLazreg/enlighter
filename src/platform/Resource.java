package platform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Resource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8166382338628222144L;
	public static class EnergyMode implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -4417199025583936206L;
		public EnergyMode(int idle, int active) {
			this.idle = idle;
			this.active = active;
		}
		
		public int idle;
		public int active;
	}
	
	public enum Type{
		MEMORY,
		PROCESSOR,
		COMPONENT;
	}
	
	public Resource(String id, Type type, Resource parent){
		this.id = id;
		this.type = type;
		this.isOptional = false;
		this.parent = parent;
		this.cost = 0;
		this.bpc = 0;
		this.energy = null;
		frequencies = new ArrayList<Integer>();
		requires = new ArrayList<Resource>();
		excludes = new ArrayList<Resource>();
	}
	
	public Resource setOptional() {
		isOptional = true;
		return this;
	}
	
	public Resource setCost(int cost) {
		this.cost = cost;
		return this;
	}
	
	public Resource setBytesPerCycle(int bpc) {
		this.bpc = bpc;
		return this;
	}
	
	public Resource setEnergyMode(int i, int j) {
		this.energy = new EnergyMode(i, j);
		return this;
	}
	
	public Resource setFrequencies(int... frequencies) {
		for(int frequency : frequencies)
			this.frequencies.add(frequency);
		return this;
	}
	
	public Collection<Integer> getFrequencies(){
		return frequencies.isEmpty() && parent != null?  parent.getFrequencies() : frequencies;
	}
	
	public int getMainClock() {
		int mainCLock = 0;
		for(Integer f : frequencies)
			if(f > mainCLock)
				mainCLock = f;
			
		return mainCLock;
	}
	
	public Resource getClockOwner() {
		if(frequencies.size()>0)
			return this;
		else
			return parent.getClockOwner();
	}
	
	public boolean isConfigurable() {
		return frequencies.size() > 1 || (parent!=null? parent.isConfigurable(): false);
	}
	
	public boolean isOptional() {
		return isOptional || (parent!=null && parent.isOptional());
	}
	
	public abstract void accept(Visitor v);
	
	public String toString() {
		return id;
	}

	public String id;
	public Type type;
	public boolean isOptional;
	
	public Resource parent;
	public Collection<Resource> requires;
	public Collection<Resource> excludes;
	
	public int cost;
	
	private Collection<Integer> frequencies;
	public int bpc;
	public EnergyMode energy;
}
