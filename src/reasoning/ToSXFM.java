package reasoning;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.ConstraintNode;
import designspace.VariabilityTree.Domain;
import designspace.VariabilityTree.VariabilityNode;
import designspace.VariabilityTree.VariabilityNode.Variability;
import splar.core.constraints.Assignment;
import splar.core.fm.FeatureModel;
import splar.core.fm.FeatureModelException;
import splar.core.fm.XMLFeatureModel;
import splar.core.heuristics.FTPreOrderSortedECTraversalHeuristic;
import splar.core.heuristics.VariableOrderingHeuristic;
import splar.core.heuristics.VariableOrderingHeuristicsManager;
import splar.plugins.reasoners.bdd.javabdd.FMReasoningWithBDD;
import splar.plugins.reasoners.bdd.javabdd.ReasoningWithBDD;

public class ToSXFM {

	private void _metaheader(VariabilityTree.Domain domain) {
		switch(domain) {
		case APP:
			sxfm+="<feature_model name=\"System Design Space\">\n" +
					"<meta>\n" +
					"<data name=\"description\">Variability-Intensive Embedded System Design Space</data>\n" +
					"<data name=\"creator\">Sami Lazreg</data>\n" +
					"<data name=\"email\">lazreg@i3s.unice.fr</data>\n" +
					"<data name=\"data\">"+DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now())+"</data>\n" +
					"<data name=\"department\">R_And_D</data>\n" +
					"<data name=\"organization\">Visteon Electonics and Universit� C�te d'Azur</data>\n" +
					"<data name=\"address\"></data>\n" +
					"<data name=\"phone\"></data>\n" +
					"<data name=\"website\"></data>\n" +
					"<data name=\"reference\"></data>\n" +
					"</meta>\n";
			break;
		case DS:
			sxfm+="<feature_model name=\"System Design Space\">\n" +
					"<meta>\n" +
					"<data name=\"description\">Variability-Intensive Embedded System Design Space</data>\n" +
					"<data name=\"creator\">Sami Lazreg</data>\n" +
					"<data name=\"email\">lazreg@i3s.unice.fr</data>\n" +
					"<data name=\"data\">"+DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now())+"</data>\n" +
					"<data name=\"department\">R_And_D</data>\n" +
					"<data name=\"organization\">Visteon Electonics and Universit� C�te d'Azur</data>\n" +
					"<data name=\"address\"></data>\n" +
					"<data name=\"phone\"></data>\n" +
					"<data name=\"website\"></data>\n" +
					"<data name=\"reference\"></data>\n" +
					"</meta>\n";
			break;
		case MAP:
			sxfm+="<feature_model name=\"System Design Space\">\n" +
					"<meta>\n" +
					"<data name=\"description\">Variability-Intensive Embedded System Design Space</data>\n" +
					"<data name=\"creator\">Sami Lazreg</data>\n" +
					"<data name=\"email\">lazreg@i3s.unice.fr</data>\n" +
					"<data name=\"data\">"+DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now())+"</data>\n" +
					"<data name=\"department\">R_And_D</data>\n" +
					"<data name=\"organization\">Visteon Electonics and Universit� C�te d'Azur</data>\n" +
					"<data name=\"address\"></data>\n" +
					"<data name=\"phone\"></data>\n" +
					"<data name=\"website\"></data>\n" +
					"<data name=\"reference\"></data>\n" +
					"</meta>\n";
			break;
		case PLT:
			sxfm+="<feature_model name=\"System Design Space\">\n" +
					"<meta>\n" +
					"<data name=\"description\">Variability-Intensive Embedded System Design Space</data>\n" +
					"<data name=\"creator\">Sami Lazreg</data>\n" +
					"<data name=\"email\">lazreg@i3s.unice.fr</data>\n" +
					"<data name=\"data\">"+DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now())+"</data>\n" +
					"<data name=\"department\">R_And_D</data>\n" +
					"<data name=\"organization\">Visteon Electonics and Universit� C�te d'Azur</data>\n" +
					"<data name=\"address\"></data>\n" +
					"<data name=\"phone\"></data>\n" +
					"<data name=\"website\"></data>\n" +
					"<data name=\"reference\"></data>\n" +
					"</meta>\n";
			break;
		default:
			break;
		
		}
	}
	
	public String DSToSXFM(String name, DesignSpace ds, VariabilityTree.Domain domain) {
		sxfm = "";
		
		_metaheader(domain);
		
		VariabilityNode root = null;
		switch(domain) {
		case APP:
			root = ds.tree.root.get("ApplicationVariability");
			assert(root != null);
			break;
		case DS:
			root = ds.tree.root;
			break;
		case MAP:
			root = ds.tree.root.get("DeploymentVariability");
			assert(root != null);
			break;
		case PLT:
			root = ds.tree.root.get("PlatformVariability");
			assert(root != null);
			break;
		default:
			break;
		
		}
		
		sxfm+="<feature_tree>\n";
		sxfm+=":r DesignSpace (design_space_id)";	
		_toSXFM(root,1);
		sxfm+="\n</feature_tree>\n";
		sxfm+="<constraints>\n";
		_cstTree(ds.tree, domain);
		sxfm+="\n</constraints>\n";
		sxfm+="</feature_model>\n";
		
		try {
			PrintWriter writer = new PrintWriter("./"+name+".sxfm.xml", "UTF-8");
			writer.print(sxfm);
			writer.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Feature model path
					String featureModelPath = "./"+name+".sxfm.xml";
					
					// Create feature model object from an XML file (SXFM format - see www.splot-research.org for details)	
					// If an identifier is not provided for a feature use the feature name as id
					FeatureModel featureModel = new XMLFeatureModel(featureModelPath, XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);
					// load feature model from 			
					try {
						featureModel.loadModel();
					} catch (FeatureModelException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}			

					// create BDD variable order heuristic
					new FTPreOrderSortedECTraversalHeuristic("Pre-CL-MinSpan", featureModel, FTPreOrderSortedECTraversalHeuristic.FORCE_SORT);		
					VariableOrderingHeuristic heuristic = VariableOrderingHeuristicsManager.createHeuristicsManager().getHeuristic("Pre-CL-MinSpan");

					// BDD construction parameters
					// - Tuning this parameters can be tricky at times and may require playing a bit
					// - For the purpose of this example let's assume "large enough" values 
					int 	bddNodeNum 		= 50000;  	// sets the initial size of the BDD table  
					int 	bddCacheSize 	= 50000;  	// sets the size of the BDD cache table
					long	maxBuildingTime = 60000L;	// sets the maximum building time
					
					// Creates the BDD reasoner
					reasoner = new FMReasoningWithBDD(featureModel, heuristic, bddNodeNum, bddCacheSize, maxBuildingTime, "pre-order");
					
					// Initialize the reasoner (BDD is created at this moment)
					try {
						reasoner.init();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// Use the reasoner			
					System.out.println("BDD has " + reasoner.getBDD().nodeCount() + " nodes and was built in " + reasoner.getBDDBuildingTime() + " ms");
					
					// Check if feature model is consistent, i.e., has at least one valid configuration
					try {
						System.out.println("Feature model is " + (reasoner.isConsistent()? "" : " NOT ") + "consistent!");
					} catch (OperationNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// Count feature model solutions		
					try {
						ds.nbDesigns = reasoner.countValidConfigurations();
					} catch (OperationNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Feature model has " + ds.nbDesigns + " possible configurations");
					
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
		
		return sxfm;
	}
	
	private String _tab(int level) {
		String res = "\n";
		for(int i = 0; i < level; ++i)
			res+="\t";
		return res;
	}
	
	private void _toSXFM(VariabilityNode node, int level) {
		sxfm+=_tab(level);
		if(node.variability == Variability.OPT) sxfm+=":o ";
		else if(node.variability == Variability.MANDATORY) sxfm+=":m "; 
		else sxfm+=": ";
		sxfm+=node.id+" ("+node.id+"_id)";
		
		switch(node.type) {
		case XOR:
			sxfm+=_tab(++level)+":g ("+node.id+"_xor_id) [1,1]";
			break;
		}
		for(VariabilityNode n : node.children)
			_toSXFM(n, level+1);
	}
	
	/*	constraints.put("Flow", new ArrayList<>());
			constraints.put("Application Deployment", new ArrayList<>());
			constraints.put("Platform Deployment", new ArrayList<>());
			constraints.put("Resources", new ArrayList<>());
			constraints.put("NodeMapping", new ArrayList<>());
			constraints.put("PathMapping", new ArrayList<>());*/
	
	private void _cstTree(VariabilityTree tree, VariabilityTree.Domain domain) {
		Map<String, List<ConstraintNode>> _constraints = new HashMap<>(tree.constraints);
		switch(domain) {
		case APP:
			_constraints.remove("Resources");
			_constraints.remove("Application Deployment");
			_constraints.remove("Platform Deployment");
			_constraints.remove("NodeMapping");
			_constraints.remove("PathMapping");
			break;
		case DS:
			break;
		case MAP:
			assert(false);
			break;
		case PLT:
			_constraints.remove("Flow");
			_constraints.remove("Application Deployment");
			_constraints.remove("Platform Deployment");
			_constraints.remove("NodeMapping");
			_constraints.remove("PathMapping");
			break;
		default:
			break;
		
		}
		
		int i = 0;
		for(Map.Entry<String, List<ConstraintNode>> e : _constraints.entrySet()) {
			sxfm+="\n"+e.getKey()+" constraints";
			for(ConstraintNode n : ConstraintNode.convertToSPLOT_CNF(e.getValue()))
				sxfm+="\nc"+(++i)+": "+_cstNode(n);
			sxfm+="\n";
		}
		
		if(domain == Domain.DS)
			for(ConstraintNode n : ConstraintNode.convertToSPLOT_CNF(tree.prunings))
				sxfm+="\nc"+(++i)+": "+_cstNode(n);
				
	}
	
	private String _cstNode(ConstraintNode n) {
		String res = "";
		switch(n.type) {
		case REQ:
			assert(false);
			assert(n.nodes.size() == 2);
			res += "~"+_cstNode(n.nodes.get(0))+" or "+_cstNode(n.nodes.get(1));
			break;
		case IFF:
			assert(false);
			assert(n.nodes.size() == 2);
			/*a iff b = (!a|b)&(a|~b)*/
			res += "(~"+_cstNode(n.nodes.get(0))+" or "+_cstNode(n.nodes.get(1))+") and ("+_cstNode(n.nodes.get(0))+" or ~"+_cstNode(n.nodes.get(1))+")";
			break;
		case NOT:
			assert(n.nodes.size() == 1);
			res += "~"+_cstNode(n.nodes.get(0));
			break;
		case LEAF:
			assert(n.nodes == null);
			res += n.id+"_id";
			break;
		case AND:
			assert(false);
			assert(n.nodes.size() >= 1);
			res += "(";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+") and (";
			//res += _cstNode(n.nodes.get(i))+")";
			res += _cstNode(n.nodes.get(i));
			break;
		case OR:
			assert(n.nodes.size() >= 1);
			//res += "(";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+" or ";
			//res += _cstNode(n.nodes.get(i))+")";
			res += _cstNode(n.nodes.get(i));
			break;
		}
		return res;
	}
	
	private String sxfm;
	public ReasoningWithBDD reasoner;
}
