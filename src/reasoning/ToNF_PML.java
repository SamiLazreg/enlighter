package reasoning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

import application.DataSource;
import application.Node;
import application.Path;
import application.Task;

import application.Node.Argument;
import application.Node.Type;
import deployment.DataSourceMapping;
import deployment.PathMapping;
import deployment.TaskMapping;
import designspace.DesignSpace;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;

public class ToNF_PML {

	public String toNfPML(DesignSpace ds, NonFunctionalCst cst, FunctionCost f) {
		this.ds = ds;
		this.cst = cst;
		this.fc = f;
		
		gen_id = 1;
		
		pml="";
		if(cst!=null)
			_generatePropery();
		_createFeatureStruct();
		_globalDefinitions();
		
		_defineStorages();
		_defineProcessors();
		
		_defineNodes();
		
		return pml;
	}
	
	private void _generatePropery() {
		pml+="check <> (";
		for(Node n : ds.app.getNodes())
			pml+=n.id.toLowerCase()+"_end && ";
		for(Memory m : ds.plt.getStorages())
			pml+=m.id.toLowerCase()+"_idle && ";
		for(Component c : ds.plt.getProcessingEngine()) 
			pml+=c.id.toLowerCase()+"_idle && ";
		pml=pml.substring(0, pml.length()-4);
		pml+=") within "+cst.get(NonFunctionalCst._RUN_TIME).value;
	}

	private void _globalDefinitions() {
		//pml+="\n\nclock time;";
		pml+="\n#define main_freq "+ds.plt.getMainCLock();
		pml+="\n#define burst 512";
		
		_defineMemoriesId(ds.plt.getMemories());
		_defineProcessorId(ds.plt.getProcessors());
		_defineCommandSystem();
	}
	
	public void _createFeatureStruct() {
		
		pml+= "\ntypedef features{";
		pml+= _createFeatures(ds.tree.root);
		pml = pml.substring(0, pml.length()-1);
		pml+= "\n}features f;";
		pml+= "\n//design number = "+ds.nbDesigns;
	}
	
	private String _createFeatures(VariabilityNode n){
		String res = "";
		if(n.type == VariabilityNode.Type.GROUP && n.children.isEmpty())
			return res;
		res+="\nbool "+n.id+";";
		for(VariabilityNode c : n.children)
			res+=_createFeatures(c);
		return res;
	}
	
	private void _defineMemoriesId(Collection<Memory> ms) {
		int i = 0;
		pml += "\n\n#define NB_MEMORY "+ms.size();
		pml += "\n#define NULL_MEMORY -1";
		for(Memory m : ms)
			pml+="\n#define "+m.id+"_ID "+(i++);
		pml+="\n";
	}
	
	private void _defineProcessorId(Collection<Processor> ps) {
		int i = 0;
		pml += "\n\n#define NB_PROCESSOR "+ps.size();
		pml += "\n#define NULL_PROCESSOR -1";
		for(Processor p : ps)
			pml+="\n#define "+p.id+"_ID "+(i++);
		i = 0;
		pml+="\n\n#define NB_PU "+(ds.plt.getProcessingEngine().size());
		pml += "\n#define NULL_PU -1";
		for(Component c : ds.plt.getProcessingEngine()) {
			pml+="\n#define "+c.id+"_ID "+(i++);
		}

		pml+="\n";
	}
	
	private void _defineCommandSystem() {
				
		//"\n\n#define empty_data {0, 0, 0}\n";
		//command
		int ins = 0; int outs = 0;
		for(Node n : ds.app.getNodes()) {
			if(n.inputs.size()>ins)
				ins = n.inputs.size();
			if(n.outputs.size()>outs)
				outs = n.outputs.size();
		}
		
		/*String s_ins = "";
		for(int i = 0; i < ins; ++i)
			s_ins+="empty_data, ";
		s_ins = s_ins.substring(0, s_ins.length()-2);
		String s_outs = "";
		for(int i = 0; i < outs; ++i)
			s_outs+="empty_data, ";
		s_outs = s_outs.substring(0, s_outs.length()-2);
		*/
		//"\n\n#define empty_cmd { 0, { "+s_ins+" }, 0, { "+s_outs+" }, 0}";

		pml+="typedef data{\n" + 
				"	byte id;\n" + 
				"	byte loc;\n" + 
				"	int size\n" + 
				"}\n" + 
				"\n" + 
				"#define MAX_NB_INS "+ins+"\n" + 
				"#define MAX_NB_OUTS "+outs+"\n" + 
				"\n" + 
				"typedef command{\n" + 
				"	byte fct;\n" + 
				"	data ins[MAX_NB_INS];\n" + 
				"	byte nb_ins;\n" + 
				"	data outs[MAX_NB_OUTS];\n" + 
				"	byte nb_outs\n" + 
				"}\n" + 
				"\n" + 
				"#define NB_TASK "+ds.app.getTasks().size()+"\n" + 
				"\n" + 
				"#define LIST_LENGTH NB_TASK\n" + 
				"\n" + 
				"typedef dependency{\n" + 
				"	byte tasks[NB_TASK];\n" + 
				"	byte depth\n" + 
				"}\n" + 
				"\n" + 
				"typedef command_list{\n" + 
				"	command cmd[LIST_LENGTH];\n" + 
				"	dependency dep[LIST_LENGTH];\n" + 
				"	int size\n" + 
				"}\n" + 
				"\n" + 
				"chan transfer[NB_MEMORY] = [0] of {data};\n" + 
				"chan send[NB_PU] = [0] of {command};\n" + 
				"chan recv[NB_PU] = [0] of {command};\n" + 
				"\n";
	}
	
	private void _defineStorages() {
		
		for(Memory m : ds.plt.getStorages()) {
			pml += "\n\nbool "+m.id.toLowerCase()+"_idle = true; \n\nactive proctype storage_"+m.id+"(){";
			pml +=" atomic {";
			
			if(m.isOptional) {
				pml+="\ngd\n::f."+m.id+" ->";
			}
			
			/*if(m.cost != 0)
				pml+="\n\tcost + "+m.cost+";";
			*/
			//pml += "\n\tclock clk;";
			pml += "\n\tint freq"+(m.getFrequencies().size()>1? "": " = "+m.getFrequencies().iterator().next())+";";
			pml += "\n\tint bpc = "+m.bpc+";";
			pml += "\n\tint latency ="+((Storage)m).latency+";";
			pml += "\n\tint _delay = 0;";
			pml += "\n\tdata in;";
			
			if(m.getFrequencies().size() > 1) {
				pml += "\n\n\tgd";
				for(int freq : m.getFrequencies())
					pml += "\n\t::f."+m.getClockOwner().id+"_frequency_"+freq+" -> freq = "+freq+";";
				pml += "\n\tdg;";
			} else {
				pml += "\n\tfreq = "+m.getFrequencies().iterator().next()+";";
			}
			
			pml += "\n\n\t_delay = (latency*main_freq/freq)+(burst/bpc*main_freq/freq);";
			
			pml += "\n\n\tdo\n\t::transfer["+m.id+"_ID]?in -> "+m.id.toLowerCase()+"_idle = false; "+
					"\n\t\twait(_delay) then skip;"+
				   "\n\t\t"+m.id.toLowerCase()+"_idle = true; \n\tod;";
			
			if(m.isOptional)
				pml+="\ndg;";
			
			pml+="\n};";
			
			pml+="\n}";
		}
	}
	
	private void _defineProcessors() {
		
		String guard = "";
		if(!ds.plt.getStorages().isEmpty()) {
			for(Memory m : ds.plt.getStorages()) {
				guard+="id == "+m.id+"_ID || ";
			}
			if(!guard.equals(""))
				guard = guard.substring(0, guard.length()-4);
			guard="\t_is_external = "+guard+";";
		} else
			guard = "_is_external = false;";
		
		
		{
		pml+="\n#define data_copy(dst, src) dst.id = src.id; dst.loc = src.loc; dst.size = src.size;\n" + 
				"\n" + 
				"#define cmd_copy(dst, src) dst.fct = src.fct; dst.nb_ins = src.nb_ins; dst.nb_outs = src.nb_outs; \\\n" + 
				"	j = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: j < src.nb_ins -> data_copy(dst.ins[j], src.ins[j]) j++			\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;																	\\\n" + 
				"	j = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: j < src.nb_outs -> data_copy(dst.outs[j], src.outs[j]) j++		\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define free_data(data) data.id = 0; data.loc = 0; data.size = 0;\n" + 
				"\n" + 
				"#define free_cmd(cmd)  \\\n" + 
				"	j = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: j < cmd.nb_ins -> free_data(cmd.ins[j]) j++						\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;																	\\\n" + 
				"	j = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: j < cmd.nb_outs -> free_data(cmd.outs[j]) j++					\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;										\\\n" + 
				"	cmd.fct = 0; cmd.nb_ins = 0; cmd.nb_outs = 0;\n" + 
				"	\n" + 
				"#define is_external(id)"+guard + 
				"\n" + 
				"#define inactive(cmd) _inactive = cmd.fct == 0 && cmd.nb_ins == 0 && cmd.nb_outs == 0;\n" + 
				"\n" + 
				"#define declare_var()													\\\n" + 
				"	int i_cmd; int _delay;												\\\n" + 
				"	int i_time_cmd;														\\\n" + 
				"	int	i, j, k, l, m;													\\\n" + 
				"	bool _is_flush_cmd, _time_cmd, _hasCompleted, _is_external, _inactive;\\\n" + 
				"	command_list list;													\\\n" +
				"\n"																			+
				"#define allocate() 													\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	::i < list.size ->													\\\n" + 
				"		inactive(list.cmd[i])											\\\n" + 
				"		if																\\\n" + 
				"		::_inactive -> i_cmd = i; break;								\\\n" + 
				"		::else -> skip;													\\\n" + 
				"		fi;																\\\n" +
				"		i++;															\\\n" +
				"	::else -> i_cmd = list.size; list.size++; break;					\\\n" + 
				"	od;																	\\\n" + 
				"	cmd_copy(list.cmd[i_cmd], cmd)										\\\n" + 
				"	list.dep[i_cmd].tasks[0] = i_cmd; 									\\\n" + 
				"	list.dep[i_cmd].depth = 1;\n" + 
				"\n" + 
				"#define cmd_list_push()												\\\n" + 
				"	allocate()															\\\n" + 
				"	j = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	::j < cmd.nb_ins ->													\\\n" + 
				"		k = 0;															\\\n" + 
				"		do																\\\n" + 
				"		:: k < list.size ->												\\\n" + 
				"			l = 0;														\\\n" + 
				"			do															\\\n" + 
				"			:: l < list.cmd[k].nb_outs ->								\\\n" + 
				"				if 														\\\n" + 
				"				:: list.cmd[k].outs[l].id == cmd.ins[j].id -> m = 0;	\\\n" + 
				"					do													\\\n" + 
				"					:: m < list.dep[k].depth ->							\\\n" + 
				"						list.dep[i_cmd].tasks[list.dep[i_cmd].depth] = list.dep[k].tasks[m];\\\n" + 
				"						list.dep[i_cmd].depth++;						\\\n" +
				"						m++;											\\\n" +
				"					:: else -> break;									\\\n" + 
				"					od;													\\\n" + 
				"				:: else -> break;										\\\n" + 
				"				fi;														\\\n" +
				"				l++;													\\\n" +
				"			:: else -> break;											\\\n" + 
				"			od;															\\\n" +
				"			k++;														\\\n" +
				"		:: else -> break;												\\\n" + 
				"		od;																\\\n" +
				"		j++;															\\\n" +
				"	:: else -> break;													\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define free_cmd_list()												\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < list.dep[i_cmd].depth ->										\\\n" + 
				"		k = list.dep[i_cmd].tasks[i];									\\\n" + 
				"		free_cmd(list.cmd[k])											\\\n" +
				"		i++;															\\\n" +
				"	:: else -> list.dep[i_cmd].depth = 0; break;						\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define is_flush_cmd()													\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < cmd.nb_outs ->												\\\n" + 
				"		is_external(cmd.outs[i].loc)									\\\n" + 
				"		if 																\\\n" + 
				"		:: !_is_external -> _is_flush_cmd = false; break;				\\\n" + 
				"		:: else -> skip;												\\\n" + 
				"		fi;																\\\n" +
				"		i++;															\\\n" +
				"	:: else -> _is_flush_cmd = true; break;								\\\n" + 
				"	od;\n" + 
				"\n" + 
				"\n" + 
				"#define prog_pipeline()												\\\n" +
				"	nb_ins = 0; nb_outs = 0;											\\\n" +
				"	i_ins = 0; i_outs = 0;												\\\n" +
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < list.dep[i_cmd].depth ->										\\\n" + 
				"		k = list.dep[i_cmd].tasks[i];									\\\n" + 
				"		j = 0;															\\\n" + 
				"		do																\\\n" + 
				"		:: j < list.cmd[k].nb_ins ->									\\\n" + 
				"			is_external(list.cmd[k].ins[j].loc)							\\\n" + 
				"			if															\\\n" + 
				"			:: _is_external -> 											\\\n" +
				"				fetch[nb_ins] = 0;										\\\n" +
				"				data_copy(ins[nb_ins], list.cmd[k].ins[j]) nb_ins++;	\\\n" + 
				"			:: else -> skip;											\\\n" + 
				"			fi;															\\\n" +
				"			j++;														\\\n" +
				"		:: else -> break;												\\\n" + 
				"		od;																\\\n" + 
				"		j = 0;															\\\n" + 
				"		do																\\\n" + 
				"		:: j < list.cmd[k].nb_outs ->									\\\n" + 
				"			is_external(list.cmd[k].outs[j].loc)						\\\n" + 
				"			if															\\\n" + 
				"			:: _is_external -> 											\\\n" +
				"				store[nb_outs] = 0;										\\\n" +
				"				data_copy(outs[nb_outs], list.cmd[k].outs[j]) nb_outs++;\\\n" + 
				"			:: else -> skip;											\\\n" + 
				"			fi;															\\\n" +
				"			j++;														\\\n" +
				"		:: else -> break;												\\\n" + 
				"		od;																\\\n" +
				"		i++;															\\\n" +
				"	:: else -> break;													\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define hasCompleted()													\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < nb_ins ->													\\\n" + 
				"		if																\\\n" + 
				"		:: fetch[i] < ins[i].size -> _hasCompleted = false; break;		\\\n" + 
				"		:: else -> skip; 												\\\n" + 
				"		fi;																\\\n" + 
				"		i++;															\\\n" +
				"	:: else -> _hasCompleted = true; break;								\\\n" + 
				"	od;																	\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < nb_outs && _hasCompleted ->									\\\n" + 
				"		if																\\\n" + 
				"		:: store[i] < outs[i].size -> _hasCompleted = false; break; 	\\\n" + 
				"		:: else -> skip; 												\\\n" + 
				"		fi;																\\\n" +
				"		i++;															\\\n" +
				"	:: else -> break;													\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define checkIns()														\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < nb_ins ->													\\\n" + 
				"		if																\\\n" + 
				"		::fetch[i] >= ins[i].size -> nb_ins--;							\\\n" + 
				"			data_copy(ins[i], ins[nb_ins])								\\\n" + 
				"			fetch[i] = fetch[nb_ins];									\\\n" + 
				"			fetch[nb_ins] = 0;											\\\n" + 
				"		:: else -> skip;												\\\n" + 
				"		fi;																\\\n" +
				"		i++;															\\\n" +
				"	:: else -> i_ins = 0; break;										\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define checkOuts()														\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < nb_outs ->													\\\n" + 
				"		if																\\\n" + 
				"		::store[i] >= outs[i].size -> nb_outs--;						\\\n" + 
				"			data_copy(outs[i], outs[nb_outs])							\\\n" + 
				"			store[i] = store[nb_outs];									\\\n" + 
				"			store[nb_outs] = 0;											\\\n" + 
				"		::else -> skip;													\\\n" + 
				"		fi;																\\\n" +
				"		i++;															\\\n" +
				"	:: else -> i_outs = 0; break;										\\\n" + 
				"	od;\n" + 
				"\n" + 
				"#define setOutputs()													\\\n" + 
				"	i = 0;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < cmd.nb_outs ->												\\\n" + 
				"		cmd.outs[i].size = cmd.ins[0].size;	i++;						\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;";
		}
		
		pml+=	"\n\n#define time_cmd(cmd)												\\\n" +
				"\tif																	\\\n";
		for(Processor p : ds.plt.getProcessors())
			pml+="\t::cmd.fct == "+p.id+"_ID -> _time_cmd = "+p.bpc+";					\\\n";
		pml+="\tfi;";
		
		pml+="\n#define time_cmds()														\\\n" +
				"	time_cmd(list.cmd[ list.dep[i_cmd].tasks[0] ])						\\\n" + 
				"	l = _time_cmd;														\\\n" +
				"	i = 1;																\\\n" + 
				"	do																	\\\n" + 
				"	:: i < list.dep[i_cmd].depth ->										\\\n" + 
				"		k = list.dep[i_cmd].tasks[i];									\\\n" + 
				"		l = _time_cmd;													\\\n" + 
				"		time_cmd(list.cmd[k])											\\\n" +
				"		i++;															\\\n" +
				"		if																\\\n" + 
				"		::_time_cmd < l -> l = _time_cmd;								\\\n" + 
				"		:: else -> skip;												\\\n" + 
				"		fi;																\\\n" + 
				"	:: else -> break;													\\\n" + 
				"	od;																	\\\n" + 
				"	_delay = (burst/l)*(main_freq/freq);";
		
		for(Component c : ds.plt.getProcessingEngine()) {
			pml+="\n\nbool "+c.id.toLowerCase()+"_idle = true;\n\nactive proctype processor_"+c.id+"(){";
			pml+=" atomic {";
			
			if(c.isOptional()) {
				pml+="\ngd\n::f."+c.id+" ->";
			}
			
			/*if(c.cost != 0)
				pml+="\n\tcost + "+c.cost+";";
			*/
			
			//pml+="\n\tclock clk;";
			pml+="\n\tint freq"+(c.getFrequencies().size()>1? "": " = "+c.getFrequencies().iterator().next())+";";
			pml+="\n\n\tcommand cmd;";
			
			pml+="\n\tdata ins[NB_MAX_INS]; int fetch[NB_MAX_INS]; byte nb_ins = 0; byte i_ins;\n" + 
				"\tdata outs[NB_MAX_OUTS]; int store[NB_MAX_OUTS]; byte nb_outs = 0; byte i_outs;\n\n";
			
			pml+="\n\tdeclare_var()\n";
			
			if(c.getFrequencies().size() > 1) {
				pml += "\n\n\tgd";
				for(int freq : c.getFrequencies())
					pml += "\n\t::f."+c.getClockOwner().id+"_frequency_"+freq+" -> freq = "+freq+";";
				pml += "\n\tdg;\n";
			} else {
				pml += "\n\tfreq = "+c.getFrequencies().iterator().next()+";\n";
			}
			
			/*pml+="	do\n" + 
					"	::send["+c.id+"_ID]?cmd -> "+c.id.toLowerCase()+"_idle = false;\n" + 
					"		setOutputs() cmd_list_push() is_flush_cmd()\n" + 
					"		if\n" + 
					"		::!_is_flush_cmd -> recv["+c.id+"_ID]!cmd;\n" + 
					"		::else -> prog_pipeline()\n" + 
					"			do\n" + 
					"			::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];\n" + 
					"				fetch[i_ins] = fetch[i_ins] + burst; i_ins++;\n" + 
					"			::i_ins == nb_ins -> checkIns()\n" + 
					"				time_cmds()\n" +
					"				wait(_delay) then skip;\n" +
					"				hasCompleted()\n" + 
					"				if\n" + 
					"				:: _hasCompleted -> recv["+c.id+"_ID]!outs[i_outs]; free_cmd_list() break;\n" + 
					"				:: else -> \n" + 
					"					do\n" + 
					"					::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];\n" + 
					"						store[i_outs] = store[i_outs] + burst; i_outs++;\n" + 
					"					::i_outs == nb_outs -> checkOuts() break;\n" + 
					"					od;\n" + 
					"				fi;\n" + 
					"			od;\n" + 
					"		fi;\n" + 
					"       "+c.id.toLowerCase()+"_idle = true;\n" +
					"	od;";*/
			
			pml+="do\n" + 
					"	::send["+c.id+"_ID]?cmd -> "+c.id.toLowerCase()+"_idle = false;\n" + 
					"		setOutputs() cmd_list_push() is_flush_cmd()\n" + 
					"		if\n" + 
					"		::!_is_flush_cmd -> recv["+c.id+"_ID]!cmd;\n" + 
					"		::else -> prog_pipeline() hasCompleted()\n" + 
					"			do\n" + 
					"			:: true -> \n" + 
					"\n" + 
					"				do \n" + 
					"				::i_ins < nb_ins -> transfer[ins[i_ins].loc]!ins[i_ins];\n" + 
					"					fetch[i_ins] = fetch[i_ins] + burst; i_ins++;\n" + 
					"				::i_ins == nb_ins -> checkIns() break;\n" + 
					"				od;\n" + 
					"				\n" + 
					"				time_cmds()\n" + 
					"				wait(_delay) then skip;\n" + 
					"				hasCompleted()\n" + 
					"				\n" + 
					"				if\n" + 
					"				:: _hasCompleted -> break;\n" + 
					"				:: else -> skip;\n" + 
					"				fi;\n" + 
					"				\n" + 
					"				do\n" + 
					"				::i_outs < nb_outs -> transfer[outs[i_outs].loc]!outs[i_outs];\n" + 
					"					store[i_outs] = store[i_outs] + burst; i_outs++;\n" + 
					"				::i_outs == nb_outs -> checkOuts() break;\n" + 
					"				od;\n" + 
					"\n" + 
					"				hasCompleted()\n" + 
					"\n" + 
					"				if\n" + 
					"				:: _hasCompleted -> break;\n" + 
					"				:: else -> skip;\n" + 
					"				fi;\n" + 
					"			od;	\n" + 
					"			recv["+c.id+"_ID]!cmd; free_cmd_list()\n" + 
					"		fi;\n" + 
					"       "+c.id.toLowerCase()+"_idle = true;\n" + 
					"	od;";
			
			if(c.isOptional())
				pml+="\n::else -> skip;\ndg;";
			pml+="\n};";
			pml+="\n}";
		}
	}
	
	public void _defineNodes() {
		
		Collection<Path> paths = ds.app.getPaths();
		pml+="\n";
		for(Path p : paths) {
			pml+="\nchan "+p.id+" = [0] of {data}";
		}
		
		for(Node n : ds.app.getSortedApplication()) {
			if(n.type == Type.TASK)
				_defineTask((Task)n);
			else if(n.type == Type.DATA_SOURCE)
				_defineInputData((DataSource)n);
		}
	}

	private void _defineInputData(DataSource n) {
		
		pml+="\n\nbool "+n.id.toLowerCase()+"_end = false;\n\nactive proctype input_"+n.id+"(){";
		pml+=" atomic {";
		
		if(n.isOptional) {
			pml+="\ngd::f."+n.id+"->\n";
		}
		
		if(n.qualityLoss != 0)
			pml+="\n\tqualityLoss + "+n.qualityLoss+";";
		
		pml+="\n\tdata in;\n\tin.id = "+(gen_id++)+";";
		
		if(n.sizes.size() > 1) {
			pml+="\n\tgd";
			for(DataSource.Size s : n.sizes) {
				String costs = "";
				if(ds.tree.root.get(n.id+"_size_"+s.value) != null)
					for(Entry<String, Integer> e : ds.tree.root.get(n.id+"_size_"+s.value).prices.entrySet())
						costs+="\n"+e.getKey()+" + "+e.getValue()+";";
				pml+="\n\t::f."+n.id+"_size_"+s.value+"-> "+costs+" in.size = "+s.value+";";
			}
			pml+="\n\tdg;";
		}else {
			for(DataSource.Size s : n.sizes) {
				String costs = "";
				if(ds.tree.root.get(s.id) != null)
					for(Entry<String, Integer> e : ds.tree.root.get(s.id).prices.entrySet())
						costs+="\n"+e.getKey()+" + "+e.getValue()+";";
				pml+="\n\t"+costs+"in.size = "+s.value+";";
			}
		}
		
		if(ds.map.dms.get(n).choices.size() > 1) {
			pml+="\n\tgd";
			for(DataSourceMapping.Choice c : ds.map.dms.get(n).choices.values()) {
				pml+="\n\t::f."+c.id+"-> ";
				pml+="\n\t\tin.loc = "+c.storage+"_ID;";
			}
			pml+="\n\tdg;";
		}else {
			pml+="\n\tin.loc = "+ds.map.dms.get(n).choices.values().iterator().next().storage+"_ID;";
		}
		
		for(Argument a : n.outputs.values()) {
			pml+="\n\n\t"+a.path+"!in;";
		}
		
		if(n.isOptional)
			pml+="\n::\n\telse -> skip;\ngd";
		
		pml+="\n\t"+n.id.toLowerCase()+"_end = true;";
		pml+="\n};";
		pml+="\n}\n";
	}

	private void _defineTask(Task n) {	
		
		pml+="\n\nbool "+n.id.toLowerCase()+"_end = false;\n\nactive proctype "+n.id+"(){";
		pml+=" atomic {";
		
		if(n.isOptional) {
			pml+="\ngd::f."+n.id+"->\n";
		}
		
		if(n.qualityLoss != 0)
			pml+="\n\tqualityLoss + "+n.qualityLoss+";";
		
		pml+="\n\tcommand cmd;";
		
		pml+="\n\n\tbyte proc = "+(ds.map.tms.get(n).choices.size()==1? ds.map.tms.get(n).choices.keySet().iterator().next()+"_ID": "0")+";";
		pml+="\n\tbyte pu = "+(ds.map.tms.get(n).choices.size()==1? ds.map.tms.get(n).choices.keySet().iterator().next().parent.id+"_ID": "0")+";";
			
		
		for(Argument a : n.inputs.values())
			pml+="\n\tdata "+a.path.id+"_d;";
		
		for(Argument a : n.outputs.values())
			pml+="\n\tdata "+a.path.id+"_d;";
		
		for(Argument a : n.inputs.values())
			pml+="\n\t"+a.path.id+"?"+a.path.id+"_d;";
		
		if(ds.map.tms.get(n).choices.size()>1) {
			pml+="\n\tgd";
			for(TaskMapping.Choice c : ds.map.tms.get(n).choices.values()) {
				pml+="\n\t::f."+c.id+" -> proc = "+c.processor.id+"_ID; pu = "+c.processor.parent.id+"_ID;";
			}
			pml+="\n\tdg;";
		}
		
		for(Argument a : n.outputs.values()) {
			if(ds.map.pms.get(a.path).choices.size()>1) {
				pml+="\n\tgd";
				for(PathMapping.Choice c : ds.map.pms.get(a.path).choices.values())
					pml+="\n\t::f."+c.id+" -> "+a.path+"_d.loc = "+c.memory.id+"_ID;";
				pml+="\n\tdg;";
			}
			else
				pml+="\n\t"+a.path+"_d.loc = "+ds.map.pms.get(a.path).choices.values().iterator().next().memory.id+"_ID;";
		}
		
		pml+="\n\tcmd.fct = proc;";
		
		int i;
		i=0;
		for(Argument a : n.inputs.values())
			pml+="\n\tdata_copy(cmd.ins["+(i++)+"], "+a.path.id+"_d)";
		pml+="\n\tcmd.nb_ins = "+i+";";
		
		i=0;
		for(Argument a : n.outputs.values()) {
			pml+="\n\t"+a.path.id+"_d.id = "+(gen_id++)
					+";"+(ds.map.pms.get(a.path).choices.size()==1? a.path.id+"_d.loc = "+ds.map.pms.get(a.path).choices.keySet().iterator().next().id+"_ID;":"");
			pml+="\n\tdata_copy(cmd.outs["+(i++)+"], "+a.path.id+"_d)";
		}
		pml+="\n\tcmd.nb_outs = "+i+";";
		
		pml+="\n\tsend[pu]!cmd;\n\trecv[pu]?cmd;";
		
		if(!n.outputs.isEmpty()) {
			i = 0;
			for(Argument a : n.outputs.values())
				pml+="\n\tdata_copy("+a.path.id+"_d, cmd.outs["+(i++)+"])"; 
		}
		
		for(Argument o : n.outputs.values())
			pml+="\n\t"+o.path.id+"!"+o.path.id+"_d;";
		
		if(n.isOptional)
			pml+="\n::else -> skip;\ndg;";
		
		pml+="\n\t"+n.id.toLowerCase()+"_end = true;";
		pml+="\n};";
		pml+="\n}\n";
	}

	private int gen_id;
	private String pml;
	private DesignSpace ds;
	private NonFunctionalCst cst;
	private FunctionCost fc;
}
