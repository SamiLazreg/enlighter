package reasoning;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.naming.OperationNotSupportedException;

import application.Application;
import deployment.Mapping;
import deployment.MappingAlgorithm;
import designspace.DesignSpace;
import designspace.VariabilityTree.Domain;
import designspace.VariabilityTree.VariabilityNode.Variability;
import platform.Component;
import splar.core.fm.FeatureModel;
import splar.core.fm.FeatureModelException;
import splar.core.fm.XMLFeatureModel;
import splar.core.heuristics.FTPreOrderSortedECTraversalHeuristic;
import splar.core.heuristics.VariableOrderingHeuristic;
import splar.core.heuristics.VariableOrderingHeuristicsManager;
import splar.plugins.reasoners.bdd.javabdd.FMReasoningWithBDD;
import splar.plugins.reasoners.bdd.javabdd.ReasoningWithBDD;

public class Main {

	public static void main(String[] args) {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ObjectInputStream ois = null;
		
		System.out.println("************************* EnLighter Beta v0 *************************");
		
		System.out.println("*** Enter application file ***");
		Application app = null;
		while(app == null) {
			try {
				ois = new ObjectInputStream(new FileInputStream(br.readLine()));
				app = (Application)ois.readObject();
				ois.close();
			} catch (ClassNotFoundException e) {
				System.out.println("*** Error : not an valid application  ***");
				//e.printStackTrace();
			} catch (FileNotFoundException e) {
				System.out.println("*** Error : file not found ***");
				//e.printStackTrace();
			} catch (IOException e) {
				System.out.println("*** Error : IO exception ***");
				//e.printStackTrace();
			}
		}
		
		System.out.println("*** Enter platform file ***");
		Component plt = null;
		while(plt == null) {
			try {
				ois = new ObjectInputStream(new FileInputStream(br.readLine()));
				plt = (Component)ois.readObject();
				ois.close();
			} catch (ClassNotFoundException e) {
				System.out.println("*** Error : not an valid patform  ***");
				//e.printStackTrace();
			} catch (FileNotFoundException e) {
				System.out.println("*** Error : file not found ***");
				//e.printStackTrace();
			} catch (IOException e) {
				System.out.println("*** Error : IO exception ***");
				//e.printStackTrace();
			}
		}
		
		MappingAlgorithm mapAlg = new MappingAlgorithm();
		Mapping mapping = mapAlg.map(app, plt);
		DesignSpace ds = new DesignSpace(app, mapping, plt);
		
		System.out.println("*** Variability-Intensive System Design Space Created ***");
		printStats(ds);
		
		System.out.println("\n*** Structural analysis ***\n\t\t -soo <{min obj1, max obj1},...,{min objn, max objn}> prune system design space to Pareto Optimal system design\n\t\t -scst <structural_cost_constraint> constraint system design space");
		System.out.println("\n*** behavioral analysis ***\n\t\t -bfct prune to executable system design\n\t\t -boo <cost_function> prune to optimal executable system\n\t\t -bcst <behavioral_cost_constraints> prune executable system through behavioral constraints");
		
		
	}
	
	public static void printStats(DesignSpace ds) {
		
		PrintWriter writer;
		ToSXFM t = new ToSXFM();
		try {
			writer = new PrintWriter("./_app.sxfm", "UTF-8");
			writer.print(t.DSToSXFM(ds.tree, Domain.APP));
			writer.close();
			
			writer = new PrintWriter("./_plt.sxfm", "UTF-8");
			writer.print(t.DSToSXFM(ds.tree, Domain.PLT));
			writer.close();
			
			writer = new PrintWriter("./_ds.sxfm", "UTF-8");
			writer.print(t.DSToSXFM(ds.tree, Domain.DS));
			writer.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		FeatureModel appfeatureModel = new XMLFeatureModel("./_app.sxfm", XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);		
		try {
			appfeatureModel.loadModel();
		} catch (FeatureModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new FTPreOrderSortedECTraversalHeuristic("Pre-CL-MinSpan", appfeatureModel, FTPreOrderSortedECTraversalHeuristic.FORCE_SORT);		
		VariableOrderingHeuristic heuristic = VariableOrderingHeuristicsManager.createHeuristicsManager().getHeuristic("Pre-CL-MinSpan");

		int 	bddNodeNum 		= 50000;  	// sets the initial size of the BDD table  
		int 	bddCacheSize 	= 50000;  	// sets the size of the BDD cache table
		long	maxBuildingTime = 60000L;	// sets the maximum building time

		ReasoningWithBDD reasoner = new FMReasoningWithBDD(appfeatureModel, heuristic, bddNodeNum, bddCacheSize, maxBuildingTime, "pre-order");
		try {
			reasoner.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			System.out.println("Application is " + (reasoner.isConsistent()? "" : " NOT ") + "consistent!");
			System.out.println("Application has " + reasoner.countValidConfigurations() + " possible data-flow variants");
		} catch (OperationNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
		FeatureModel pltfeatureModel = new XMLFeatureModel("./_plt.sxfm", XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);		
		try {
			pltfeatureModel.loadModel();
		} catch (FeatureModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new FTPreOrderSortedECTraversalHeuristic("Pre-CL-MinSpan", pltfeatureModel, FTPreOrderSortedECTraversalHeuristic.FORCE_SORT);		
		heuristic = VariableOrderingHeuristicsManager.createHeuristicsManager().getHeuristic("Pre-CL-MinSpan");

		reasoner = new FMReasoningWithBDD(pltfeatureModel, heuristic, bddNodeNum, bddCacheSize, maxBuildingTime, "pre-order");
		try {
			reasoner.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			System.out.println("Platform is " + (reasoner.isConsistent()? "" : " NOT ") + "consistent!");
			System.out.println("Platform has " + reasoner.countValidConfigurations() + " possible target configuration");
		} catch (OperationNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
		FeatureModel dsfeatureModel = new XMLFeatureModel("./_ds.sxfm", XMLFeatureModel.USE_VARIABLE_NAME_AS_ID);		
		try {
			dsfeatureModel.loadModel();
		} catch (FeatureModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new FTPreOrderSortedECTraversalHeuristic("Pre-CL-MinSpan", dsfeatureModel, FTPreOrderSortedECTraversalHeuristic.FORCE_SORT);		
		heuristic = VariableOrderingHeuristicsManager.createHeuristicsManager().getHeuristic("Pre-CL-MinSpan");
		
		reasoner = new FMReasoningWithBDD(dsfeatureModel, heuristic, bddNodeNum, bddCacheSize, maxBuildingTime, "pre-order");
		try {
			reasoner.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			System.out.println("Design space is " + (reasoner.isConsistent()? "" : " NOT ") + "consistent!");
			System.out.println("Design space " + reasoner.countValidConfigurations() + " possible system design");
		} catch (OperationNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}

}
