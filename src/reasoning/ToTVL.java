package reasoning;

import java.util.List;
import java.util.Map;

import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.ConstraintNode;
import designspace.VariabilityTree.VariabilityNode;
import designspace.VariabilityTree.VariabilityNode.Type;
import designspace.VariabilityTree.VariabilityNode.Variability;

//DE TOUTE BEAUTE

public class ToTVL{
	
	public String DStoTVL(DesignSpace ds) {
		tvl = "//"+ds.nbDesigns+"\nroot DesignSpace{\n\tgroup allOf{";
		_toTVL(ds.tree.root, 2);
		tvl += "\n\t}";
		_cstTree(ds.tree);
		tvl += "\n}";
		return tvl;
	}

	private String _tab(int level) {
		String res = "\n";
		for(int i = 0; i < level; ++i)
			res+="\t";
		return res;
	}
	
	private void _toTVL(VariabilityNode node, int level) {
		
		if(node.type == Type.GROUP && node.children.isEmpty())
			return;
		
		tvl+=_tab(level)+(node.variability == Variability.OPT? "opt ":"")+node.id;
		switch(node.type) {
		case XOR:
			tvl+=" group oneOf{";
			break;
		case GROUP:
			tvl+=" group allOf{";
			break;
		case LEAF:
			tvl+=!node.isLast()?",":"";
			break;
		}
		for(VariabilityNode n : node.children)
			_toTVL(n, level+1);
		
		if(node.type == Type.XOR || node.type == Type.GROUP)
			tvl+=_tab(level)+"}"+(!node.isLast()?",":"");
	}
	
	private void _cstTree(VariabilityTree tree) {
		for(Map.Entry<String, List<ConstraintNode>> e : tree.constraints.entrySet()) {
			tvl+="\n\t/*"+e.getKey()+" constraints*/";
			for(ConstraintNode n : e.getValue())
				tvl+="\n\t"+_cstNode(n)+";";
			tvl+="\n";
		}
		tvl+="\n\t/*invalid products*/";
		for(ConstraintNode n : tree.prunings)
			tvl+="\n\t"+_cstNode(n)+";";
	}
	
	private String _cstNode(ConstraintNode n) {
		String res = "";
		switch(n.type) {
		case REQ:
			assert(n.nodes.size() == 2);
			res += _cstNode(n.nodes.get(0))+" -> "+_cstNode(n.nodes.get(1));
			break;
		case IFF:
			assert(n.nodes.size() == 2);
			res += _cstNode(n.nodes.get(0))+" <-> "+_cstNode(n.nodes.get(1));
			break;
		case NOT:
			assert(n.nodes.size() == 1);
			res += "!"+_cstNode(n.nodes.get(0));
			break;
		case LEAF:
			assert(n.nodes == null);
			res += n.id;
			break;
		case AND:
			assert(n.nodes.size() >= 1);
			res += "(";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+" && ";
			res += _cstNode(n.nodes.get(i))+")";
			break;
		case OR:
			assert(n.nodes.size() >= 1);
			res += "(";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+" || ";
			res += _cstNode(n.nodes.get(i))+")";
			break;
		}
		return res;
	}
	
	
	
	private String tvl;
}
