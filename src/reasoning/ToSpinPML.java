package reasoning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import application.Application;
import application.DataSource;
import application.Node;
import application.Node.Argument;
import application.Node.Type;
import application.Path;
import application.Task;
import deployment.DataSourceMapping;
import deployment.PathMapping;
import deployment.TaskMapping;
import designspace.DesignSpace;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Processor.Port;
import platform.Storage;
import platform.Storage.Capacity;
import requirements.NonFunctionalCst;
import splar.core.constraints.Assignment;

public class ToSpinPML {

	public Collection<String> DStoFctFA(DesignSpace ds, ToSXFM fm) {
		instances = new ArrayList<>();
		this.ds = ds;
		
		pml = "";
		
		_createFeatureStruct();
		
		_defineMemoriesId(ds.plt.getMemories());
		
		if(!ds.plt.getStorages().isEmpty()) _defineStorages();
		
		_defineProcessors();
		
		_defineNodes();
		
		Iterator<Assignment> it = null;
		try {
			it = fm.reasoner.iterateOverValidConfigurations();
		} catch (OperationNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(it.hasNext()) {
			Assignment d = it.next();
			String tmp = _dfsTrans(ds.tree.root, d);
			String instance = pml;
			instance+="\nactive proctype reachability(){ atomic{";
			instance+=tmp;
			instance+="\nprojeted = true;";
			for(Node n : ds.app.getSortedApplication()) {
				instance+="\n\t"+n.id+"_end;";
			}
			instance+="\n\treachable = true;\n};}\n\n";
			instances.add(instance);
		}
		
		return instances;
	}
	
	private String _dfsTrans(VariabilityNode n, Assignment d) {
		String res = "\t";
		
		res+= (d.getVariableValue(n.id+"_id") == 1)? "\nf."+n.id+" = true; ": "\nf."+n.id+" = false; ";
		
		for(VariabilityNode c : n.children)
			res+=_dfsTrans(c, d);
		return res;
	}

	private void _createFeatureStruct() {
		pml+="typedef features{";
		_createFeatures(ds.tree.root);
		pml=pml.substring(0, pml.length() - 1);
		pml+="\n}\nfeatures f;";
		
		pml+="\ntypedef data{\n\tbyte id;\n\tbyte loc;\n\tint size\n}\n";
		pml+="\nbool reachable = false;";
		pml+="\nbool projeted = false;";
	}

	private void _createFeatures(VariabilityNode n) {
		if(n.type == VariabilityNode.Type.GROUP && n.children.isEmpty())
			return;
		pml+="\n\tbool "+n.id+";";
		for(VariabilityNode c : n.children)
			_createFeatures(c);
	}
	
	private void _defineMemoriesId(Collection<Memory> ms) {
		int i = 1;
		for(Memory m : ms)
			pml+="\n#define "+m.id+"_ID "+i++;
		pml+="\n";
	}
	
	private void _defineStorages() {
		Collection<Memory> storages = ds.plt.getStorages();
		
		for(Memory s : storages)
			pml+="\nchan "+s.id+"claim = [0] of {data};";
		
		pml+="\nactive proctype memory(){ atomic{\n";
		
		pml+="\nprojeted;";
		
		pml+="\tbyte addr = 1;";
		pml+="\n\tdata in;";
		for(Memory s : storages) {
	    	pml+="\n\tint "+s.id+"_size = 0;";
	    	pml+="\n\tint "+s.id+"_maxSize = 0;";
		}
		
		pml+="\n\tdo";
		
		for(Memory m : storages) {
			Storage s = (Storage)m;
			pml+="\n\t:: "+s.id+"claim?in -> \n\t\t"+s.id+"_size = "+s.id+"_size+in.size;";
		    if(s.capacities.size() > 1){
                   pml+="\n\t\tif";
                   for(Capacity i : s.capacities){
                	   pml+="\n\t\t::f."+s.id+"_capacity_"+i.value+";if::"+s.id+"_maxSize = "+i.value+";fi;";
                       //pml+=" cost += "+i.cost+";fi;";
                   }
                   pml+="\n\t\t:: else -> assert(0);\n\t\tfi;";
		    }
		    else
		    	for(Capacity i : s.capacities) {
		           pml+="\n\t\t"+s.id+"_maxSize = "+i.value+";";
		           //pml+=" cost += "+i.cost+";fi;";
		    	}
		    //pml+="\n\t\tassert("+_toPML(cst)+");";
		    pml+="\n\t\tassert(in.loc == "+s.id+"_ID);";
		    pml+="\n\t\tassert("+s.id+"_size <= "+s.id+"_maxSize);";
		    pml+="\n\t\tin.id = addr; addr=addr+1; "+s.id+"claim!in;";
		}
		pml+="\n\t::reachable == true -> break;";
		pml+="\n\tod;\n};}\n";
	}
	
	private void _defineProcessors() {

		Component c = ds.plt;
		
		ins = new HashMap<>();
		outs = new HashMap<>();
		for(Processor p : c.getProcessors()){
			int i = 0;
			ins.put(p, new HashMap<>());
			for(Port port : p.inputs.values())
				ins.get(p).put(i++, port);
			i = 0;
			outs.put(p, new HashMap<>());
			for(Port port : p.outputs.values())
				outs.get(p).put(i++, port);
		}
		
        for(Processor p : c.getProcessors()){
            pml+="\nchan "+p.id+"_func = [0] of {";
            for(int i = 0; i < p.inputs.size(); ++i)
            	   pml+="data"+( i<p.inputs.size()-1 || !p.outputs.isEmpty()?",":"");
            for(int i = 0; i < p.outputs.size(); ++i)
            	   pml+="data"+(i < p.outputs.size()-1?",":"");
            pml+="};";
        }
        
        pml+="\n";
		
        pml+="\nactive proctype platform(){ atomic{\n";
        
        int maxInArgs = 0;
        int maxOutArgs = 0;
		for(Processor p : c.getProcessors()){
	       if(p.inputs.size() > maxInArgs)
	    	   maxInArgs = p.inputs.size();
	       if(p.outputs.size() > maxOutArgs)
	    	   maxOutArgs = p.outputs.size();
        }
		
        pml+="\n\tdata _in["+maxInArgs+"];";
        pml+="\n\tdata _out["+maxOutArgs+"];";

        for(Memory s : c.getFIFOBuffers()){
        	pml+="\n\tbyte "+s.id+"_REG = 0;";
        }
        
        pml+="\nprojeted;";
        
        pml+="\n\tdo";
        
       for(Processor p : c.getProcessors()){
    	   pml+="\n\t:: "+p.id+"_func?";
    	   for(int i = 0; i < p.inputs.size(); ++i)
                pml+="_in["+i+"]"+(((i<p.inputs.size()-1)||p.outputs.size() > 0)?",":"");
           for(int i = 0; i < p.outputs.size(); ++i)
                pml+="_out["+i+"]"+((i<p.outputs.size()-1)?",":"");
           pml+=";";
                             
           //check in
           for(Map.Entry<Integer, Port> e : ins.get(p).entrySet()) {      	   
        	   pml+="\n\t\tif\n\t\t:: ";
                                            
               //fetch from external storage
				Iterator<Memory> itt = p.getInputStorages(e.getValue().id).iterator();
				for(Memory m : p.getInputStorages(e.getValue().id)){
	               itt.next();
	               pml+="(_in["+e.getKey()+"].loc == "+m.id+"_ID)"+((itt.hasNext())?" || ":"");
				}
				
				if(p.getInputStorages(e.getValue().id).size() > 0 && p.getInputFIFObuffers(e.getValue().id).size() > 0)
					pml += " && ";
				
				//front pipeline free?
				itt = p.getInputFIFOBuffersPath(e.getValue().id).iterator();
				for(Memory st : p.getInputFIFOBuffersPath(e.getValue().id)){
	               itt.next();
	               pml+="("+st.id+"_REG == 0)"+((itt.hasNext())?" && ":"");
				}                            
				pml+=" -> skip;";
           
				//fetch from internal storage
				if(!p.getInputFIFOBuffersPath(e.getValue().id).isEmpty()){
					pml+="\n\t\t:: ";
						itt = p.getInputFIFOBuffersPath(e.getValue().id).iterator();
						for(Memory m : p.getInputFIFOBuffersPath(e.getValue().id)){
							itt.next();
							pml+="((_in["+e.getKey()+"].loc == "+m.id+"_ID) && ("+m.id+"_REG == _in["+e.getKey()+"].id))"+((itt.hasNext())?" || ":"");
						}
						pml+=" -> skip;";
				}
	           pml+="\n\t\t:: else -> assert(0);\n\t\tfi;";
	
	           pml+="\n\t\t_out[0].size = _in[0].size;";

           }
           
           for(Map.Entry<Integer, Port> e : outs.get(p).entrySet()) {
        	   pml += "\n\t\tif";
        	   for(Memory s : p.getOutputStorages(e.getValue().id)) {
        		   pml+="\n\t\t:: ";
        		   pml+="(_out["+e.getKey()+"].loc == "+s.id+"_ID)";
        		   
        		   if(!p.getOutputStorages(e.getValue().id).isEmpty() && !p.getOutputFIFOBuffers(e.getValue().id).isEmpty())
        			   pml+=" && ";
        		   
        		   Iterator<Memory> it = p.getOutputFIFOBuffers(e.getValue().id).iterator();
        		   for(Memory m : p.getOutputFIFOBuffers(e.getValue().id)) {
        			   it.next();
        			   pml += "("+m.id+"_REG == 0)"+(it.hasNext()?" && ":"");
        		   }
        		   
        		   pml+=" -> "+s.id+"claim!_out["+e.getKey()+"]; "+s.id+"claim?_out["+e.getKey()+"];";
        	 }
        	 
        	   Iterator<Memory> it = p.getOutputFIFOBuffers(e.getValue().id).iterator();
        	   for(Memory m : p.getOutputFIFOBuffers(e.getValue().id)) {
        		   it.next();
        		   pml+="\n\t\t::(_out["+e.getKey()+"].loc == "+m.id+"_ID) && ("+m.id+"_REG == 0) -> "+m.id+"_REG = _out["+e.getKey()+"].id;";
        	   }
        	   pml+="\n\t\t::else -> assert(0);\n\t\tfi;";
          }
           pml+="\n\t\t"+p.id+"_func!";
           for(int i = 0; i < p.inputs.size(); ++i)
               pml+="_in["+i+"]"+(((i<p.inputs.size()-1)||p.outputs.size() > 0)?",":"");
           for(int i = 0; i < p.outputs.size(); ++i)
               pml+="_out["+i+"]"+((i<p.outputs.size()-1)?",":"");
           pml+=";";
       }
       pml+="\n\t::reachable == true -> break;\n\tod;";
       pml+="\n};}\n";
       pml+="\n";
	}
	
	private void _defineNodes() {
		Collection<Path> paths = ds.app.getPaths();
		for(Path p : paths)
			pml+="\nchan "+p.id+" = [0] of {data};";
		
		for(Node n : ds.app.getSortedApplication()) {
			pml+="bool "+n.id+"_end = false;\n";
		}
		
		for(Node n : ds.app.getSortedApplication()) {
			if(n.type == Type.TASK)
				_defineTask((Task)n);
			else if(n.type == Type.DATA_SOURCE)
				_defineInputData((DataSource)n);
		}
		
		
	}
	
	private void _defineInputData(DataSource d) {
		pml+="\nactive proctype "+d.id+"(){ atomic{";
		
		pml+="\nprojeted;";
		
		if(d.isOptional)
			pml+="\nif::f."+d.id+"; if::";
		
		pml+="\n\tdata in;";
		if(d.sizes.size() > 1) {
			pml+="\n\tif";
			for(DataSource.Size s : d.sizes)
				pml+="\n\t::f."+d.id+"_size_"+s.value+"-> if::in.size = "+s.value+";fi;";
			pml+="\n\tfi;";
		}else {
			for(DataSource.Size s : d.sizes)
				pml+="\n\tin.size = "+s.value+";";
		}
		
		pml+="\n\tif";
		for(DataSourceMapping.Choice c : ds.map.dms.get(d).choices.values()) {
			pml+="\n\t::f."+c.id+"-> ";
			pml+="\n\t\tin.loc = "+c.storage+"_ID;";
			pml+="\n\t\t"+c.storage+"claim!in;"+c.storage+"claim?in;";
		}
		pml+="\n\tfi;";
		
		for(Argument a : d.outputs.values()) {
			pml+="\n\t"+a.path+"!in;";
		}
		
		if(d.isOptional)
			pml+="\nfi::else -> skip;\nfi";
		
		pml+="\n\t"+d.id+"_end=true;\n};}\n";
		
	}

	private void _defineTask(Task t) {
		pml+="\nactive proctype "+t.id+"(){ atomic{";
		
		pml+="\nprojeted;";
		
		if(t.isOptional)
			pml+="\nif::f."+t.id+"-> if::";
		
		for(Argument a : t.inputs.values())
			pml+="\n\tdata "+a.id+";";
		
		for(Argument a : t.outputs.values())
			pml+="\n\tdata "+a.id+";";
		
		for(Argument a : t.inputs.values())
			pml+="\n\t"+a.path.id+"?"+a.id+";";
		
		for(Argument a : t.outputs.values()) {
			pml+="\n\tif";
			for(PathMapping.Choice c : ds.map.pms.get(a.path).choices.values())
				pml+="\n\t::f."+c.id+"-> if::"+a.id+".loc = "+c.memory+"_ID;fi;";
			pml+="\n\tfi;";
		}
		
		pml+="\n\tif";
		for(TaskMapping.Choice c : ds.map.tms.get(t).choices.values()) {
			pml+="\n\t::f."+c.id+"-> if::"+c.processor.id+"_func!";
			Collection<Argument> sortedArgs = _mapInputArguments(t.inputs.values(), c.processor);
			sortedArgs.addAll(_mapOutputArguments(t.outputs.values(), c.processor));
			Iterator<Argument> it = sortedArgs.iterator();
			for(Argument a : sortedArgs) {
				it.next();
				pml+=a.id+(it.hasNext()? ",": ";");
			}
			pml+=c.processor.id+"_func?";
			it = sortedArgs.iterator();
			for(Argument a : sortedArgs) {
				it.next();
				pml+=a.id+(it.hasNext()? ",": ";");
			}
			pml+="fi;";
			
		}
		pml+="\n\tfi;";
		
		for(Argument o : t.outputs.values())
			pml+="\n\t"+o.path.id+"!"+o.id+";";
		
		if(t.isOptional)
			pml+="\nfi::else -> skip;\nfi;";
		
		pml+="\n\t"+t.id+"_end=true;\n};}\n";
	}

	private Collection<Argument> _mapInputArguments(Collection<Argument> args, Processor p){
		Collection<Argument> res = new ArrayList<Argument>();
		for(Processor.Port port : p.inputs.values())
			for(Argument a : args)
				if(port.id.equals(a.id))
					res.add(a);
		return res;
	}
	
	private Collection<Argument> _mapOutputArguments(Collection<Argument> args, Processor p){
		Collection<Argument> res = new ArrayList<Argument>();
		for(Processor.Port port : p.outputs.values())
			for(Argument a : args)
				if(port.id.equals(a.id))
					res.add(a);
		return res;
	}
	
	private String _toPML(NonFunctionalCst n) {
		String res = "";
		switch(n.type) {
		case REQ:
			assert(n.nodes.size() == 2);
			//todo
			assert(false);
			//res += _toPML(n.nodes.get(0))+" -> "+_toPML(n.nodes.get(1));
			break;
		case IFF:
			assert(n.nodes.size() == 2);
			//todo
			assert(false);
			//res += _toPML(n.nodes.get(0))+" <-> "+_toPML(n.nodes.get(1));
			break;
		case NOT:
			assert(n.nodes.size() == 1);
			res += "!"+_toPML(n.nodes.get(0));
			break;
		case LEAF:
			assert(n.nodes == null);
			res += _toPMLLeaf(n);
			break;
		case AND:
			assert(n.nodes.size() >= 1);
			res += "(";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _toPML(n.nodes.get(i))+"&&";
			res += _toPML(n.nodes.get(i))+")";
			break;
		case OR:
			assert(n.nodes.size() >= 1);
			res += "(";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _toPML(n.nodes.get(i))+"||";
			res += _toPML(n.nodes.get(i))+")";
			break;
		default:
			assert(false);
		}
		return res;
	}
	

	private String _toPMLLeaf(NonFunctionalCst n) {
		String nfp = null;
		switch(n.nfp) {
		case NonFunctionalCst._COST:
			nfp = "cost";
			break;
		case NonFunctionalCst._QUALITY_LOSS:
			nfp = "quality";
			break;
		default:
			assert(false);
		}
		
		switch(n.op) {
		case EQ:
			nfp+=" == ";
			break;
		case G:
			nfp+=" > ";
			break;
		case GEQ:
			nfp+=" >= ";
			break;
		case L:
			nfp+=" < ";
			break;
		case LEQ:
			nfp+=" <= ";
			break;
		case NA:
			assert(false);
			break;
		default:
			break;
		}
		
		return nfp+=n.value;
	}
	
	private String pml;
	private Collection<String> instances; 
	private DesignSpace ds;
	private Map<Processor, Map<Integer, Port>> ins;
	private Map<Processor, Map<Integer, Port>> outs;
}


