package reasoning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

import designspace.DesignSpace;
import designspace.VariabilityTree.Domain;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;

public class Reasoning {
	
	//public static int VF_S is done on the mapping stage
	public enum Verification{
		VF_C,
		VNF_CS_S,
		VNF_CS_C,
		VNF_OO_S,
		VNF_OO_C;
	}
	
	public void reason(String name, DesignSpace ds, NonFunctionalCst nfr, FunctionCost cfct, Verification... verifications) throws IOException {
		
		this.name = name;
		this.ds = ds;
		fm = new ToSXFM();
		fm.DSToSXFM(name, ds, Domain.DS);
		
		boolean validFct = false;
		
		for(Verification verif : verifications)
		{
			switch(verif) {
			case VF_C:
				break;
			case VNF_CS_C:
				break;
			case VNF_CS_S:
				break;
			case VNF_OO_C:
				break;
			case VNF_OO_S:
				break;
			default:
				break;
			
			}
		}
		
		
		
		
		functionalValidation(ds);
		
		
		
		/*nonfunctionalValidation(ds, nfr);
		
		optimalDesignUnderCst(ds, cfct, nfr);*/
	
	}
	
	DesignSpace functionalValidation(DesignSpace ds) throws IOException {
		ToPML toPML = new ToPML();
		ToPMLOpt toPMLOpt = new ToPMLOpt();
		
		String tvl = new ToTVL().DStoTVL(ds);
		
		PrintWriter writer = new PrintWriter("./ProVeLines/"+name+".tvl", "UTF-8");
		writer.print(tvl);
		writer.close();
		
		writer = new PrintWriter("./ProVelines/"+name+".pml", "UTF-8");
		writer.print(toPML.DStoFctFA(ds));
		writer.close();
		
		writer = new PrintWriter("./ProVelinesOpt/"+name+".opt.pml", "UTF-8");
		writer.print(toPMLOpt.DStoFctFA(ds));
		writer.close();
		
		writer = new PrintWriter("./ProVelinesOpt/"+name+".opt.tvl", "UTF-8");
		writer.print(tvl);
		writer.close();
		
		int i = 0;
		Collection<String> PbP = new ToSpinPML().DStoFctFA(ds, fm);
		for(String p : PbP) {
			writer = new PrintWriter("./SPIN/"+name+"/"+name+"_"+(i++)+".pml");
			writer.write(p);
			writer.flush();
		}
		
		writer = new PrintWriter("./SPIN/"+name+".sh");
		writer.write("#!/bin/bash\n for i in {0.."+((int)ds.nbDesigns)+"}\n do\n\t ./spin "+name+"_$i.pml\n done");
		writer.flush();
		
		
		//_removeDesigns();
		
		return ds;
	}
	
	private void _removeDesigns() throws IOException {
		Collection<String> str = null;
		str = Files.readAllLines(Paths.get("./"+name+"_fct_invalid.txt"));
		ds.tree.removeDesigns(str);
	}

	DesignSpace nonfunctionalValidation(DesignSpace ds, NonFunctionalCst cst) throws FileNotFoundException, UnsupportedEncodingException {
		String res = new ToCfrMOO().DStoCfrMOO(ds, cst);
		System.out.println(res);
		PrintWriter writer = new PrintWriter(name+".cfr", "UTF-8");
		writer.print(res);
		writer.close();
		return null;
	}
	
	String optimalDesign(DesignSpace ds, ToSXFM fm, FunctionCost fc) throws FileNotFoundException, UnsupportedEncodingException {
		return optimalDesignUnderCst(ds, fm, fc, null);
	}
	
	String optimalDesignUnderCst(DesignSpace ds, ToSXFM fm, FunctionCost fc, NonFunctionalCst cst) throws FileNotFoundException, UnsupportedEncodingException {
		
		/*************** UPPAAL CORA **************/
		
		ToFPTA toFPTA = new ToFPTA();
		
		PrintWriter writer = new PrintWriter("./UPPAAL-CORA/"+name+".fpta.xml", "UTF-8");
		writer.print(toFPTA.SXFMtoFPTA(name+".sxfm.xml", ds, fm, cst, fc));
		writer.close();
		
		writer = new PrintWriter("./UPPAAL-CORA/"+name+".fpta.q", "UTF-8");
		writer.print(toFPTA.queries);
		writer.close();
		
		/*************** ProVeLines CORA ***********/
		
		ToNF_PML toNf_PML = new ToNF_PML();
		
		writer = new PrintWriter("./ProVeLines-CORA/"+name+".fpta.pml", "UTF-8");
		String res = toNf_PML.toNfPML(ds, cst, fc);
		System.out.println(res);
		writer.print(res);
		writer.close();
		
		writer = new PrintWriter("./ProVeLines-CORA/"+name+".fpta.tvl", "UTF-8");
		writer.print(new ToTVL().DStoTVL(ds.tree));
		writer.close();
		
		return null;
	}
	
	private String name;
	private DesignSpace ds;
	public ToSXFM fm;
}
