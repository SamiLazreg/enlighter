package reasoning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.rowset.spi.TransactionalWriter;

import application.DataSource;
import application.DataSource.Size;
import application.Node;
import application.Path;
import application.Task;
import deployment.DataSourceMapping;
import deployment.PathMapping;
import deployment.TaskMapping;
import deployment.DataSourceMapping.Choice;
import application.Node.Argument;
import application.Node.Type;
import designspace.DesignSpace;
import designspace.VariabilityTree.VariabilityNode;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Storage.Capacity;
import reasoning.ToFPTA.Template.Location;
import reasoning.ToFPTA.Template.Transition;
import requirements.FunctionCost;
import requirements.NonFunctionalCst;
import requirements.NonFunctionalCst.Op;
import splar.core.constraints.Assignment;
import splar.core.constraints.BooleanVariableInterface;
import splar.core.fm.FeatureModel;
import splar.core.fm.XMLFeatureModel;
import splar.core.heuristics.FTPreOrderSortedECTraversalHeuristic;
import splar.core.heuristics.VariableOrderingHeuristic;
import splar.core.heuristics.VariableOrderingHeuristicsManager;
import splar.plugins.reasoners.bdd.javabdd.FMReasoningWithBDD;
import splar.plugins.reasoners.bdd.javabdd.ReasoningWithBDD;
import xpgenerator.FunctionalDomain.Function;

public class ToFPTA {
	
	public static class Template{
		public static class Action{
			public Action(String id, int x, int y) {
				this.id = id;
				this.x = x;
				this.y = y;
			}
			public String id;
			public int x;
			public int y;
		}
		public static class Location{
			public Location(String id, int x, int y) {
				this.id = id;
				this.x = x;
				this.y = y;
			}
			public Location setCommitted() {
				assert(!urgent);
				committed = true;
				return this;
			}
			public Location setUrgent() {
				assert(!committed);
				urgent = true;
				return this;
			}
			public Location setLabeled(int x, int y) {
				labeled = new Action(id, x, y);
				return this;
			}
			public Location setInvariant(String expr, int x, int y) {
				assert(invariant == null);
				invariant = new Action(expr, x, y);
				return this;
			}
			public Location setPricedInvariant(String inv, String price, int x, int y) {
				assert(invariant == null);
				invariant = new Action(inv+" &amp;&amp; cost'== "+price, x, y);
				return this;
			}
			public Location setPriced(String price, int x, int y) {
				assert(invariant == null);
				invariant = new Action("cost'== "+price, x, y);
				return this;
			}
			public String toString() {
				String res = "\n\t<location id = \""+id+"\" x = \""+x+"\" y = \""+y+"\">";
				res+= labeled!=null? "<name x = \""+labeled.x+"\" y = \""+labeled.y+"\">"+labeled.id+"</name>" : "";
				res+= committed? "<committed/>" : "";
				res+= urgent? "<urgent/>" : "";
				res+= invariant!=null? "<label kind = \"invariant\" x = \""+invariant.x+"\" y = \""+invariant.y+"\">"+invariant.id+"</label>" : "";
				res+= "</location>";
				return res;
			}
			public String id;
			public int x;
			public int y;
			public boolean committed;
			public boolean urgent;
			public Action labeled;
			public Action invariant;
		}
		public static class Transition{
			public static class Nail{
				public Nail(int x, int y) {
					this.x = x;
					this.y = y;
				}
				public int x;
				public int y;
			}
			public Transition(String src, String dst) {
				this.src = src;
				this.dst = dst;
				guard = null;
				sync = null;
				assign = null;
				nails = new ArrayList<>();
			}
			public Transition setGuard(String g, int x, int y) {
				assert(guard == null);
				guard = new Action(g, x, y);
				return this;
			}
			public Transition setSync(String s, int x, int y) {
				assert(sync == null);
				sync = new Action(s, x, y);
				return this;
			}
			public Transition setAssign(String a, int x, int y) {
				assert(assign == null);
				assign = new Action(a, x, y);
				return this;
			}
			public Transition addNail(int x, int y) {
				nails.add(new Nail(x,y));
				return this;
			}
			public String toString() {
				String res = "\n\t<transition>\n\t\t<source ref = \""+src+"\"/>\n\t\t<target ref = \""+dst+"\"/>";
				res+=guard!=null?"\n\t\t<label kind = \"guard\" x = \""+guard.x+"\" y = \""+guard.y+"\">"+guard.id+"</label>" : "";
				res+=sync!=null?"\n\t\t<label kind = \"synchronisation\" x = \""+sync.x+"\" y = \""+sync.y+"\">"+sync.id+"</label>" : "";
				res+=assign!=null?"\n\t\t<label kind = \"assignment\" x = \""+assign.x+"\" y = \""+assign.y+"\">"+assign.id+"</label>" : "";
				for(Nail n : nails)
					res+="\n\t\t<nail x = \""+n.x+"\" y = \""+n.y+"\"/>";
				res+="\n\t</transition>";
				return res;
			}
			public String src;
			public String dst;
			public Action guard;
			public Action sync;
			public Action assign;
			public Collection<Nail> nails;
			
		}
		public Template(String name) {
			this.name = name;
			this.decl="";
			this.init=null;
			this.loc = new ArrayList<>();
			this.trans = new ArrayList<>();
		}
		public Location addLoc(String id, int x, int y) {
			Location n = new Location(id, x, y);
			loc.add(n);
			return n;
		}
		public Transition addTrans(String src, String dst) {
			Transition n = new Transition(src, dst);
			trans.add(n);
			return n;
		}
		public String toString() {
			StringBuilder b = new StringBuilder();
			b.append("\n<template>\n\t<name x = \"5\" y = \"5\">"+name+"</name>");
			b.append("\n\t<declaration>"+decl+"\n\t</declaration>");
			for(Location l : loc)
				b.append(l);
			assert(init!=null);
			b.append("\n\t<init ref = \""+init+"\"/>");
			for(Transition t : trans)
				b.append(t);
			b.append("\n</template>");
			return b.toString();
		}
		public String name;
		public Collection<Location> loc;
		public String init;
		public Collection<Transition> trans;
		public String decl;
	}
	
	public ToFPTA() {
		queries = "";
		uppaal = "";
		global = "";
		templates = new ArrayList<>();
		instances = "";
		system = "";
		gen_id = 1;
		systemMemories = "";
		systemCores = "";
		systemNodes = "";
	}
	
	public String SXFMtoFPTA(String name, DesignSpace ds, ToSXFM sxfm, NonFunctionalCst cst, FunctionCost fct) {

		this.ds = ds;
		this.cst = cst;
		this.fct = fct;
		
		try {
	
			
			queries += "E<> "; _genQueries();
			uppaal += "<?xml version=\"1.0\" encoding=\"utf-8\"?><!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_1.dtd'>\n<nta>\n<declaration>";

			Iterator<Assignment> it = sxfm.reasoner.iterateOverValidConfigurations();
			templates.add(_createFeatureStruct(ds, it));
			
			_defineTimeCost();
			
			_globalDefinitions();
			_defineNodes();
			if(!ds.plt.getStorages().isEmpty()) _defineStorages();
			_defineProcessors();
			
			queries = queries.substring(0, queries.length()-3);
			System.out.println(queries);
			
			uppaal += global+"\n</declaration>";
			for(Template t : templates)
				uppaal+=t;
			uppaal += instances;
			
			uppaal += "\nsystem "+systemNodes+" &lt; "+systemMemories+systemCores+" &lt; clock_t &lt; fm;\n</system>\n</nta>";
			//System.out.println(uppaal);
			//uppaal += system.substring(0, system.length()-1);
			// Important:
			// - My research focused on heuristics to reduce the size of BDDs as much as possible 
			//  (e.g. I proposed the "Pre-CL-MinSpan" heuristic above) 
			//   Therefore, there are plenty of opportunities for improving the performance of many BDD reasoning operations provided
			//   I actually know the algorithms but need time to code them
			//   For now, use the BDD to check consistency of models and to count valid configurations
		
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return uppaal;
	}
	
	private void _globalDefinitions() {
		global+="\n\nclock time;";
		global+="\nconst int main_freq = "+ds.plt.getMainCLock()+";";
		global+="\nconst int burst = 512;";
		global+="\nint p_id = 0;";
		
		global+="\n\nurgent chan proj;";
		
		_defineMemoriesId(ds.plt.getMemories());
		_defineProcessorId(ds.plt.getProcessors());
		_defineCommandSystem();
	}
	
	public Template _createFeatureStruct(DesignSpace ds, Iterator<Assignment> it) {
		
		global+= "\ntypedef struct{";
		global+= _createFeatures(ds.tree.root);
		global+= "\n}features;\nfeatures f;";
		global+= "\n//design number = "+ds.nbDesigns;
		global+= "\n\ntypedef struct{\nint qualityLoss;\nint cost;\n}design;\ndesign d = {0, 0};";
		
		Template res = new Template("DS_STRUCT");
		
		//String res = "\n<template><name x = \"5\" y = \"5\">DS_STRUCT</name><declaration></declaration>";
		
		int x1 = 0; final int x2 = 10000; int y = 0;
		final int dy = 25;
		int iy = 0;
		
		Location init = res.addLoc("fm_", x1, y).setCommitted();
		res.init="fm_";
		//res.addLoc("fm1", x2, y);
		/*res+="\n<location id = \"fm_0\" x = \""+x1+"\" y = \""+y+"\"><committed/></location>";
		res+="\n<location id = \"fm_1\" x = \""+x2+"\" y = \""+y+"\"></location>";
		res+="\n<init ref = \"fm_0\"/>";*/
		
		int i = 0;
		int nb_system = 8000;
		while(it.hasNext()) {
			iy+=dy;
			Assignment d = it.next();
			/*"P4_On_Soc_DCU_R1_id", "P2_On_Soc_DCU_R0_id"*/
			if(true/*sat(d, "", "P2_On_Soc_RAM_id")||sat(d, */) {
				String tmp = _dfsTrans(ds.tree.root, d);
				res.addLoc("fm_"+i, x2, y);
				res.addTrans("fm_", "fm_"+i).setAssign("p_id = "+(i++)+", "+tmp.substring(0, tmp.length()-2), x1, iy).addNail(x1, iy).addNail(x2, iy);
			}
		}
		//res.addLoc("end", 10100, 0);
		//res.addTrans("fm1", "end").setSync("proj!", 10100, 0);
		
		/*StringBuilder sb = new StringBuilder();
		while(it.hasNext()) {
			iy+=dy;
			Assignment d = it.next();
			sb.append("\n<transition><source ref=\"fm_0\"/><target ref=\"fm_1\"/><label kind = \"assignment\" x = \""+x1+"\" y = \""+iy+"\">");
			String tmp = _dfsTrans(ds.tree.root, d);
			sb.append(tmp.substring(0, tmp.length()-2));
			sb.append("</label><nail x = \""+x1+"\" y = \""+iy+"\"/><nail x = \""+x2+"\" y = \""+iy+"\"/></transition>");
		}*/
		
		instances+="\n<system>\nfm = DS_STRUCT();";
		return res;
	}

	private boolean sat(Assignment d, String... dcs) {
		for(String dc : dcs)
			if(d.getVariableValue(dc) != 1)
				return false;
		return true;
	}

	private String _dfsTrans(VariabilityNode n, Assignment d) {
		String res = "\t";
		
		res+= (d.getVariableValue(n.id+"_id") == 1)? "f."+n.id+"_id = true, ": "f."+n.id+"_id = false, ";
		
		/*Map<String, Integer> lambda = new HashMap<>();
		lambda.put("cost", fct.get(FunctionCost._COST));
		lambda.put("qualityLoss", fct.get(FunctionCost._QUALITY_LOSS));
		
		if(d.getVariableValue(n.id+"_id") == 1) {
			for(Entry<String, Integer> e : n.prices.entrySet()) {
				res+="d."+e.getKey()+" += "+e.getValue()+", ";
				//lambda.put(e.getKey(), lambda.get(e.getKey())+e.getValue());
			}
		}*/
		
		
		for(VariabilityNode c : n.children)
			res+=_dfsTrans(c, d);
		return res;
	}

	private String _createFeatures(VariabilityNode n){
		String res = "";
		res+="\nbool "+n.id+"_id;";
		for(VariabilityNode c : n.children)
			res+=_createFeatures(c);
		return res;
	}
	
	private void _defineTimeCost() {
		Template t = new Template("CLOCK_T");
		t.addLoc("counting", 0, 0).setPriced(""+fct.get(FunctionCost._RUN_TIME), 0, 10);
		t.init="counting";
		//t.addTrans("start", "counting").setSync("proj?", -100, -50).setAssign("cost+=d.cost * "+fct.get(FunctionCost._COST)+" + d.quality * "+fct.get(FunctionCost._QUALITY), -100, -25);
		templates.add(t);
		instances+="\nclock_t = CLOCK_T();";
	}
	
	
	private void _defineMemoriesId(Collection<Memory> ms) {
		int i = 1;
		global += "\n\nconst int NB_MEMORY = "+(ms.size()+1)+";";
		global += "\ntypedef int[0, NB_MEMORY] memory_id;";
		global += "\nconst memory_id NULL_MEMORY = 0;";
		for(Memory m : ms)
			global+="\nconst memory_id "+m.id+"_ID = "+(i++)+";";
		global+="\n";
	}
	
	private void _defineProcessorId(Collection<Processor> ps) {
		int i = 1;
		global += "\n\nconst int NB_PROCESSOR = "+(ps.size()+1)+";";
		global += "\ntypedef int[0, NB_PROCESSOR] processor_id;";
		global += "\nconst processor_id NULL_PROCESSOR = 0;";
		for(Processor p : ps)
			global+="\nconst processor_id "+p.id+"_ID = "+(i++)+";";
		i = 1;
		global+="\n\nconst int NB_PU = "+(ds.plt.getProcessingEngine().size()+1)+";";
		global += "\ntypedef int[0, NB_PU] pu_id;";
		global += "\nconst pu_id NULL_PU = 0;";
		for(Component c : ds.plt.getProcessingEngine()) {
			global+="\nconst pu_id "+c.id+"_ID = "+(i++)+";";
		}

		global+="\n";
	}
	
	private void _defineCommandSystem() {
		global+="\n\ntypedef int[0, "+ds.app.getDataSources().size()+ds.app.getPaths().size()+"] data_id;";
		global+="\n\ntypedef struct{\ndata_id id;\nmemory_id loc;\nint size;\n}data;"+
				"\nconst data empty_data = { 0, 0, 0};\r\n";
		//command
		int ins = 0; int outs = 0;
		for(Node n : ds.app.getNodes()) {
			if(n.inputs.size()>ins)
				ins = n.inputs.size();
			if(n.outputs.size()>outs)
				outs = n.outputs.size();
		}
		global+="\n\nconst int MAX_NB_INS = "+ins+";";
		
		String s_ins = "";
		for(int i = 0; i < ins; ++i)
			s_ins+="empty_data, ";
		s_ins = s_ins.substring(0, s_ins.length()-2);
		String s_outs = "";
		for(int i = 0; i < outs; ++i)
			s_outs+="empty_data, ";
		s_outs = s_outs.substring(0, s_outs.length()-2);
		
		global+="\nconst int MAX_NB_OUTS = "+outs+";";
		global+="\n\ntypedef struct{\nprocessor_id fct;\ndata ins[MAX_NB_INS];\nint nb_ins;\ndata outs[MAX_NB_OUTS];\nint nb_outs;\n}command;"+
				"\nconst command empty_cmd = { 0, { "+s_ins+" }, 0, { "+s_outs+" }, 0};";
		
		global+="\nconst int NB_TASK = "+ds.app.getTasks().size()+";\r\n" +
				"\nconst int LIST_LENGTH = NB_TASK;\r\n" +
				"typedef struct{\r\n" + 
				"int indexes[NB_TASK];\r\n" + 
				"int depth;\r\n" + 
				"}dependency;\r\n" + 
				"\r\n" + 
				"\ntypedef struct{\r\n" + 
				"command cmd[LIST_LENGTH];\r\n" + 
				"dependency dep[LIST_LENGTH];\r\n" + 
				"int size;\r\n" + 
				"}command_list;\r\n";

		//channel
		global+="\nurgent chan transfer[NB_MEMORY];";
		global+="\nurgent chan send[NB_PU];";
		global+="\nurgent chan recv[NB_PU];";
		global+="\ncommand cmd_buf;";
	}
	
	private void _genQueries() {
		if(cst == null)
			return;
		queries+= _genQuery(cst) + " && ";
	}
	
	private String _genQuery(NonFunctionalCst n) {
		String res = "";
		switch(n.type) {
		case AND:
			assert(n.nodes.size() >= 1);
			res += "(";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _genQuery(n.nodes.get(i))+" && ";
			res += _genQuery(n.nodes.get(i))+")";
			break;
		
		case IFF:
			assert(false);
			break;
		case LEAF:
			assert(n.nodes == null);
			res += _toString(n.nfp)+n.op+n.value;
			break;
			
		case NOT:
			assert(n.nodes.size() == 1);
			res += "!"+_genQuery(n.nodes.get(0));
			break;
			
		case OR:
			assert(n.nodes.size() >= 1);
			res += "(";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _genQuery(n.nodes.get(i))+" || ";
			res += _genQuery(n.nodes.get(i))+")";
			break;
			
		case REQ:
			assert(false);
			break;
		default:
			assert(false);
			break;
		
		}
		return res;
	}
	
	private String _toString(int nfp) {
		switch(nfp) {
		case NonFunctionalCst._NONE:
			assert(false);
		case NonFunctionalCst._QUALITY_LOSS:
			return "d.qualityLoss";
		case NonFunctionalCst._COST:
			return "d.cost";
		case NonFunctionalCst._RUN_TIME:
			return "time";
		}
		return null;
	}
	
	private void _defineStorages() {
		
		for(Memory m : ds.plt.getStorages()) {
			Template t = new Template(m.id.toUpperCase());
			t.decl += "\nclock clk;\nint freq"+(m.getFrequencies().size()>1? "": " = "+m.getFrequencies().iterator().next())+";";
			t.decl += "\nconst int bpc = "+m.bpc+";";
			t.decl += "\nconst int latency ="+((Storage)m).latency+";";
			t.decl += "\n\nvoid config(){\n\tif(false);";
			if(m.getFrequencies().size() > 1) {
				for(int freq : m.getFrequencies())
					t.decl += "\n\telse if(f."+m.getClockOwner().id+"_frequency_"+freq+"_id) freq = "+freq+";";
			}
			t.decl += "\n}";
			
			t.decl += "\n\nint time_trans(){ const int ratio = main_freq/freq; return (latency*ratio)+(burst/bpc*ratio);}";
			
			/*String costs = "";
			for(Entry<String, Integer> e : ds.tree.root.get(m.id).prices.entrySet()) {
				costs+="\n\td."+e.getKey()+" += "+e.getValue()+" * "+toCostFactor(e.getKey())+"," +
						"\n\tcost += "+e.getValue()+" * "+toCostFactor(e.getKey());
			}*/
		
			t.addLoc("proj", 0, -72).setUrgent().setLabeled(-10, -102);
			t.init = "proj";
			t.addLoc("idle", 128, 0).setLabeled(144, -16);
			t.addLoc("busy", 328, 0).setInvariant("clk&lt;=time_trans()", 344, -8).setLabeled(280, -16);
			Transition config = t.addTrans("proj", "idle").setAssign("config()", 40, 0).addNail(0, 0);
			if(m.isOptional()) {
				t.addTrans("proj", "idle").setGuard("!f."+m.id+"_id", -48, 24).addNail(0, 72).addNail(128, 72);
				config.setGuard("f."+m.id+"_id", 16, -24);
			}
			t.addTrans("idle", "busy").setSync("transfer["+m.id+"_ID]?", 152, -96).setAssign("clk = 0", 200, -72).addNail(128, -72).addNail(328, -72);
			t.addTrans("busy", "idle").setGuard("clk == time_trans()", 176, 48).addNail(328, 72).addNail(128, 72);
			
			templates.add(t);
			queries+=""+m.id.toLowerCase()+".idle && ";
			instances+="\n"+m.id.toLowerCase()+" = "+m.id.toUpperCase()+"();";
			systemMemories+=m.id.toLowerCase()+",";
		}
	}
	
	private int toCostFactor(String key) {
		switch(key) {
		case "cost":
			return fct.get(FunctionCost._COST);
		case "qualityLoss":
			return fct.get(FunctionCost._QUALITY_LOSS);
		}
		assert(false);
		return 0;
	}

	private void _defineProcessors() {
		for(Component c : ds.plt.getProcessingEngine()) {
			Template t = new Template(c.id.toUpperCase());
			t.decl+="\nclock clk;\nint freq"+(c.getFrequencies().size()>1? "": " = "+c.getFrequencies().iterator().next())+";";
			
			t.decl+="command_list list;\r\n" + 
					"int i_cmd;\r\n" + 
					"data ins[MAX_NB_INS]; int fetch[MAX_NB_INS]; int nb_ins = 0; int i_ins;\r\n" + 
					"data outs[MAX_NB_OUTS]; int store[MAX_NB_OUTS]; int nb_outs = 0; int i_outs;\r\n\n";
			
			if(c.getFrequencies().size() > 1) {
				t.decl += "\n\nvoid config(){\n\tif(false);";
				for(int freq : c.getFrequencies())
					t.decl += "\n\telse if(f."+c.getClockOwner().id+"_frequency_"+freq+"_id) freq = "+freq+";";
				t.decl += "\n}";
			}
			
			
			t.decl+="\n\nbool is_external(memory_id id){";
			String guard = "";
			if(!c.getFIFOBuffers().isEmpty()) {
				for(Memory m : c.getFIFOBuffers()) {
					guard+="id == "+m.id+"_ID || ";
				}
				if(!guard.equals(""))
					guard = guard.substring(0, guard.length()-4);
				t.decl+="\n\treturn !("+guard+");";
			}
			else t.decl+="\n\treturn true;";
			t.decl+="\n}";
			
			t.decl+="\r\n" + 
					"bool _inactive(command c){\r\n" + 
					"	return c.fct == 0 &amp;&amp; c.nb_ins == 0 &amp;&amp; c.nb_outs == 0; \r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"int _allocate(){\r\n" + 
					"	int i;int res;\r\n" + 
					"	for(i = 0; i &lt; list.size; ++i){\r\n" + 
					"		if(_inactive(list.cmd[i])){\r\n" + 
					"			list.dep[i].depth = 0;\r\n" + 
					"			list.cmd[i] = cmd_buf;\r\n" + 
					"\r\n" + 
					"			list.dep[i].indexes[  list.dep[i].depth++  ] = i;\r\n" + 
					"			return i;\r\n" + 
					"		}\r\n" + 
					"	}\r\n" + 
					"	res = list.size;\r\n" + 
					"	list.dep[res].depth = 0;\r\n" + 
					"	list.cmd[res] = cmd_buf;\r\n" + 
					"	\r\n" + 
					"	list.dep[i].indexes[  list.dep[i].depth++  ] = i;\r\n" + 
					"	++list.size;\r\n" + 
					"	return res;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void cmd_list_push(){\r\n" + 
					"	int i; int j; int k; int l;int m;\r\n" + 
					"	i = _allocate();\r\n" + 
					"	for(j = 0; j&lt;cmd_buf.nb_ins; ++j){\r\n" + 
					"		for(k = 0; k&lt;list.size; ++k){\r\n" + 
					"			for(l = 0; l&lt;list.cmd[k].nb_outs; ++l){\r\n" + 
					"				if(list.cmd[k].outs[l] == cmd_buf.ins[j]){\r\n" + 
					"					for(m = 0; m &lt; list.dep[k].depth; ++m){\r\n" + 
					"						list.dep[i].indexes[  list.dep[i].depth++  ] = list.dep[k].indexes[m];\r\n" + 
					"					}\r\n" + 
					"				}\r\n" + 
					"			}\r\n" + 
					"		}\r\n" + 
					"	}\r\n" + 
					"	i_cmd = i;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void free_cmd_list(){\r\n" + 
					"	int i;int index;\r\n" + 
					"	for(i = 0; i &lt; list.dep[i_cmd].depth; ++i){\r\n" + 
					"		index = list.dep[i_cmd].indexes[i];\r\n" + 
					"		list.cmd[index] = empty_cmd;\r\n" + 
					"	}\r\n" + 
					"	list.dep[i_cmd].depth = 0;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"bool is_flush_cmd(command cmd){\r\n" + 
					"	int i;\r\n" + 
					"	for(i = 0; i &lt; cmd_buf.nb_outs; ++i){\r\n" + 
					"		if(!is_external(cmd_buf.outs[i].loc))\r\n" + 
					"			return false;\r\n" + 
					"	}\r\n" + 
					"	return true;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void prog_pipeline(){\r\n" + 
					"	int i; int j;int index;\r\n" + 
					"	nb_ins = 0; nb_outs = 0;\r\n" + 
					"	i_ins = 0; i_outs = 0;\r\n" +
					"	for(i = 0; i&lt;list.dep[i_cmd].depth; ++i){\r\n" + 
					"		index = list.dep[i_cmd].indexes[i];\r\n" + 
					"\r\n" + 
					"		for(j = 0; j&lt;list.cmd[index].nb_ins; ++j)\r\n" + 
					"			if(is_external(list.cmd[index].ins[j].loc)) { ins[nb_ins++] = list.cmd[index].ins[j]; }\r\n" + 
					"\r\n" + 
					"		for(j = 0; j&lt;list.cmd[index].nb_outs; ++j)\r\n" + 
					"			if(is_external(list.cmd[index].outs[j].loc)) { outs[nb_outs++] = list.cmd[index].outs[j]; }\r\n" + 
					"	}\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"bool hasCompleted(){\r\n" + 
					"	int i;\r\n" + 
					"	for(i = 0; i &lt; nb_ins; ++i)\r\n" + 
					"		if(fetch[i] &lt; ins[i].size) return false;\r\n" + 
					"	for(i = 0; i &lt; nb_outs; ++i)\r\n" + 
					"		if(store[i] &lt; outs[i].size) return false;\r\n" + 
					"	return true;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void checkIns(){\r\n" + 
					"	int i;\r\n" + 
					"	for(i = 0; i &lt; nb_ins; ++i)\r\n" + 
					"		if(fetch[i] >= ins[i].size){\r\n" + 
					"			ins[i] = ins[nb_ins-1];\r\n" + 
					"			fetch[i] = fetch[nb_ins-1];\r\n" + 
					"			fetch[--nb_ins] = 0;\r\n" + 
					"		}\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void checkOuts(){\r\n" + 
					"	int i;	for(i = 0; i &lt; nb_outs; ++i)\r\n" + 
					"		if(store[i] &gt;= outs[i].size){\r\n" + 
					"			outs[i] = outs[nb_outs-1];\r\n" + 
					"			store[i] = store[nb_outs-1];\r\n" + 
					"			store[--nb_outs] = 0;\r\n" + 
					"		}\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"void setOutputs(){\r\n" + 
					"	int i;\r\n" + 
					"	for(i = 0; i &lt; cmd_buf.nb_outs; ++i)\r\n" + 
					"		cmd_buf.outs[i].size = cmd_buf.ins[0].size;\r\n" + 
					"}\r\n";
			
			t.decl+="\n\nint time_cmd(command cmd){";
			for(Processor p : c.getProcessors())
				t.decl+="\n\tif(cmd.fct == "+p.id+"_ID) return "+p.bpc+";";
			t.decl+="\n\treturn 0;\n}";
			
			t.decl+="\nint time_cmds(){\r\n" + 
					"	int res = time_cmd(list.cmd[list.dep[i_cmd].indexes[0]]); \r\n" + 
					"	int i;int index;\r\n" + 
					"	for(i = 1; i &lt; list.dep[i_cmd].depth; ++i){\r\n" + 
					"		index = list.dep[i_cmd].indexes[i];\r\n" + 
					"		if(time_cmd(list.cmd[index]) &lt; res)\r\n" + 
					"			res = time_cmd(list.cmd[index]);\r\n" + 
					"	}\r\n" + 
					"	return (burst/res)*(main_freq/freq);\r\n" + 
					"}";

			boolean configure = c.isConfigurable();
			if(configure || c.isOptional) {
				t.addLoc("proj", 125, 0).setUrgent().setLabeled(112, -40);
				t.init = "proj";
				Transition tmp = t.addTrans("proj", "idle").addNail(125, 250);
				if(configure)
					tmp.setAssign("config()", 160, 256);
				if(c.isOptional) {
					t.addTrans("prog", "idle").setGuard("f."+c.id, 125, 250).addNail(0, 500).addNail(250, 500);
					tmp.setGuard("!f."+c.id, 125, 375);
				}
			} else t.init = "idle";
			
			t.addLoc("idle", 250, 250).setLabeled(264, 216);
			t.addLoc("hcmd1", 250, 0).setCommitted();
			t.addLoc("hcmd2", 375, 0).setCommitted();
			t.addLoc("hcmd3", 625, 125).setCommitted();
			t.addLoc("hfetch", 875, 125).setCommitted();
			t.addLoc("fetching", 1000, 0).setLabeled(976, -40);
			t.addLoc("sfetch", 750, 0).setCommitted();
			t.addLoc("busy", 1000, 250).setInvariant("clk &lt;= time_cmds()", 1024, 240);
			t.addLoc("hstore", 875, 375).setCommitted();
			t.addLoc("storing", 750, 500).setLabeled(728, 520);
			t.addLoc("sstore", 1000, 500).setCommitted();
			
			t.addTrans("idle", "hcmd1").setSync("send["+c.id+"_ID]?", 176, 112);
			t.addTrans("hcmd1", "hcmd2").setAssign("setOutputs()", 272, -24);

			t.addTrans("hcmd2", "hcmd3").addNail(375, 125).setAssign("cmd_list_push()", 456, 128);
			t.addTrans("hcmd3", "idle").addNail(625, 250).setGuard("!is_flush_cmd(cmd_buf)", 376, 224).setSync("recv["+c.id+"_ID]!", 376, 256);
			t.addTrans("hcmd3", "hfetch").setGuard("is_flush_cmd(cmd_buf)", 672, 104).setAssign("prog_pipeline()", 696, 128);
			t.addTrans("hfetch", "fetching").setGuard("i_ins &lt; nb_ins", 952, 56);
			t.addTrans("fetching", "sfetch").setSync("transfer[ins[i_ins].loc]!", 808, -24);
			t.addTrans("sfetch", "hfetch").setAssign("fetch[i_ins++]+=\nburst", 696, 48);
			t.addTrans("hfetch", "busy").addNail(1000, 125).setGuard("i_ins == nb_ins", 1008, 120).setAssign("checkIns(),\ni_ins=0,\nclk=0", 1008, 136);
			t.addTrans("busy", "hstore").addNail(1000, 375).setGuard("clk == time_cmds()", 1008, 320);
			t.addTrans("hstore", "hfetch").setGuard("i_outs == nb_outs &amp;&amp;\n !hasCompleted()", 736, 208).setAssign("checkOuts(),\ni_outs=0", 760, 240);
			t.addTrans("hstore" , "storing").setGuard("i_outs &lt; nb_outs", 712, 416);
			t.addTrans("storing", "sstore").setSync("transfer[outs[i_outs].loc]!", 800, 472);
			t.addTrans("sstore", "hstore").setAssign("store[i_outs++]+=\nburst", 952, 416);
			t.addTrans("hstore", "idle").addNail(250, 375).setGuard("hasCompleted()", 536, 352).setSync("recv["+c.id+"_ID]!", 512, 376).setAssign("free_cmd_list()", 520, 392);
			
			templates.add(t);
			queries+=""+c.id.toLowerCase()+".idle && ";
			instances+="\n"+c.id.toLowerCase()+" = "+c.id.toUpperCase()+"();";
			systemCores+=c.id.toLowerCase()+",";
		}
		systemCores = systemCores.substring(0, systemCores.length()-1);
	}
	
	public void _defineNodes() {
		
		Collection<Path> paths = ds.app.getPaths();
		for(Path p : paths) {
			global+="\n\nurgent chan "+p.id+"_c;\ndata "+p.id+"_d;";
		}
		
		for(Node n : ds.app.getSortedApplication()) {
			if(n.type == Type.TASK)
				_defineTask((Task)n);
			else if(n.type == Type.DATA_SOURCE)
				_defineInputData((DataSource)n);
			
			systemNodes = n.id.toLowerCase()+ " &lt; " + systemNodes;
		}
		systemNodes = systemNodes.substring(0, systemNodes.length()-4);
	}
	
	private void _defineInputData(DataSource n) {
		Template t = new Template(n.id.toUpperCase());
		for(Argument a : n.outputs.values()) {
			t.decl+="\n\nbool "+a.id+"_b = false;";
		}
		
		t.decl+="\nint config_cost = 0;";
		
		boolean configure = ds.map.dms.get(n).choices.size()>1 || n.isVariable();
		if(configure) {
			t.decl+="\n\nvoid configure(){";
			t.decl+="\n\tif(false);";
			for(DataSourceMapping.Choice c : ds.map.dms.get(n).choices.values()) {
				t.decl+="\n\telse if(f."+c.id+"_id){";
				for(Argument a : n.outputs.values())
					t.decl+="\n\t\t"+a.path.id+"_d.loc = "+c.storage.id+"_ID;";
				t.decl+="\n\t}";
			}
		
			if(n.sizes.size()>1) {
				t.decl+="\n\n\tif(false);";
				for(Size s : n.sizes) {
					t.decl+="\n\telse if(f."+n.id+"_size_"+s.value+"_id){";
					for(Argument a : n.outputs.values())
						t.decl+="\n\t\t"+a.path.id+"_d.size = "+s.value+";"+(s.qualityLoss!=0? " config_cost += "+s.qualityLoss+"; d.cost += "+s.qualityLoss+";" : "");
					t.decl+="\n\t}";
				}
			}
			t.decl+="\n}";
		}
		
		
		t.addLoc("end", 75, 75).setLabeled(64, 88);
		
		t.addLoc("outs", 0, 0).setCommitted();
		t.addLoc("sending", 75, -75).setLabeled(96, -80);
		
		if(configure || n.isOptional) {
			t.addLoc("proj", -100, -52).setUrgent();
			t.init = "proj";
			Transition tmp = t.addTrans("proj", "outs").addNail(-100, 0);
			if(configure)
				tmp.setAssign("configure()", -88, -24);
			if(n.isOptional) {
				tmp.setGuard("f."+n.id+"_id", -80, -40);
				t.addTrans("proj", "end").setGuard("!f."+n.id+"_id", -88, 16).addNail(-100, 88);
			}
		} else {
			t.init = "outs";
		}
		
		String guard = "";
		String assign = "";
		for(Argument a : n.outputs.values()) {
			t.addTrans("sending", "outs").setGuard("!"+a.id+"_b", 64, -56).setSync(a.path+"_c!", 48, -40).setAssign(a.id+"_b = true", 32, -24);
			guard += a.id+"_b &amp;&amp; ";
			assign += a.path+"_d.id = "+(gen_id++)+",\n"
			+(ds.map.dms.get(n).choices.size()==1? a.path.id+"_d.loc = "+ds.map.dms.get(n).choices.keySet().iterator().next()+"_ID,\n": "")
			+(n.sizes.size()==1? a.path.id+"_d.size = "+n.sizes.iterator().next().value+",\n": "");
		}
		assign = assign.substring(0, assign.length() - 2);
		guard = guard.substring(0, guard.length() - 12);
		t.addTrans("outs", "sending").setGuard("!("+guard+")", 8, -128).setAssign(assign, 125, -125).addNail(0, -75);
		t.addTrans("outs", "end").setGuard(guard, 16, 32).setAssign(n.qualityLoss!=0? "cost += "+n.qualityLoss+" + config_cost,\n d.qualityLoss += "+n.qualityLoss+" + config_cost" : "", 24, 32).addNail(0, 75);
	
		templates.add(t);
		queries+=""+n.id.toLowerCase()+".end && ";
		instances+="\n"+n.id.toLowerCase()+" = "+n.id.toUpperCase()+"();";
		//systemNodes+=n.id.toLowerCase()+",";
	}

	private void _defineTask(Task n) {	
		Template t = new Template(n.id.toUpperCase());
		
		for(Argument a : n.inputs.values()) {
			t.decl+="\nbool "+a.id+"_b = false;";
		}
		/*for(Argument a : n.inputs.values())
			t.decl+="\ndata "+a.id+";";
		*/
		for(Argument a : n.outputs.values()) {
			t.decl+="\nbool "+a.id+"_b = false;";
		}
		/*for(Argument a : n.outputs.values())
			t.decl+="\ndata "+a.id+" = { "+(gen_id++)+", "+(ds.map.pms.get(a.path).choices.size()==1? ds.map.pms.get(a.path).choices.keySet().iterator()+"_ID" : "0" )+", 0};";
		*/
		t.decl+="\n\nprocessor_id proc = "+(ds.map.tms.get(n).choices.size()==1? ds.map.tms.get(n).choices.keySet().iterator().next()+"_ID": "0")+";";
		t.decl+="\npu_id pu = "+(ds.map.tms.get(n).choices.size()==1? ds.map.tms.get(n).choices.keySet().iterator().next().parent.id+"_ID": "0")+";";
		
		boolean configure = false;
		for(Argument a : n.outputs.values())
			if(ds.map.pms.get(a.path).choices.size()>1) {
				configure = true;
				break;
			}
		configure = configure || ds.map.tms.get(n).choices.size()>1;
		
		if(configure) {
			t.decl+="\n\nvoid configure(){\n";
			if(ds.map.tms.get(n).choices.size()>1) {
				t.decl+="\n\n\tif(false);";
				for(TaskMapping.Choice c : ds.map.tms.get(n).choices.values())
					t.decl+="\n\telse if(f."+n.id+"_On_"+c.processor.id+"_id) { proc = "+c.processor.id+"_ID; pu = "+c.processor.parent.id+"_ID; }";
			}
			for(Argument a : n.outputs.values()) {
				if(ds.map.pms.get(a.path).choices.size()>1) {
					t.decl+="\n\n\tif(false);";
					for(PathMapping.Choice c : ds.map.pms.get(a.path).choices.values())
						t.decl+="\n\telse if(f."+a.path.id+"_On_"+c.memory.id+"_id) "+a.path+"_d.loc = "+c.memory.id+"_ID;";
				}
			}
			
			t.decl+="\n}";
		}
		
		t.decl+="\n\nvoid gen_cmd(){\n\tcmd_buf.fct = proc;";
		int i;
		i=0;
		for(Argument a : n.inputs.values())
			t.decl+="\n\tcmd_buf.ins["+(i++)+"] = "+a.path.id+"_d;";
		t.decl+="\n\tcmd_buf.nb_ins = "+i+";";
		i=0;
		
		for(Argument a : n.outputs.values()) {
			t.decl+="\n\t"+a.path.id+"_d.id = "+(gen_id++)
					+";\n\t"+(ds.map.pms.get(a.path).choices.size()==1? a.path.id+"_d.loc = "+ds.map.pms.get(a.path).choices.keySet().iterator().next().id+"_ID;":"");
			t.decl+="\n\tcmd_buf.outs["+(i++)+"] = "+a.path.id+"_d;";
		}
		t.decl+="\n\tcmd_buf.nb_outs = "+i+";";
		t.decl+="\n}";
		
		if(!n.outputs.isEmpty()) {
			t.decl+="\n\nvoid set_result(){";
			i = 0;
			for(Argument a : n.outputs.values())
				t.decl+="\n\t"+a.path+"_d = cmd_buf.outs["+(i++)+"];"; 
			t.decl+="\n}";
		}
		t.addLoc("hins", 0, 0).setCommitted();
		
		if(configure || n.isOptional) {
			t.addLoc("proj", -100, -125).setUrgent();
			t.init = "proj";
		} else {
			t.init = "hins";
		}
		
		t.addLoc("starting", 125, 0).setLabeled(104, 24);
		t.addLoc("processing", 250, 0).setLabeled(216, 24);
		t.addLoc("houts", 375, 0).setCommitted();
		
		if(!n.inputs.isEmpty())
			t.addLoc("ins", 125, -125).setLabeled(144, -136);
		if(!n.outputs.isEmpty())
			t.addLoc("outs", 500, -125).setLabeled(520, -136);
			
		t.addLoc("end", 375, 125).setLabeled(392, 120);
		
		
		if(configure || n.isOptional) {
			Transition tmp = t.addTrans("proj", "hins").addNail(-100, 0);
			if(configure) {
				tmp.setAssign("configure()", -88, -24);
			}
			if(n.isOptional) {
				t.addTrans("proj", "end").setGuard("!f."+n.id+"_id", 112, 104).addNail(-100, 125);
				tmp.setGuard("f."+n.id+"_id", -80, -40);
			}
		}
			
		String guard = "";
		int ix = 0; final int dx = 64;
		for(Argument a : n.inputs.values()) {
			t.addTrans("ins", "hins").setGuard("!"+a.id+"_b", 104, -104).setSync(a.path+"_c?", 88, -88).setAssign(a.id+"_b = true", 72, -72);
			guard += a.id+"_b &amp;&amp; ";
			ix+=dx;
		}
		guard = guard.substring(0, guard.length() - 12);
		t.addTrans("hins", "ins").setGuard("!("+guard+")", 0, -160).addNail(0, -125);
		t.addTrans("hins", "starting").setGuard(guard, 48, -24).setAssign(n.qualityLoss!=0? "cost += "+n.qualityLoss+",\n d.qualityLoss += "+n.qualityLoss : "", 56, -24);
		t.addTrans("starting", "processing").setSync("send[pu]!", 152, -24).setAssign("gen_cmd()", 152, -36);
		
			
		guard = "";
		for(Argument a : n.outputs.values()) {
			t.addTrans("outs", "houts").setGuard("!"+a.id+"_b", 480, -104).setSync(a.path+"_c!", 464, -88).setAssign(a.id+"_b = true", 448, -72);
			guard += a.id+"_b &amp;&amp; ";
		}
		
		Transition e = t.addTrans("processing", "houts").setSync("recv[pu]?", 280, -24);
		if(!n.outputs.isEmpty())
			e.setAssign("set_result()", 272, 0);
		
		e = t.addTrans("houts", "end");
		if(!n.outputs.isEmpty()) {
			guard = guard.substring(0, guard.length() - 12);
			t.addTrans("houts", "outs").setGuard("!("+guard+")", 376, -160).addNail(375, -125);
			e.setGuard(guard, 384, 48);
		}
		
		
		
		templates.add(t);
		queries+=""+n.id.toLowerCase()+".end && ";
		instances+="\n"+n.id.toLowerCase()+" = "+n.id.toUpperCase()+"();";
		//system+=n.id.toLowerCase()+",";
	}

	private String systemMemories;
	private String systemCores;
	private String systemNodes;
	
	public String queries;
	public String uppaal;
	private String global;
	private Collection<Template> templates;
	private String instances;
	private String system;

	private DesignSpace ds;
	private NonFunctionalCst cst;
	private FunctionCost fct;
	private int gen_id;
}
