package reasoning;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import designspace.DesignSpace;
import designspace.VariabilityTree;
import designspace.VariabilityTree.ConstraintNode;
import designspace.VariabilityTree.VariabilityNode;
import designspace.VariabilityTree.VariabilityNode.Type;
import designspace.VariabilityTree.VariabilityNode.Variability;
import requirements.NonFunctionalCst;

public class ToCfrMOO {

	/*
	 * 
abstract Feature
cost -> integer
performance -> integer
abstract Computer
CPU : Feature
[ cost = 2 ]
[ performance = 0 ]
IntegratedGPU : Feature ?
[ cost = 1 ]
[ performance = 1 ]
SharedMemory : Feature ?
[ cost = 0 ]
[ performance = 0 ]
DiscreteGPU : Feature ?
[ cost = 2 ]
[ performance = 2 ]
[ IntegratedGPU || DiscreteGPU ]
total_cost -> integer = sum Feature.cost
total_performance -> integer = sum Feature.performance
OptimalComputer : Computer
<<minimize OptimalComputer.total_cost>>
<<maximize OptimalComputer.total_performance>>
*/
	
	public String DStoCfrMOO(DesignSpace ds, NonFunctionalCst cst) {
		grp="";
		
		cfr = "abstract ApplicationFeature" +
				_tab(1)+"qualityLoss->integer" +
				_tab(2)+"[this>=0]" +
				"\nabstract PlatformFeature" +
				_tab(1)+"cost->integer" +
				_tab(2)+"[this>=0]" +
				"\nabstract DesignSpaceVariability";
		for(VariabilityNode node : ds.tree.root.children)
			_toTVL(node, 1);
		_cstTree(ds.tree);
		String nfr = "["+_genQueries(cst)+"]\n";
		cfr +=  _tab(1) + 
				"total_qualityLoss -> integer = sum ApplicationFeature.qualityLoss"+_tab(1) + 
				"total_cost -> integer = sum PlatformFeature.cost"+_tab(1)+
				nfr + "\n"+
				"OptimalDesign : DesignSpaceVariability\n" +
				"<< min OptimalDesign.total_qualityLoss>>\n" + 
				"<< min OptimalDesign.total_cost>>";
		return cfr;
	}

	private String _genQueries(NonFunctionalCst cst) {
		if(cst == null)
			return "";
		return _genQuery(cst);
	}
	
	private String _genQuery(NonFunctionalCst n) {
		String res = "";
		switch(n.type) {
		case AND:
			assert(n.nodes.size() >= 1);
			res += "";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _genQuery(n.nodes.get(i))!=null? _genQuery(n.nodes.get(i))+" && ":"";
			res += _genQuery(n.nodes.get(i))+"";
			res = res.substring(0, res.length()-8);
			break;
		
		case IFF:
			assert(false);
			break;
		case LEAF:
			assert(n.nodes == null);
			res += _toString(n.nfp)!=null?_toString(n.nfp)+n.op+n.value:null;
			break;
			
		case NOT:
			assert(n.nodes.size() == 1);
			res += "!"+_genQuery(n.nodes.get(0));
			break;
			
		case OR:
			assert(n.nodes.size() >= 1);
			res += "";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _genQuery(n.nodes.get(i))+" || ";
			res += _genQuery(n.nodes.get(i))+"";
			break;
			
		case REQ:
			assert(false);
			break;
		default:
			assert(false);
			break;
		
		}
		return res;
	}
	
	private String _toString(int nfp) {
		switch(nfp) {
		case NonFunctionalCst._NONE:
			assert(false);
		case NonFunctionalCst._QUALITY_LOSS:
			return "total_qualityLoss";
		case NonFunctionalCst._COST:
			return "total_cost";
		case NonFunctionalCst._RUN_TIME:
			return null;
		}
		return null;
	}

	private String _tab(int level) {
		String res = "\n";
		for(int i = 0; i < level; ++i)
			res+="    ";
			//res+="\t";
		return res;
	}
	
	private void _toTVL(VariabilityNode node, int level) {
		cfr+=_tab(level);
		switch(node.type) {
		case XOR:
			cfr+="xor "+node.id;
			break;
		case GROUP:
			grp = _setType(node.id);
			cfr+=node.id;
			break;
		case LEAF:
			cfr+=node.id;
			break;
		}
		
		if(!node.prices.isEmpty()) {
			cfr+=_type(grp)+(node.variability == Variability.OPT? "?":"");
			for(Entry<String, Integer> e : node.prices.entrySet())
				cfr+=_tab(level+1)+"["+e.getKey()+" = "+e.getValue()+"]";
		}
		else
			cfr+=(node.variability == Variability.OPT? "?":"");
		
		for(VariabilityNode n : node.children)
			_toTVL(n, level+1);
		
		if(node.type == Type.XOR || node.type == Type.GROUP)
			cfr+="";
	}
	
	private String _defaultPrice(String grp2, int level) {
		switch(grp2) {
		case "ApplicationVariability":
			return _tab(level)+"[qualityLoss = 0]";
		case "PlatformVariability":
			return _tab(level)+"[cost = 0]";
		/*case "DeploymentVariability":
			return "";*/
		}
		return "";
	}

	private String _type(String grp2) {
		switch(grp2) {
		case "ApplicationVariability":
			return " : ApplicationFeature";
		case "PlatformVariability":
			return " : PlatformFeature";
		/*case "DeploymentVariability":
			return "";*/
		}
		return "";
	}
	
	private String _setType(String grp2) {
		switch(grp2) {
		case "ApplicationVariability":
			return "ApplicationVariability";
		case "PlatformVariability":
			return "PlatformVariability";
		case "DeploymentVariability":
			return "";
		}
		return grp;
	}

	private void _cstTree(VariabilityTree tree) {
		for(Map.Entry<String, List<ConstraintNode>> e : tree.constraints.entrySet()) {
			//cfr+="\n\t/*"+e.getKey()+" constraints*/";
			for(ConstraintNode n : e.getValue())
				cfr+=_tab(1)+"["+_cstNode(n)+"]";
			cfr+="\n";
		}
		//cfr+="\n\t/*invalid products*/";
		for(ConstraintNode n : tree.prunings)
			cfr+=_tab(1)+"["+_cstNode(n)+"]";
	}
	
	private String _cstNode(ConstraintNode n) {
		String res = "";
		switch(n.type) {
		case REQ:
			assert(n.nodes.size() == 2);
			res += _cstNode(n.nodes.get(0))+" => "+_cstNode(n.nodes.get(1));
			break;
		case IFF:
			assert(n.nodes.size() == 2);
			res += _cstNode(n.nodes.get(0))+" <=> "+_cstNode(n.nodes.get(1));
			break;
		case NOT:
			assert(n.nodes.size() == 1);
			res += "!"+_cstNode(n.nodes.get(0));
			break;
		case LEAF:
			assert(n.nodes == null);
			res += n.id;
			break;
		case AND:
			assert(n.nodes.size() >= 1);
			res += "(";
			int i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+" && ";
			res += _cstNode(n.nodes.get(i))+")";
			break;
		case OR:
			assert(n.nodes.size() >= 1);
			res += "(";
			i = 0;
			for(i = 0; i < n.nodes.size() - 1; ++i)
				res += _cstNode(n.nodes.get(i))+" || ";
			res += _cstNode(n.nodes.get(i))+")";
			break;
		}
		return res;
	}
	
	private String cfr;
	private String grp;
	private DesignSpace ds;
}
