package xpgenerator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import application.Application;
import application.DataSource;
import application.Element;
import application.Node;
import application.Node.Argument;
import application.Path;
import application.Task;
import xpgenerator.FunctionalDomain.Function;
import xpgenerator.GeneratedApplication.ApplicationParameters.DataSizeParameter;
import xpgenerator.GeneratedPlatform.PlatformParameters.CapacityParameter;
import xpgenerator.Graph.Statistic;

public class GeneratedApplication {
	
	public static abstract class NodeVertex{
		
		public NodeVertex(Vertex v) {
			this.v = v;
		}
		
		public abstract int getNbInputs();
		
		public abstract int getNbOutputs();
		
		public abstract Node getNode();
		
		public Vertex v;
	}
	
	public static class DataVertex extends NodeVertex{
		
		public DataVertex(Vertex v, DataSource d) {
			super(v);
			this.d = d;
		}
		
		public int getNbInputs() {
			assert(v.parents.size() == 0);
			assert(v.join == false);
			return 0;
		}
		
		public int getNbOutputs() {
			if(v.split)
				assert(v.children.size()>1);
			return v.split? 1: v.children.size();
		}
		
		public Node getNode() {
			return d;
		}
		
		public DataSource d;
	}
	
	public static class TaskVertex extends NodeVertex{
		
		public TaskVertex(Vertex v, Task t) {
			super(v);
			this.t = t;
		}
		
		public Task t;
		
		public int getNbInputs() {
			assert(v.parents.size() > 0);
			if(v.join)
				assert(v.parents.size()>1);
			return v.join? 1: v.parents.size();
		}
		
		public int getNbOutputs() {
			if(v.split)
				assert(v.children.size()>1);
			return v.split? 1: v.children.size();
		}
		
		public void setFunction(int fct) {
			assert(v.value == 0);
			v.value = fct;
			t.function = "F"+fct;
		}
		
		public Node getNode() {
			return t;
		}

		public void setInputPorts(Collection<String> ins) {
			v.setInputs(ins);
		}

		public void setOutputPorts(Collection<String> outs) {
			v.setOutputs(outs);
		}
	}
	
	public static class ApplicationParameters{
		
		public static class DataSizeParameter{
			
			public DataSizeParameter(int size, float p, int qualityLoss) {
				this.size = size;
				this.p = p;
				this.qualityLoss = qualityLoss;
			}
			
			public int size;
			public float p;
			public int qualityLoss;
		}
		
		public ApplicationParameters() {
			sSizes = new ArrayList<>();
		}
		
		public Collection<Map.Entry<Integer, DataSizeParameter>> sSizes;
		public float sNbInputs;
		public float sNbOutputs;
		public float pAlt;
		public float sNbNodes;
		public float pVariableFlow;
		public float sNodeQuality;
		public float sNbSize;
		public float pMappingRate;
		public float pAltSize;
	}
	
	public int getNbNodes() {
		assert(app.getNodes().size() == g.nodes.size());
		return g.nodes.size();
	}
	
	private Graph deepCopy(Graph g2) {
		try {
		     ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		     ObjectOutputStream outputStrm = new ObjectOutputStream(outputStream);
		     outputStrm.writeObject(g2);
		     ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
		     ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
		     return (Graph) objInputStream.readObject();
		   }
		   catch (Exception e) {
		     e.printStackTrace();
		     return null;
		   }
	}

	public GeneratedApplication(FunctionalDomain domain, ApplicationParameters param, Statistic stats) {
		this(domain, param, stats, null);
	}
		

	public GeneratedApplication(FunctionalDomain domain, ApplicationParameters param, Statistic stats, Graph ref) {
		this.app = new Application("");
		this.domain = domain;
		this.param = param;
		this.g = null;
		this.stats = stats;
		
		datas = new ArrayList<>();
		tasks = new ArrayList<>();
		
		do {
			
			System.out.println("***** 1 GRAPHE CREATION *****");
			
			if(ref == null)
				g = new Graph(param);
			else
				g = deepCopy(ref);
			
			System.out.println(Graph.printAdjMatrix(g.toAdjMatrix()));
			System.out.println(Graph.printAdjMatrix(g.toReverseAdjMatrix()));
			System.out.println(g.printStatistics());
			
			System.out.println("******* 2 ADD FLOW VARIABILITY *****");
			
			g.addFlowVariability(param);
			
			System.out.println(Graph.printAdjMatrix(g.toAdjMatrix()));
			System.out.println(Graph.printAdjMatrix(g.toReverseAdjMatrix()));
			System.out.println(g.printStatistics());
			
			g.clearVoidVertex();
			
			System.out.println("***************************");
			
		} while (!g.satisfy(stats, 0.0f));
		
		this.stats = g.getStats();
		
		System.out.println("***** Satisfy statistics constraints *****");
		System.out.println(g.printStatistics());
		
		
		try {
			Thread.sleep(3);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.save = deepCopy(g);
		
		System.out.println("***** 3 GRAPHE APPLICATION NODES *****");
		
		Map<Vertex, NodeVertex> res = _createApplicationNodes();
		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("***** 4 ASSIGN DOMAIN TASK FUNCTIONS *****");
		
		
		_assignTaskFunctions();
		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("***** 5 GRAPHE APPLICATION PATHS *****");
		
		_createDataPaths(res);
		app.print();
		try {
			Thread.sleep(3);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assert(app.getPaths().size() == g.getNbPaths());
		
		System.out.println("***** 6 ASSIGN DATAS SIZES *****");
		
		_assignDataSizes();
		
	
		
		System.out.println("****************************************************\n" +
							"****************** APPLICATION ********************\n" +
							"***************************************************\n" +
							"******************* GENERATED *********************\n" +
							"***************************************************");
		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	private void _assignDataSizes() {
		
		Random r = new Random();
		
		float avgDataNbSize;
		float avgDataSize;
		
		do {
			
			avgDataNbSize = 0.f;
			avgDataSize = 0.f;
			
			for(DataVertex dv : datas) {
				DataSource d = dv.d;
				
				d.sizes.clear();
				
				double nbAltSizes = (double)(r.nextGaussian() + param.sNbSize);
				int k = ((int) nbAltSizes <= 0)? 1 :  ((int) nbAltSizes > param.sSizes.size())? param.sSizes.size(): (int) nbAltSizes ;
				
				//System.out.println(" data sizes = "+k);
				
				Collection<Integer> sizes = new ArrayList<>();
				
				do {
					for(Entry<Integer, DataSizeParameter> e : param.sSizes)
						if(r.nextFloat() <= param.pAltSize || d.sizes.isEmpty()) {
							
							if(!d.sizes.isEmpty()) {
								d.addSize("", e.getKey().intValue(), e.getValue().qualityLoss);
								quality += e.getValue().qualityLoss;
							}
							else
								d.addSize("", e.getKey().intValue(), 0);
							
							sizes.add(e.getKey());
							avgDataSize += e.getKey();
						}
					
				} while (d.sizes.isEmpty());
				
				avgDataNbSize += sizes.size();
				//System.out.println("nb size "+sizes+" avg "+avgDataNbSize);
			}	
			avgDataSize /= avgDataNbSize;
			avgDataNbSize /= datas.size();
			
			System.out.println("DataNbSize/DataSize \t\t"+avgDataNbSize+"/"+avgDataSize);
			System.out.println("*******************************************************");
			
			
		} while(false/*!(stats.avgDataNbSize < avgDataNbSize && stats.avgDataSize < avgDataSize)*/);
		
		Statistic genAppStat = g.getStats();
		genAppStat.avgDataNbSize = avgDataNbSize;
		genAppStat.avgDataSize = avgDataSize;
		
		/*try {
			Thread.sleep(400000000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
	}
	
	private void _createDataPaths(Map<Vertex, NodeVertex> map) {
		Collection<Vertex> res = new HashSet<>();
		Collection<Vertex> flags = new ArrayList<>();
		
		assert(map.size() == g.nodes.size());
		
		for(Vertex v : g.nodes) {
			if(!flags.contains(v)) {
				res.add(v);
				_addaPathsdfs(v, flags, map);
			}
		}
		
		List<Element> alt = new ArrayList<>();
		for(Entry<Vertex, Collection<Collection<Vertex>>> e : g.getJoins().entrySet()) {
			
			int i = 0;
			System.out.println("----> join vertex : "+map.get(e.getKey()).getNode().id);
			
			Node join = map.get(e.getKey()).getNode();
			
			Path p = null;
			assert(join.inputs.values().size() == 1);
			for(Argument a : join.inputs.values())
				p = a.path;
			assert(p != null);
			
			for(Collection<Vertex> x : e.getValue()) {
				alt.clear();
				//alt.add(join);
				
				System.out.println("--------> flow : "+(++i));
				for(Vertex v : x) {
					

					Node child = map.get(v).getNode();
					alt.add(child);
					
					System.out.println("----------------> node :"+map.get(v).getNode().id);
					
					boolean join_child = false;
					for(Vertex c : v.children)
						if(c.join) {
							join_child = true;
							break;
						}
					if(join_child)
						continue;
					
					
					
					for(Argument a : child.outputs.values()) {
						System.out.println("----------------> path :"+a.path.id);
						alt.add(a.path);
					}
				}
				app.join(p, ((List<Element>)alt).get(0), alt);	
			}
			
		}
		alt = new ArrayList<>();
		for(Entry<Vertex, Collection<Collection<Vertex>>> e : g.getSplits().entrySet()) {
			
			alt.clear();
			
			System.out.println("----> split vertex : "+map.get(e.getKey()).getNode().id);
			int i = 0;
			
			Node split = map.get(e.getKey()).getNode();
			
			Path p = null;
			assert(split.outputs.values().size() == 1);
			for(Argument a : split.outputs.values())
				p = a.path;
			assert(p != null);
			
			for(Collection<Vertex> x : e.getValue()) {
				
				alt.clear();
				//alt.add(split);
				
				System.out.println("--------> flow : "+(++i));
				for(Vertex v : x) {
					
					Node child = map.get(v).getNode();
					alt.add(child);
					
					System.out.println("----------------> node :"+map.get(v).getNode().id);
					
					boolean join_child = false;
					for(Vertex c : v.children)
						if(c.join) {
							join_child = true;
							break;
						}
					if(join_child)
						continue;
					
					
					
					for(Argument a : child.outputs.values()) {
						System.out.println("----------------> path :"+a.path.id);
						alt.add(a.path);
					}
				}
				
				app.split(p, ((List<Element>)alt).get(0), alt);
			}
		}
	}
	
	private void _addaPathsdfs(Vertex v, Collection<Vertex> flags, Map<Vertex, NodeVertex> map) {

		Node in = map.get(v).getNode();
		
		Path p = null;
		
		//assert(!(v.join && v.split));
		
		if(v.join) {
			p = app.addPath("PTo"+map.get(v).getNode().id);
			in.connect(p, v.getInput(in.inputs.size()));
			for(Vertex parent : v.parents) {
				
				assert(parent.children.size() == 1);
				assert(parent.children.contains(v));
				
				Node out = map.get(parent).getNode();
				out.connect(parent.getOutput(out.outputs.size()), p);
			}
		}
		
		if(v.split) {
			
			p = app.addPath("PFrom"+map.get(v).getNode().id);
			in.connect(v.getOutput(in.outputs.size()), p);
			for(Vertex c : v.children) {
				
				assert(c.parents.size() == 1);
				assert(c.parents.contains(v));
				
				Node out = map.get(c).getNode();
				out.connect(p, c.getInput(out.inputs.size()));
			}
			
		} else {
		
			boolean child_join = false;
			for(Vertex c : v.children)
				if(c.join) {
					
					assert(v.children.size() == 1);
					
					child_join = true;
					break;
				}
			
			if(child_join == false) {
				for(Vertex c : v.children) {
					p = app.addPath("P"+map.get(v).getNode().id+"_"+map.get(c).getNode().id);
					in.connect(v.getOutput(in.outputs.size()), p);
					Node out = map.get(c).getNode();
					out.connect(p, c.getInput(out.inputs.size()));
				}
			}
		}
		
		flags.add(v);
		
		/************************************************/
		Collection<Vertex> neigh = new HashSet<>();
		neigh.addAll(v.parents);
		neigh.addAll(v.children);
		
		for(Vertex n : neigh)
			if(!flags.contains(n))
				_addaPathsdfs(n, flags, map);
	}

	private Map<Vertex, NodeVertex> _createApplicationNodes() {
		Map<Vertex, NodeVertex> res = new HashMap<>();
		for(Vertex n : g.nodes) {
			if(n.parents.isEmpty()) {
				assert(n.parents.size() == 0);
				assert(n.children.size() > 0);
				
				DataSource data = app.addDataSource("D"+n.id);
				
				int tmp = _getQualityLoss(param);
				data.setQualityLoss(tmp);
				quality += tmp;
				
				DataVertex dv = new DataVertex(n, data);
				datas.add(dv);
				res.put(n, dv);
			}
			else {
				assert(n.parents.size() > 0);
				assert(n.children.size() >= 0);
				
				Task t = app.addTask("T"+n.id, "");
				
				int tmp = _getQualityLoss(param);
				t.setQualityLoss(tmp);
				quality += tmp;
				
				TaskVertex tv = new TaskVertex(n, t);
				tasks.add(tv);
				res.put(n, tv);
			}
		}
		return res;
	}

	private int _getQualityLoss(ApplicationParameters param) {
		Random r = new Random();
		int qualityLoss = (int) (r.nextGaussian() + param.sNodeQuality);
		qualityLoss = (qualityLoss < 0) ? 0: qualityLoss;
		//System.out.println("qualitad "+qualityLoss);
		return qualityLoss;
	}
	
	private void _assignTaskFunctions() {
		Map<Integer, Collection<TaskVertex>> map = new HashMap<>();
		
		
		for(TaskVertex tv : tasks) {
			assert(tv.v.parents.size() > 0);
			Function fct = domain.setFctInApp(tv.getNbInputs(), tv.getNbOutputs());
			assert(fct.value != 0);
			tv.setFunction(fct.value);
			assert(fct.ins != null && fct.outs != null);
			assert(fct.ins.size() > 0);
			assert(fct.ins.size() == tv.getNbInputs() && fct.outs.size() == tv.getNbOutputs());
			tv.setInputPorts(fct.ins);
			tv.setOutputPorts(fct.outs);
			if(!map.containsKey(fct.value))
				map.put(fct.value, new ArrayList<>());
			map.get(fct.value).add(tv);
			
		}
		
		for(DataVertex dv : datas) {
			assert(dv.getNbInputs() == 0);
			assert(dv.getNbOutputs() > 0);
			Collection<String> outs = new ArrayList<>();
			for(int i = 0; i < dv.getNbOutputs(); ++i)
				outs.add(dv.d.id+"_o_"+i);
			dv.v.setOutputs(outs);
		}
		
		for(Map.Entry<Integer, Collection<TaskVertex>> e : map.entrySet()) {
			//System.out.println("Task with fct "+e.getKey()+" : ");
			System.out.println(e.getKey()+" :");
			for(TaskVertex tvfct : e.getValue())
				System.out.println("-> "+tvfct.v);
			System.out.println();
			
			/*try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
		}
	}
	
	
	public Application app;
	public ApplicationParameters param;
	public FunctionalDomain domain;
	public Graph g;
	public Graph save;
	public Statistic stats;
	public int quality;
	public Collection<DataVertex> datas;
	public Collection<TaskVertex> tasks;
	

}
