package xpgenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Processor.Port;
import platform.Resource;
import platform.Storage;
import platform.Storage.Type;
import xpgenerator.GeneratedApplication.ApplicationParameters;
import xpgenerator.GeneratedPlatform.PlatformParameters.CapacityParameter;
import xpgenerator.Graph.Statistic;

public class GeneratedPlatform {
	
	public static class ProcessingVertex {
		
		public ProcessingVertex(Vertex v, Processor p) {
			this.p = p;
			this.v = v;
		}
		
		public void addStorage(Storage s) {
			assert(v.parents.size() > 0);
			
			if(s.type == Type.READ_ONLY)
				p.connectToInputPorts(s);
			else if(s.type == Type.READ_AND_WRITE) {
				p.connectToInputPorts(s);
				if(v.children.size() > 0) {
					p.connectToOutputPorts(s);
				}
			}
		}
		
		public void addStorages(Collection<Storage> storages) {
			for(Storage s : storages)
				addStorage(s);
			
			for(Memory so : p.getOutputStorages()) {
				assert(((Storage)so).type == Type.READ_AND_WRITE);
			}
		}
		
		
		public Processor p;
		public Vertex v;
		
	}
	
	public static class CoreVertex {
		public Collection<ProcessingVertex> pipe;

		public CoreVertex(Graph g, Component core) {
			this.core = core;
			this.g = g;
			processors = new HashMap<>();
			
			int i = 0;
			for(Vertex v : g.nodes) {
				Processor p = core.addProcessor("P"+(i++), "");
				p.setBytesPerCycle(4);
				processors.put(v, new ProcessingVertex(v, p));
			}
		}

		public void createPipeline() {
			for(ProcessingVertex pv : processors.values()) {
				
				for(String in : pv.v.getInputs()) {
					pv.p.inputs.put(in, new Port(pv.p, in));
				}
				for(String out : pv.v.getOutputs()) {
					pv.p.outputs.put(out, new Port(pv.p, out));
				}
			}
			
			for(ProcessingVertex pv : processors.values()) {	
				int i = 0;
				for(Vertex c : pv.v.children) {
					Memory r = core.addFIFOBuffer("R"+pv.v.id+"_"+(i++));
					pv.p.connectToOutputPorts(r);
					processors.get(c).p.connectToInputPorts(r);
				}
			}
			
			System.out.println("*********************** CREATE PIPELINES STEP 1 *************************");
			print();
			
			Map<ProcessingVertex, Collection<Memory>> ins =  new HashMap<>();
			//Map<ProcessingVertex, Collection<Memory>> outs = new HashMap<>();
			
			for(ProcessingVertex pv : processors.values()) {
				for(Vertex p : pv.v.parents) {
					if(p!=null) {
						ins.put(pv, processors.get(p).p.getInputFIFOBuffersPath());
					}
				}
				/*for(Vertex c : pv.v.children) {
					outs.put(pv, processors.get(c).p.getOutputFIFOBuffersPath());
				}*/
			}
			
			for(ProcessingVertex pv : processors.values()) {
				if(ins.get(pv)!=null) pv.p.connectToInputPorts(ins.get(pv));
				//if(outs.get(pv) != null) pv.p.connectToOutputPorts(outs.get(pv));
			}
			
			for(ProcessingVertex pv : processors.values()) {
				assert(pv.p.inputs.keySet().size() > 0);
			}
			
			System.out.println("*********************** CREATE PIPELINES STEP 2 *************************");
			print();
		}
		
		public void print() {
			System.out.println("====================================");
			for(ProcessingVertex pv : processors.values()) {
				String ins = "";
				//if(pv.p.inputs.size()>0)
					//ins+=" {";
				//else
					//ins+="xVOIDx";
				for(String port : pv.p.inputs.keySet()) {
					ins+="\n"+port+" : ";
					for(Memory m : pv.p.getInputFIFOBuffers()) {
						ins+=m.id;
						/*for(Processor p : m.producers)
							ins+=p.id+" and ";*/
						ins+="|";
					}
					if(pv.p.getInputFIFOBuffers().size() == 0)
						ins += " EXT ";
				}
				if(ins.equals(""))
					ins="\n VOID";
				//if(pv.p.inputs.size()>0)
					//ins+="}";
				
				String outs = "";
				//if(pv.p.outputs.size()>0)
					//outs+="{";
				//else
					//outs+="X";
				for(String port : pv.p.outputs.keySet()) {
					outs+="\n"+port+" : ";
					for(Memory m : pv.p.getOutputFIFOBuffers()) {
						outs+=m.id;
						/*for(Processor c : m.consumers)
							outs+=c.id+" and ";*/
						outs+="|";
					}
					if(pv.p.getOutputFIFOBuffers().size() == 0)
						outs += " EXT ";
				}
				if(outs.equals(""))
					outs="\n VOID";
				//if(pv.p.outputs.size()>0)
					//outs+="} ";
				
				ins = ins.toLowerCase();
				outs = outs.toLowerCase();
				System.out.println("("+pv.p.id+"),[["+pv.p.function+"]]{\n\tINS:"+ins+"\n\tOUTS:"+outs+"\n}\n");
			}
			
		}
		
		public void addStorage(Storage storage) {
			for(ProcessingVertex pv : processors.values())
				pv.addStorage(storage);
		}
		
		public void bindProcFunctions() {
			for(ProcessingVertex pv : processors.values())
				pv.p.function = "F"+pv.v.value;
		}
		
		public void addStorages(Collection<Storage> storages) {
			for(ProcessingVertex pv : processors.values())
				pv.addStorages(storages);
		}
		
		Map<Vertex, ProcessingVertex> processors;
		Component core;
		Graph g;
	}
	
	public static class GPUVertex {
		
		public Component gpu;

		public GPUVertex(Component gpu) {
			this.gpu = gpu;
			cores = new ArrayList<>();
		}
		
		public void addCore(CoreVertex core) {
			cores.add(core);
		}
		
		public void addCache(Storage storage) {
			for(CoreVertex cv : cores)
				cv.addStorage(storage);
		}
		

		public void bindProcFunctions() {
			for(CoreVertex cv : cores)
				cv.bindProcFunctions();
		}
		
		public Collection<CoreVertex> cores;
		//public Collection<Storage> caches;

		public void addStorages(Collection<Storage> storages) {
			for(CoreVertex core : cores)
				core.addStorages(storages);
		}
	}
	
public static class PlatformParameters{
		
		public int sNbProcessors;
		public int sNbCores;
		public int sNbCoreCache;
		public int sNbExtStorages;
		public Collection<Map.Entry<Integer, CapacityParameter>> sInternalCaps;
		public Collection<Map.Entry<Integer, CapacityParameter>> sCacheCaps;
		public int sExternalCap;
		public int sNbTaskDomain;
		public int sPipelineDepth;
		public int nbIntRom;
		public int nbIntRam;
		public int sNbBytesPerCycle;
		public int mainFreq;
		public Collection<Integer> sFreqs;
		public int sNbIntStorages;
		public int nbExtRom;
		public int nbExtRam;
		public int sNbLatency;
		public int costProc;
		public float pAltFreq;
		public float sAltCapacities;
		public float pOptResources;
		
		public PlatformParameters() {
			sInternalCaps = new ArrayList<>();
			sCacheCaps = new ArrayList<>();
			sFreqs = new ArrayList<>();
		}
		
		
		public static class CapacityParameter{
			
			public CapacityParameter(int capacity, int cost) {
				this.capacity = capacity;
				this.cost = cost;
			}
			
			public int capacity;
			public int cost;
		}
	}
	
	public GeneratedPlatform(FunctionalDomain domain, ApplicationParameters app_param, PlatformParameters plt_param, Statistic stats) {
		
		this.domain = domain;
		this.app_param = app_param;
		this.plt_param = plt_param;
		this.stats = stats;
		
		ApplicationParameters core = new ApplicationParameters();
		
		core.sNbInputs = app_param.sNbInputs;
		core.sNbOutputs = app_param.sNbOutputs;
		core.sNbNodes = plt_param.sPipelineDepth;
		
		Statistic core_stats = new Statistic();
		core_stats.avgChild = stats.avgChild;
		core_stats.avgLeaf = stats.avgLeaf;
		core_stats.avgMultiChild = stats.avgMultiChild;
		core_stats.avgMultiParents = stats.avgMultiParents;
		core_stats.avgOrphan = stats.avgOrphan;
		core_stats.avgParents = stats.avgParents;
		
		System.out.println("***** 1 CREATE GRAPH PROCESSOR ARCHITETURE *****");
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Collection<Resource> resources = new ArrayList<>();
		
		float epsilon = 0.6f;
		
		Collection<Graph> graphs = new ArrayList<>();
		Collection<GPUVertex> gpuvs = new ArrayList<>();
		do {
			soc = new Component("Plt");
			graphs.clear();
			gpuvs.clear();
			for(int j = 0; j < plt_param.sNbProcessors; ++j) {
				Component gpu = soc.addComponent("P"+j);
				resources.add(gpu);
				gpu.setCost(plt_param.costProc);
				GPUVertex gpuv = new GPUVertex(gpu);
				gpuvs.add(gpuv);
				for(int i = 0; i < plt_param.sNbCores; ++i) {
					Graph g = null;
					do {
						g = new Graph(core);
					} while(!g.satisfyTopology(core_stats, epsilon));
					g.expandOrphan(core);
					graphs.add(g);
					CoreVertex cv = new CoreVertex(g, gpu.addComponent("C"+i));
					gpuv.addCore(cv);
					
				}
			}
		} while(!_map(graphs, domain, app_param.pMappingRate));
		
		System.out.println("***** ASSESS MAPPABLE PROCESSOR ARCHITETURE *****");
		
		
		for(Graph g : graphs) {
			System.out.println("********* CORE *******");
			for(Vertex v : g.nodes)
				System.out.println(">"+v);
		}
		
		System.out.println("***** 2 BIND FUNCTIONAL DOMAIN TO GRAPHS *****");
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(GPUVertex gpus : gpuvs)
			gpus.bindProcFunctions();
		
		System.out.println("***** 3 CREATE CORES INTERNAL PIPELINES *****");
		try {
			Thread.sleep(3);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(GPUVertex gpuv : gpuvs) {
			for(CoreVertex cv : gpuv.cores) {
				cv.createPipeline();
				System.out.println(Graph.printAdjMatrix(cv.g.toAdjMatrix()));
				
				//for(Vertex v : g.nodes)
					//System.out.println(v);
				cv.print();
				try {
					Thread.sleep(3);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		System.out.println("***** 4 CREATE MEMORY ARCHITECTURE *****");
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Collection<Storage> storages =  new ArrayList<>();
		
		Random r = new Random();
		for(int i = 0; i < plt_param.nbIntRom ; ++i) {
			Storage s = soc.addStorage("IntROM"+i, Type.READ_ONLY);
			s.setBytesPerCycle(plt_param.sNbBytesPerCycle);
			s.setAccessLatency(plt_param.sNbLatency);
			
			do {
				for(Entry<Integer, CapacityParameter> e : plt_param.sInternalCaps)
					if(r.nextFloat() < plt_param.sAltCapacities) {
						s.addCapacity("", e.getValue().capacity, e.getValue().cost);
						cost += e.getValue().cost;
					}
			} while (s.capacities.isEmpty());
			
			do {
				for(Integer f : plt_param.sFreqs)
					if(r.nextFloat() < plt_param.pAltFreq)
						s.setFrequencies(f);
			} while (s.getFrequencies().isEmpty());
			
			for(Integer f : s.getFrequencies())
				System.out.println(s.id+ " : " +f);
			
			resources.add(s);
			storages.add(s);
		}
		
		for(int i = 0; i < plt_param.nbIntRam ; ++i) {
			Storage s = soc.addStorage("IntRAM"+i, Type.READ_AND_WRITE);
			s.setBytesPerCycle(plt_param.sNbBytesPerCycle);
			s.setAccessLatency(plt_param.sNbLatency);
			
			do {
				for(Entry<Integer, CapacityParameter> e : plt_param.sInternalCaps)
					if(r.nextFloat() < plt_param.sAltCapacities) {
						s.addCapacity("", e.getValue().capacity, e.getValue().cost);
						cost += e.getValue().cost;
					}
			} while (s.capacities.isEmpty());
			
			do {
				for(Integer f : plt_param.sFreqs)
					if(r.nextFloat() < plt_param.pAltFreq)
						s.setFrequencies(f);
			} while (s.getFrequencies().isEmpty());
			
			for(Integer f : s.getFrequencies())
				System.out.println(s.id+ " : " +f);
			
			resources.add(s);
			storages.add(s);
		}
		
		for(int i = 0; i < plt_param.nbExtRom ; ++i) {
			Storage s = soc.addStorage("ExtROM"+i, Type.READ_ONLY);
			s.setBytesPerCycle(plt_param.sNbBytesPerCycle);
			s.setAccessLatency(plt_param.sNbLatency*2);
			s.addCapacity("", plt_param.sExternalCap, 0);
			
			do {
				for(Integer f : plt_param.sFreqs)
					if(r.nextFloat() < plt_param.pAltFreq)
						s.setFrequencies(f/4);
			} while (s.getFrequencies().isEmpty());
			
			for(Integer f : s.getFrequencies())
				System.out.println(s.id+ " : " +f);
			
			resources.add(s);
			storages.add(s);
		}
		
		for(int i = 0; i < plt_param.nbExtRam ; ++i) {
			Storage s = soc.addStorage("ExtRAM"+i, Type.READ_AND_WRITE);
			s.setBytesPerCycle(plt_param.sNbBytesPerCycle);
			s.setAccessLatency(plt_param.sNbLatency*2);
			s.addCapacity("", plt_param.sExternalCap, 0);
			
			do {
				for(Integer f : plt_param.sFreqs)
					if(r.nextFloat() < plt_param.pAltFreq)
						s.setFrequencies(f/4);
			} while (s.getFrequencies().isEmpty());
			
			for(Integer f : s.getFrequencies())
				System.out.println(s.id+ " : " +f);
			
			resources.add(s);
			storages.add(s);
		}
		
		for(GPUVertex gpuv : gpuvs) {
			
			do {
				for(Integer f : plt_param.sFreqs)
					if(r.nextFloat() < plt_param.pAltFreq)
						gpuv.gpu.setFrequencies(f);
			} while (gpuv.gpu.getFrequencies().isEmpty());
			
			for(Integer f : gpuv.gpu.getFrequencies())
				System.out.println(gpuv.gpu.id+ " : " +f);
				
			gpuv.addStorages(storages);
			for(int i = 0; i < plt_param.sNbCoreCache; ++i) {
				Storage cache = gpuv.gpu.addStorage("C"+i, Type.READ_AND_WRITE);
				resources.add(cache);
				do {
					for(Entry<Integer, CapacityParameter> e : plt_param.sInternalCaps)
						if(r.nextFloat() < plt_param.sAltCapacities) {
							cache.addCapacity("", e.getValue().capacity/2, e.getValue().cost);
							cost += e.getValue().cost;
						}
				} while (cache.capacities.isEmpty());
			
				cache.setBytesPerCycle(plt_param.sNbBytesPerCycle*2);
				cache.setAccessLatency(0);
				gpuv.addCache(cache);
			}
		}
		
		for(Resource c : resources) {
			if(r.nextFloat() < plt_param.pOptResources) {
				c.setOptional();
				cost += c.cost;
			}
		}
		
		System.out.println("****************************************************\n" +
				"******************** PLATFORM *********************\n" +
				"***************************************************\n" +
				"******************** GENERATED ********************\n" +
				"***************************************************");
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private boolean _map(Collection<Graph> graphs, FunctionalDomain domain, float mappingRate) {
		List<Vertex> nodes = new ArrayList<>();
		for(Graph g : graphs)
			nodes.addAll(g.nodes);
		Collections.shuffle(nodes);
		return domain.map(nodes, mappingRate);
	}

	public Component soc;
	public Collection<GPUVertex> gpus;
	public Collection<Storage> storages;
	public FunctionalDomain domain;
	public ApplicationParameters app_param;
	public PlatformParameters plt_param;
	public Statistic stats;
	public int cost;
}
