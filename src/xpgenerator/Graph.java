package xpgenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import xpgenerator.GeneratedApplication.ApplicationParameters;

import java.util.Random;

public class Graph implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3228412371906991511L;

	public static class Statistic{
		public int nbFlows = 0;
		public float avgParents = 0.f;
		public float avgMultiParents = 0.f;
		public float avgOrphan = 0.f;
		public float avgChild = 0.f;
		public float avgMultiChild = 0.f;
		public float avgLeaf = 0.f;
		public float avgSplit = 0.f;
		public float avgJoin = 0.f;
		public float avgVariableTask = 0.f;
		public float avgDataSize;
		public float avgDataNbSize;
		public int nbNodes;
	}
	
	public int idx;
	public Collection<Vertex> nodes;
	
	public Graph(ApplicationParameters param) {
		nodes = new ArrayList<>();
		do {
			idx = 0;
			nodes.clear();
			_createTaskGraph(param, nodes);
		} while(idx < param.sNbNodes);
	}
	
	public int[][] toValuedAdjMatrix(){
		int[][] res = new int[idx][idx];
		
		for(int i = 0; i < idx; ++i)
			for(int j = 0; j < idx; ++j)
				res[i][j]=-1;
		
		for(Vertex n : nodes)
			for(Vertex c : n.children) {
				res[n.id][c.id] = c.value;
			}
		return res;
	}
	
	public int[][] toAdjMatrix(){
		int[][] res = new int[idx][idx];
		
		for(int i = 0; i < idx; ++i)
			for(int j = 0; j < idx; ++j)
				res[i][j]=-1;
		
		for(Vertex n : nodes)
			for(Vertex c : n.children) {
				res[n.id][c.id] = n.split? -2: c.id;
			}
		return res;
	}
	
	public int[][] toReverseAdjMatrix() {
		int[][] res = new int[idx][idx];
		
		for(int i = 0; i < idx; ++i)
			for(int j = 0; j < idx; ++j)
				res[i][j]=-1;
		
		for(Vertex n : nodes)
			for(Vertex c : n.parents)
				res[n.id][c.id] = n.join? -2: c.id;
		return res;
	}
	
	public static String printAdjMatrix(int[][] m) {
		String res = "";
		res+="   ";
		for(int i = 0; i < m[0].length; ++i) 
			res+=i+":";
		res+="\n";
		
		for(int i = 0; i < m[0].length; ++i) {
			res+=i+": ";
			for(int j = 0; j < m[0].length; ++j)
				
				if(i==j)
					res+="+ ";
				else if(m[i][j]==-1)
					res+="o ";
				else if(m[i][j]==-2) 
					res+="x ";
				else
					res+=m[i][j]+" ";
			res+="\n";
		}
		return res;
	}
	
	public Statistic getStats() {
		Statistic s = new Statistic();
		
		for(Vertex n : nodes) {
			if(n.children.size()==0)
				++s.avgLeaf;
			else if(n.children.size() == 1)
				++s.avgChild;
			else
				if(n.split) ++s.avgSplit; else ++s.avgMultiChild;
			
			if(n.parents.size()==0)
				++s.avgOrphan;
			else if(n.parents.size() == 1)
				++s.avgParents;
			else
				if(n.join) ++s.avgJoin; else ++s.avgMultiParents;
			//res+="\n average parent "+avgParents/nodes.size()+" children "+avgChild/nodes.size();
		}
		
		Collection<Vertex> variableTask = new HashSet<>();
		for(Entry<Vertex, Collection<Collection<Vertex>>> e : getSplits().entrySet())
			for(Collection<Vertex> c : e.getValue())
				variableTask.addAll(c);
		for(Entry<Vertex, Collection<Collection<Vertex>>> e : getJoins().entrySet())
			for(Collection<Vertex> c : e.getValue())
				variableTask.addAll(c);
		
		//s.nbFlows = _getNbConnex();
		
		s.avgOrphan/=nodes.size();
		s.avgChild/=nodes.size();
		s.avgJoin/=nodes.size();
		s.avgLeaf/=nodes.size();
		s.avgMultiChild/=nodes.size();
		s.avgMultiParents/=nodes.size();
		s.avgParents/=nodes.size();
		s.avgSplit/=nodes.size();
		s.nbNodes = nodes.size();
		s.avgVariableTask = variableTask.size()/(float)nodes.size();
		return s;
	}

	public Map<Vertex, Collection<Collection<Vertex>>> getSplits(){
		Map<Vertex, Collection<Collection<Vertex>>> res = new HashMap<>();
		for(Vertex n : nodes)
			if(n.split)
				res.put(n, getSplit(n));
		return res;
	}
	
	public Collection<Collection<Vertex>> getSplit(Vertex n){
		assert(n.split);
		Collection<Collection<Vertex>> res = new ArrayList<>();
		for(Vertex c : n.children)
			c.parents.remove(n);
		
		for(Vertex c : n.children)
			res.add(explore(c));
		
		for(Vertex c : n.children)
			c.parents.add(n);
		return res;
	}
	
	public Map<Vertex,Collection<Collection<Vertex>>> getJoins(){
		Map<Vertex, Collection<Collection<Vertex>>> res = new HashMap<>();
		for(Vertex n : nodes)
			if(n.join)
				res.put(n, getJoin(n));
		return res;
	}
	
	public Collection<Collection<Vertex>> getJoin(Vertex n){
		assert(n.join);
		Collection<Collection<Vertex>> res = new ArrayList<>();
		for(Vertex c : n.parents)
			c.children.remove(n);
		
		for(Vertex c : n.parents)
			res.add(explore(c));
		
		for(Vertex c : n.parents)
			c.children.add(n);
		
		return res;
	}
	
	private int _getNbConnex(){
		
		int res = 0;
		Collection<Vertex> flags = new ArrayList<>();
		Collection<Vertex> tmp = new HashSet<>();
		
		for(Vertex n : nodes)
			if(!flags.contains(n)) {
				dfs(n, flags, tmp);
				++res;
			}
		
		return res;
	}
	
	public int getNbPaths() {
		int res = 0;
		for(Vertex v : nodes) {
			if(v.split)
				++res;
			else {
				boolean join = false;
				for(Vertex c : v.children)
					if(c.join) {
						join = true;
						assert(v.children.size() == 1);
					}
				if(!join)
					res += v.children.size();
			}
			if(v.join) {
				res += 1;
				for(Vertex p : v.parents)
					assert(p.children.size() == 1);
			}
		}
		return res;
	}
	
	public Collection<Vertex> explore(Vertex n){
		
		Collection<Vertex> res = new HashSet<>();
		Collection<Vertex> flags = new ArrayList<>();
		
		res.add(n);
		dfs(n, flags, res);
		
		return res;
	}
	
	private void dfs(Vertex v, Collection<Vertex> flags, Collection<Vertex> res) {
		
		flags.add(v);
		
		if(v.join || v.split)
			return;
		
		res.add(v);
		
		Collection<Vertex> neigh = new HashSet<>();
		neigh.addAll(v.parents);
		neigh.addAll(v.children);
		
		for(Vertex n : neigh)
			if(!flags.contains(n))
				dfs(n, flags, res);
	}

	
	public boolean satisfy(Statistic stats) {
		return satisfy(stats, 0.f);
	}
	
	public boolean satisfyTopology(Statistic stats, float epsilon) {
		
		Statistic s = getStats();
		
		/*if(s.avgOrphan + epsilon >= stats.avgOrphan)
			System.out.println("orphan : \t\tsatisfied");
		else
			System.out.println("orphan : \t\tviolated");
		
		if(s.avgChild + epsilon>= stats.avgChild)
			System.out.println("child : \t\tsatisfied");
		else
			System.out.println("child : \t\tviolated");
		
		if(s.avgJoin + epsilon>= stats.avgJoin)
			System.out.println("join : \t\t\tsatisfied");
		else
			System.out.println("join : \t\t\tviolated");
		
		if(s.avgLeaf + epsilon>= stats.avgLeaf)
			System.out.println("leaf : \t\t\tsatisfied");
		else
			System.out.println("leaf : \t\t\tviolated");
		
		if(s.avgMultiChild + epsilon >= stats.avgMultiChild)
			System.out.println("multi-child : \t\tsatisfied");
		else
			System.out.println("multi-child : \t\tviolated");
		
		if(s.avgMultiParents + epsilon >= s.avgMultiParents)
			System.out.println("multi-parent : \t\tsatisfied");
		else
			System.out.println("multi-parent : \t\tviolated");
		
		if(s.avgParents + epsilon >= stats.avgParents)
			System.out.println("parent : \t\tsatisfied");
		else
			System.out.println("parent : \t\tviolated");
		
		if(s.avgSplit + epsilon >= stats.avgSplit)
			System.out.println("split : \t\tsatisfied");
		else
			System.out.println("split : \t\tviolated");
		
		if(s.nbNodes + epsilon>= stats.nbNodes)
			System.out.println("nb nodes : \t\tsatisfied");
		else
			System.out.println("nb nodes : \t\tviolated");
		
		if(s.avgVariableTask <= epsilon + stats.avgVariableTask)
			System.out.println("vtask : \t\tsatisfied");
		else
			System.out.println("vtask : \t\tviolated");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return //s.avgOrphan + epsilon >= stats.avgOrphan &&
				s.avgChild + epsilon>= stats.avgChild &&
				//s.avgJoin + epsilon>= stats.avgJoin &&
				s.avgLeaf + epsilon>= stats.avgLeaf &&
				s.avgMultiChild + epsilon >= stats.avgMultiChild &&
				s.avgMultiParents + epsilon >= s.avgMultiParents &&
				s.avgParents + epsilon >= stats.avgParents;
				//s.avgSplit + epsilon >= stats.avgSplit &&
				//s.avgVariableTask <= epsilon + stats.avgVariableTask;
	}

	public boolean satisfy(Statistic stats, float epsilon) {
		Statistic s = getStats();
		
		/*if(s.avgOrphan + epsilon >= stats.avgOrphan)
			System.out.println("orphan : \t\tsatisfied");
		else
			System.out.println("orphan : \t\tviolated");
		
		if(s.avgChild + epsilon>= stats.avgChild)
			System.out.println("child : \t\tsatisfied");
		else
			System.out.println("child : \t\tviolated");
		
		if(s.avgJoin + epsilon>= stats.avgJoin)
			System.out.println("join : \t\t\tsatisfied");
		else
			System.out.println("join : \t\t\tviolated");
		
		if(s.avgLeaf + epsilon>= stats.avgLeaf)
			System.out.println("leaf : \t\t\tsatisfied");
		else
			System.out.println("leaf : \t\t\tviolated");
		
		if(s.avgMultiChild + epsilon >= stats.avgMultiChild)
			System.out.println("multi-child : \t\tsatisfied");
		else
			System.out.println("multi-child : \t\tviolated");
		
		if(s.avgMultiParents + epsilon >= s.avgMultiParents)
			System.out.println("multi-parent : \t\tsatisfied");
		else
			System.out.println("multi-parent : \t\tviolated");
		
		if(s.avgParents + epsilon >= stats.avgParents)
			System.out.println("parent : \t\tsatisfied");
		else
			System.out.println("parent : \t\tviolated");
		
		if(s.avgSplit + epsilon >= stats.avgSplit)
			System.out.println("split : \t\tsatisfied");
		else
			System.out.println("split : \t\tviolated");
		
		if(s.nbNodes + epsilon>= stats.nbNodes)
			System.out.println("nb nodes : \t\tsatisfied");
		else
			System.out.println("nb nodes : \t\tviolated");
		
		if(s.avgVariableTask <= epsilon + stats.avgVariableTask)
			System.out.println("vtask : \t\tsatisfied");
		else
			System.out.println("vtask : \t\tviolated");*/
		
		
		return s.avgOrphan + epsilon >= stats.avgOrphan &&
				s.avgChild + epsilon>= stats.avgChild &&
				s.avgJoin + epsilon>= stats.avgJoin &&
				s.avgLeaf + epsilon>= stats.avgLeaf &&
				s.avgMultiChild + epsilon >= stats.avgMultiChild &&
				s.avgMultiParents + epsilon >= s.avgMultiParents &&
				s.avgParents + epsilon >= stats.avgParents &&
				s.avgSplit + epsilon >= stats.avgSplit &&
				//s.nbNodes + epsilon>= stats.nbNodes &&
				s.avgVariableTask - stats.avgVariableTask <= 0.3f;
	}
	
	public String printStatistics() {
		String res = "";
		
		Statistic s = getStats();
		
		res+="\norphan/leaf \t\t"+s.avgOrphan+"/"+s.avgLeaf+" | \nparents/child \t\t"+s.avgParents+"/"+s.avgChild+" | \nmulti-parents,child \t"+s.avgMultiParents+"/"+s.avgMultiChild+"|\nsplit/join \t\t"+s.avgSplit+"/"+s.avgJoin;
		res+="\nvtask \t\t\t"+s.avgVariableTask;
		return res;
	}
	
	private Vertex _createTaskGraph(ApplicationParameters param, Collection<Vertex> v) {
		Random r = new Random();
		Vertex node = new Vertex(idx++);
		nodes.add(node);
		
		int nbInputs = _getNbInputs(param);
		int nbOutputs = _getNbOutouts(param);
	
		for(int i = 0; i < nbInputs && idx < param.sNbNodes; ++i){	
			if(r.nextFloat() > 0.f) {
				Vertex parent = _createTaskGraph(param, nodes);
				node.parents.add(parent);
				parent.children.add(node);
			}
		}
		
		for(int i = 0; i < nbOutputs && idx < param.sNbNodes; ++i) {
			if(r.nextFloat() > 0.f) {
				Vertex child = _createTaskGraph(param, nodes);
				child.parents.add(node);
				node.children.add(child);
			}
		}
		
		return node;
	}

	public Collection<Vertex> getSortedGraph(){
		Deque<Vertex> result = new LinkedList<>();
		Map<Vertex, Integer> colors = new HashMap<>();
		
		for(Vertex n : nodes) colors.put(n, 0);
		
		for(Vertex n : nodes)
			if(colors.get(n) == 0)
				_topologicalSort(nodes, result, n, colors);
				
		return result;
	}
	
	private void _topologicalSort(Collection<Vertex> nodes, Deque<Vertex> result, Vertex n, Map<Vertex, Integer> colors) {
		colors.put(n, 1);
		for(Vertex children : n.parents)
			if(colors.get(children) == 0)
				_topologicalSort(nodes, result, children, colors);
		colors.put(n, 2);
		result.addFirst(n);
	}
	
	public void addFlowVariability(ApplicationParameters param) {
		//Collection<Vertex> toDel = new HashSet<>();
		
		
		
		for(Vertex n : nodes) {
			if(n.children.size() > 1 && _isVariablePath(param)) {
				n.split = true;
				for(Vertex alt : n .children) {
					
					for(Vertex parent_alt : alt.parents) {
						
						if(parent_alt != n)
							parent_alt.children.remove(alt);
						
						//if(parent_alt.children.isEmpty() && parent_alt.parents.isEmpty())
							//toDel.add(parent_alt);
					}
					
					alt.parents.clear();
					alt.parents.add(n);
				}
			}
		}
		
		for(Vertex n : nodes) {
			if(n.parents.size() > 1  && _isVariablePath(param)) {
				n.join = true;
				for(Vertex alt : n.parents) {
					
					for(Vertex alt_child : alt.children) {
						
						if(alt_child != n)
							alt_child.parents.remove(alt);
						
						//if(alt_child.parents.isEmpty() && alt_child.children.isEmpty())
							//toDel.add(alt_child);
					}
					
					alt.children.clear();
					alt.children.add(n);
				}
			}
		}
		/*if(toDel.size() > 0) {
			System.out.println("TO DEL !!!");
			printAdjMatrix(toAdjMatrix());
			nodes.removeAll(toDel);
			toDel.clear();
			printAdjMatrix(toAdjMatrix());
		}
		
		for(Vertex n : nodes)
			if(n.children.size() == 0 && n.parents.size() == 0)
				toDel.add(n);
		for(Vertex v : toDel)
			nodes.remove(v);
		
		for(Vertex v : getSortedGraph())
			if(v.children.size() > 0 || v.parents.size() > 0) {
				System.out.println("IF ALONE !!!");
				assert(getSortedGraph().size() > 0);
				printAdjMatrix(toAdjMatrix());
				nodes.removeAll(toDel);
				printAdjMatrix(toAdjMatrix());
			}
				
		for(Vertex del : toDel)
			assert(!nodes.contains(del));*/
	}
	
	public void expandOrphan(ApplicationParameters param) {
		for(Vertex v : nodes)
			if(v.parents.size() == 0) {
				int nbInputs = _getNbInputs(param);
				if(nbInputs == 0) nbInputs = 1; 
				for(int i = 0; i < nbInputs; ++i)
					v.parents.add(null);
			}
	}
	
	public void clearVoidVertex() {
		Collection<Vertex> oldNodes = new ArrayList<>(nodes);
		nodes = nodes.stream().filter(e -> e.children.size() > 0 || e.parents.size() > 0).collect(Collectors.toList());
		
		System.out.println("***** CLEAR AND CLEAN VERTEX *****");
		
		/*for(Vertex v : oldNodes)
			if(!nodes.contains(v)) {
				System.out.println("-> "+v);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}*/
		
		
		for(Vertex v : nodes)
			if(v.children.size() == 0 && v.parents.size() == 0) {
				System.out.println("-> "+v);
				assert(false);
			}
	}

	private boolean _isVariablePath(ApplicationParameters param) {
		Random r = new Random();
		return r.nextFloat() < param.pVariableFlow;
	}

	private int _getNbOutouts(ApplicationParameters param) {
		Random r = new Random();
		int outputs = (int) Math.round((double) (r.nextGaussian() + param.sNbOutputs));
		outputs = outputs < 0 ? 0 : outputs;
		return outputs;
	}

	private int _getNbInputs(ApplicationParameters param) {
		Random r = new Random();
		int inputs = (int) Math.round((double) (r.nextGaussian() + param.sNbInputs));
		inputs = inputs < 0 ? 0 : inputs;
		return inputs;
	}
	
	public static String printSplit(Graph a, Graph b) {
		
		
		Graph min = _getSubGraph(a, b);
		Graph max = _getOverGraph(a, b);
		
		int[][] min_m = min.toAdjMatrix();
		int[][] max_m = max.toAdjMatrix();
		
		int[][] xm = new int[max.idx][max.idx];
		for(int i = 0; i < min.idx; ++i) {
			for(int j = 0; j < min.idx; ++j) {
				xm[i][j]=-1;
			}
		}
		
		for(int i = 0; i < min.idx; ++i) {
			int j = 0;
			for(; j < min.idx; ++j) {
				if(i != j) {
					if(min_m[i][j] == max_m[i][j] && min_m[i][j] == 1) {
						xm[i][j] = j;
					} else if (min_m[i][j] > max_m[i][j] && xm[i][j] == -1){
						int k = j;
						for(; k < a.idx; ++k)
							if(min_m[i][k] < max_m[i][k]) {
								xm[i][j] = k;
								xm[i][k] = j;
								min_m[i][j] = max_m[i][k] = 0;
								break;
							}
						if(k==a.idx)
							xm[i][j] = j;
							
								
					} else if (min_m[i][j] < max_m[i][j] && xm[i][j] == -1){
						int k = j;
						for(; k < a.idx; ++k)
							if(min_m[i][k] > max_m[i][k]) {
								xm[i][j] = k;
								xm[i][k] = j;
								min_m[i][j] = max_m[i][k] = 0;
								break;
							}
						if(k==a.idx)
							xm[i][j] = j;
					}
				}
			}
			for(; j < max.idx; ++j) {
				xm[i][j] = max_m[i][j];
			}
		}
		return printAdjMatrix(xm);
	}
	
	public static String printJoin(Graph a, Graph b) {
		
		
		Graph min = _getSubGraph(a, b);
		Graph max = _getOverGraph(a, b);
		
		int[][] min_m = min.toReverseAdjMatrix();
		int[][] max_m = max.toReverseAdjMatrix();
		
		int[][] xm = new int[max.idx][max.idx];
		for(int i = 0; i < min.idx; ++i) {
			for(int j = 0; j < min.idx; ++j) {
				xm[i][j]=-1;
			}
		}
		
		for(int i = 0; i < min.idx; ++i) {
			int j = 0;
			for(; j < min.idx; ++j) {
				if(i != j) {
					if(min_m[i][j] == max_m[i][j] && min_m[i][j] == 1) {
						xm[i][j] = j;
					} else if (min_m[i][j] > max_m[i][j] && xm[i][j] == -1){
						int k = j;
						for(; k < a.idx; ++k)
							if(min_m[i][k] < max_m[i][k]) {
								xm[i][j] = k;
								xm[i][k] = j;
								min_m[i][j] = max_m[i][k] = 0;
								break;
							}
						if(k==a.idx)
							xm[i][j] = j;
							
								
					} else if (min_m[i][j] < max_m[i][j] && xm[i][j] == -1){
						int k = j;
						for(; k < a.idx; ++k)
							if(min_m[i][k] > max_m[i][k]) {
								xm[i][j] = k;
								xm[i][k] = j;
								min_m[i][j] = max_m[i][k] = 0;
								break;
							}
						if(k==a.idx)
							xm[i][j] = j;
					}
				}
			}
			for(; j < max.idx; ++j) {
				xm[i][j] = max_m[i][j];
			}
		}
		return printAdjMatrix(xm);
	}

	private static Graph _getOverGraph(Graph a, Graph b) {
		return (a.idx < b.idx)? b : a;
	}

	private static Graph _getSubGraph(Graph a, Graph b) {
		return (a.idx < b.idx)? a : b;
	}

	public String printValues() {
		return printAdjMatrix(toValuedAdjMatrix());
	}

}
