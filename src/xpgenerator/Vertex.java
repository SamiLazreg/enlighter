package xpgenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import application.Path;

public class Vertex implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5244646751318239389L;
	
	public Collection<Vertex> parents;
	private Collection<String> ins;
	public boolean join;
	public Collection<Vertex> children;
	private Collection<String> outs;
	public boolean split;
	
	public int id;
	public int value;
	
	public Vertex(int id) {
		this.id = id;
		parents = new ArrayList<>();
		children = new ArrayList<>();
	}
	
	public void setInputs(Collection<String> ins) {
		for(String s : ins)
			assert(!s.contains("_o_"));
		this.ins = new ArrayList<>(ins);
	}
	
	public String getInput(int i) {
		return ((List<String>)ins).get(i);
	}
	
	public void setOutputs(Collection<String> outs) {
		for(String s : outs)
			assert(!s.contains("_i_"));
		this.outs = new ArrayList<>(outs);
	}
	
	public String getOutput(int i) {
		return ((List<String>)outs).get(i);
	}
	
	public String toString() {
		String res = "";
		String outs = "";
		String ins = "";
		
		if(parents.size()>0)
			ins+="{";
		else
			ins+="X";
		for(Vertex p : parents)
			if(p==null)
				ins+=" VOID ";
			else
				ins+=""+p.id+" ";
		if(parents.size()>0)
			ins+="}";
		
		if(children.size()>0)
			outs+="{";
		else
			outs+="X";
		for(Vertex c : children)
			outs+=""+c.id+" ";
		if(children.size()>0)
			outs+="}";
		
		res+="["+id+" f:"+value+"|("+ins+";"+outs+")]";
		return res;
	}

	public Collection<String> getInputs() {
		return ins;
	}
	
	public Collection<String> getOutputs() {
		return outs;
	}
}
