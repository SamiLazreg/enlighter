package xpgenerator;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class FunctionalDomain {

	
	public String toString() {
		String res = "***** FUNCTIONAL DOMAIN *****\n";
		res+="Domain size : "+nbFunctions+"\n";
		for(Function f : inApp.values())
			res+="APP - "+f.value+" : ("+f.nbInputs+"),("+f.nbOutputs+")\n";
		for(Function f : instancied.values())
			res+="PLT - "+f.value+" : ("+f.nbInputs+"),("+f.nbOutputs+")\n";
		for(int i = 1; i <= nbFunctions; ++i)
			if(!inApp.containsKey(i) && !instancied.containsKey(i))
				res+="FREE - "+i+" : (free),(free)\n";
		res += "***** ********* *****";
		return res;
	}
	
	public static class Type{
		public int value;
		public String string;
	}
	
	public static class Function{
		
		public Function(int value, int nbInputs, int nbOutputs) {
			assert(value != 0);
			this.value = value;
			this.nbInputs = nbInputs;
			this.nbOutputs = nbOutputs;
			this.ins = new ArrayList<>();
			this.outs = new ArrayList<>();
			string = _Convert(value);
			for(int k = 0; k < this.nbInputs; ++k)
				ins.add(string+"_i_"+k);
			for(int k = 0; k < this.nbOutputs; ++k)
				outs.add(string+"_o_"+k);
			assert(ins != null && outs != null);
			assert(ins.size() > 0);
		}
		
		private String _Convert(int value) {
			return "F"+value;
		}

		public int value;
		public int nbInputs;
		public int nbOutputs;
		public Collection<Type> types;
		public String string;
		public Collection<String> ins;
		public Collection<String> outs;
		
	}
	
	public FunctionalDomain(int domainSize){
		nbFunctions = domainSize;
		mean = nbFunctions/2.f;
		deviation = 1.1f;
		inApp = new HashMap<>();
		instancied = new HashMap<>();
		r = new Random();
	}
	
	public boolean map(List<Vertex> nodes, float mappingRate) {
		for(Function f : inApp.values()) {
			boolean found = false;
			for(Vertex v : nodes) {
				if(v.value == 0 && f.nbInputs == v.parents.size() && f.nbOutputs == v.children.size()) {
					
					v.value = f.value;
					assert(f.value != 0);
					assert(f.ins != null && f.outs != null);
					assert(f.ins.size() > 0);
					assert(f.ins.size() == v.parents.size() && f.outs.size() == v.children.size());
					v.setInputs(f.ins);
					v.setOutputs(f.outs);
					
					found = true;
					break;
				}
			}
			if(!found)
				return false;
		}
		
		int i = 0;
		for(Vertex v : nodes) {
			
			Function fct = null;
			if(v.value == 0) {
				if(r.nextFloat() <= mappingRate && existFctInApp(v.parents.size(), v.children.size())) {
					fct = getFctInApp(v.parents.size(), v.children.size());
					System.out.println("platform node : "+(i++)+" mapped "+fct.value+" :\t("+v.parents.size()+":"+v.children.size()+")");
					
				} else {
					fct = getFctNotInApp(v.parents.size(), v.children.size());
					System.out.println("platform node : "+(i++)+" freefct "+fct.value+" :\t("+v.parents.size()+":"+v.children.size()+")");
				}
				v.value = fct.value;
				assert(fct.value != 0);
				assert(fct.ins != null && fct.outs != null);
				assert(fct.ins.size() > 0);
				assert(fct.ins.size() == v.parents.size() && fct.outs.size() == v.children.size());
				v.setInputs(fct.ins);
				v.setOutputs(fct.outs);
			}
		}
		
		for(Vertex v : nodes) {
			assert(v.value != 0);
			assert(v.parents.size()>0);
		}
		
		System.out.println("*********** PLATFORM MAPPED ************");
		
		return true;
	}
	
	private boolean existFctInApp(int nbIns, int nbOuts) {
		for(Function f : inApp.values()) {
			if(f.nbInputs == nbIns && f.nbOutputs == nbOuts)
				return true;
		}
		return false;
	}

	public Function setFctInApp(int nbIns, int nbOuts) {
		int fct = 0;
		int i = 0;
		int j = 0;
		float localDeviaton = deviation;
		do {
			assert(existFreeFct(nbIns, nbOuts));
			if(j > nbFunctions) {
				localDeviaton *= 1.5;
				j = 0;
			}
			
			fct = (int)(r.nextGaussian() * localDeviaton + mean);
			if(fct <= 0)
				fct = 1;
			else if(fct > nbFunctions)
				fct = nbFunctions;
			
			Function f = null;
			if(inApp.containsKey(fct)) {
				f = inApp.get(fct);
				assert(f != null);
				if(f.nbInputs == nbIns && f.nbOutputs == nbOuts)
					return f;
			} else {
				f = new Function(fct, nbIns, nbOuts);
				inApp.put(fct, f);
				return f;
			}
			
			/*if(i > 10 && f != null) {
				System.out.println("("+nbIns+":"+nbOuts+") != "+fct+"("+f.nbInputs+":"+f.nbOutputs+")");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}*/
			
			++i;
			++j;
			assert(i < 1000);
			
		} while (true);
	}
	
	private boolean existFreeFct(int nbIns, int nbOuts) {
		if(inApp.size() >= nbFunctions) {
			System.out.println("FCT DOMAIN TOO SMALL FOR APP");
			System.out.println(this);
			return false;
		}
		if(instancied.size() + inApp.size() >= nbFunctions) {
			for(Function f : instancied.values())
				if(f.nbInputs == nbIns && f.nbOutputs == nbOuts)
					return true;
			System.out.println("FCT DOMAIN TOO SMALL TO IMPLEMENT A NEW FCT: ("+nbIns+")("+nbOuts+")");
			System.out.println(this);
			return false;
		}
		for(int i = 1; i <= nbFunctions; ++i)
			if(!instancied.containsKey(i))
				return true;
		System.out.println("FCT DOMAIN TOO SMALL");
		System.out.println(this);
		return false;
	}

	public Function getFctNotInApp(int nbIns, int nbOuts) {
		
		assert(existFreeFct(nbIns, nbOuts));
		
		int fct = 0;
		float localDeviation = deviation;
		int i = 0;
		do {
			fct = (int)(r.nextGaussian() * localDeviation + mean);
			if(fct <= 0)
				fct = 1;
			else if(fct > nbFunctions)
				fct = nbFunctions;
			
			Function f = null;
			if(!inApp.containsKey(fct)) {
				if(instancied.containsKey(fct)) {
					f = instancied.get(fct);
					if(f.nbInputs == nbIns && f.nbOutputs == nbOuts) {
						return f;
					}
				} else {
					f = new Function(fct, nbIns, nbOuts);
					instancied.put(fct, f);
					return f;
				}
			}
			
			if(++i > 10000) {
				localDeviation *= 1.01f;
				i = 0;
			}
			if(localDeviation> mean)
				localDeviation = 0.01f;
			
			//System.out.println("getFctNotInApp "+fct+" | ("+nbIns+","+nbOuts+")");
		} while (true);
	}

	public Function getFctInApp(int nbIns, int nbOuts) {
		
		assert(existFctInApp(nbIns, nbOuts));
		
		int fct = 0;
		float localDeviation = deviation;
		int i = 0;
		do {
			fct = (int)(r.nextGaussian() * localDeviation + mean);
			if(fct <= 0)
				fct = 1;
			else if(fct > nbFunctions)
				fct = nbFunctions;
			
			if(inApp.containsKey(fct)) {
				Function f = inApp.get(fct);
				if(f.nbInputs == nbIns && f.nbOutputs == nbOuts) {
					return f;
				}
			}
			
			if(++i > 10000) {
				localDeviation *= 1.01f;
				i = 0;
			}
			if(localDeviation> mean)
				localDeviation = 0.01f;
			
			System.out.println("getFctInApp");
			
		} while (true);
	}
	
	int nbFunctions;
	float mean; 
	float deviation;
	Random r;
	Map<Integer, Function> inApp;
	Map<Integer, Function> instancied;
	
}
