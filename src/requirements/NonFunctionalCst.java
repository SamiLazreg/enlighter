package requirements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NonFunctionalCst {

	public enum Type {
		AND,
		OR,
		LEAF,
		NOT,
		REQ,
		IFF,
	}
	
	public enum Op{
		NA,
		LEQ,
		GEQ,
		L,
		G,
		EQ;
		public String toString() {
			return str;
		}
		String str;
	}
	
	public final static int _NONE = 0x0;
	public final static int _QUALITY_LOSS = 0x1;
	public final static int _COST = 0x2;
	public final static int _RUN_TIME = 0x4;
	
	private NonFunctionalCst(Type type, int nfp, Op op, int value) {
		this.type = type;
		this.nfp = nfp;
		this.op = op;
		this.value = value;
		nodes = null;
	}
	
	private NonFunctionalCst(Type type, int nfp, Collection<NonFunctionalCst> csts) {
		this.type = type;
		this.nfp = nfp;
		this.op = Op.NA;
		this.value = -1;
		this.nodes = new ArrayList<>(csts);
	}
	
	public NonFunctionalCst get(int nfp) {
		for(NonFunctionalCst n : nodes)
			if(n.nfp == nfp)
				return n;
		assert(false);
		return null;
	}
	
	public static NonFunctionalCst AND(NonFunctionalCst... csts) {
		return _node(Type.AND, csts);
	}
	
	public static NonFunctionalCst OR(NonFunctionalCst... csts) {
		return _node(Type.OR, csts);
	}
	
	public static NonFunctionalCst REQ(NonFunctionalCst... csts) {
		return _node(Type.REQ, csts);
	}
	
	public static NonFunctionalCst IFF(NonFunctionalCst... csts) {
		return _node(Type.IFF, csts);
	}
	
	public static NonFunctionalCst NOT(NonFunctionalCst... csts) {
		return _node(Type.NOT, csts);
	}
	
	private static NonFunctionalCst _node(Type type, NonFunctionalCst... csts) {
		int nfps = _NONE;
		Collection<NonFunctionalCst> children = new ArrayList<>();
		for(NonFunctionalCst cst : csts) {
			nfps &= cst.nfp;
			children.add(cst);
		}
		return new NonFunctionalCst(type, nfps, children);
	}
	
	public static NonFunctionalCst QUALITY_LOSS(String op, int value) {
		return _leaf(op, _QUALITY_LOSS, value);
	}
	
	public static NonFunctionalCst COST(String op, int value) {
		return _leaf(op, _COST, value);
	}
	
	public static NonFunctionalCst RUN_TIME(String op, int value) {
		return _leaf(op, _RUN_TIME, value);
	}
	
	private static NonFunctionalCst _leaf(String op, int nfp, int value) {
		Op _op = Op.NA;
		switch(op) {
		case "<=":
			_op = Op.LEQ;
			_op.str = "<=";
			break;
		case ">=":
			_op = Op.GEQ;
			_op.str = ">=";
			break;
		case "<":
			_op = Op.L;
			_op.str = "<";
			break;
		case ">":
			_op = Op.G;
			_op.str = ">";
			break;
		case "=":
			_op = Op.EQ;
			_op.str = "==";
			break;
		default:
			assert(false);
		}
		
		return new NonFunctionalCst(Type.LEAF, nfp, _op, value);
	}
	
	public Type type;
	public int nfp;
	public Op op;
	public int value;
	
	public List<NonFunctionalCst> nodes;
}
