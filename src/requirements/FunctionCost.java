package requirements;

import java.util.HashMap;
import java.util.Map;

public class FunctionCost {

	public final static int _NONE = 0x0;
	public final static int _QUALITY_LOSS = 0x1;
	public final static int _COST = 0x2;
	public final static int _RUN_TIME = 0x4;
	
	public FunctionCost(){
		costs = new HashMap<>();
	}
	
	public FunctionCost QUALITY(int value) {
		assert(!costs.containsKey(_QUALITY_LOSS));
		costs.put(_QUALITY_LOSS, value);
		return this;
	}
	
	public FunctionCost COST(int value) {
		assert(!costs.containsKey(_COST));
		costs.put(_COST, value);
		return this;
	}
	
	public  FunctionCost RUN_TIME(int value) {
		assert(!costs.containsKey(_RUN_TIME));
		costs.put(_RUN_TIME, value);
		return this;
	}
	
	public int get(int nfcp) {
		assert(costs.containsKey(nfcp));
		return costs.get(nfcp);
	}
	
	Map<Integer, Integer> costs;
}
