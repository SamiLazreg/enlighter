package designspace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.Alternatives;
import application.Application;
import application.DataSource;
import application.Element;
import application.Path;
import application.Task;
import application.Visitor;
import deployment.DataSourceMapping;
import deployment.Mapping;
import deployment.PathMapping;
import deployment.PathMapping.Choice;
import designspace.VariabilityTree.VariabilityNode.Type;
import designspace.VariabilityTree.VariabilityNode.Variability;
import deployment.TaskMapping;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Resource;
import platform.Storage;
import platform.Storage.Capacity;

public class VariabilityTree {
	
	public enum Domain {
		DS,
		APP,
		PLT,
		MAP
	}
	
	public static class VariabilityNode {
		
		public enum Type{
			XOR,
			GROUP,
			LEAF;
		}
		
		public enum Variability{
			OPT,
			MANDATORY,
			CHOICE;
		}
		
		public enum Semantic{
			BOOL,
			INT;
		}
		
		public VariabilityNode(String id, VariabilityNode parent, Type type, Variability variability, Semantic sem, int value, Domain domain) {
			this.id = id;
			this.parent = parent;
			this.type = type;
			this.variability = variability;
			this.sem = sem;
			this.value = value;
			this.domain = domain;
			
			children = new ArrayList<VariabilityNode>();
			prices = new HashMap<String, Integer>();
		}
		
		public VariabilityNode(String id, VariabilityNode parent, Type type, Variability variability, Domain domain) {
			this(id, parent, type, variability, Semantic.BOOL, 0, domain);
		}
		
		public VariabilityNode createNode(String id, Type type, Variability variability, Domain domain) {
			VariabilityNode res = new VariabilityNode(id, this, type, variability, domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id, Type type, Variability variability) {
			VariabilityNode res = new VariabilityNode(id, this, type, variability, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id, Type type, Variability variability, int value) {
			VariabilityNode res = new VariabilityNode(id, this, type, variability, Semantic.INT, value, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id, Type type) {
			VariabilityNode res = new VariabilityNode(id, this, type, (this.type == Type.XOR)? Variability.CHOICE: Variability.MANDATORY, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id, Type type, int value) {
			VariabilityNode res = new VariabilityNode(id, this, type, (this.type == Type.XOR)? Variability.CHOICE: Variability.MANDATORY, Semantic.INT, value, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id) {
			VariabilityNode res = new VariabilityNode(id, this, Type.LEAF, (this.type == Type.XOR)? Variability.CHOICE: Variability.MANDATORY, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode createNode(String id, int value) {
			VariabilityNode res = new VariabilityNode(id, this, Type.LEAF, (this.type == Type.XOR)? Variability.CHOICE: Variability.MANDATORY, Semantic.INT, value, this.domain);
			children.add(res);
			return res;
		}
		
		public VariabilityNode get(String id) {
			
			VariabilityNode res = null;
			if(this.id.equals(id))
					return this;
			
			for(VariabilityNode c : children) {
				if((res = c.get(id)) != null)
					return res;
			}
			
			return null;
		}
		
		public boolean isLast() {
			if(parent==null)
				return true;
			return !parent.children.isEmpty() && this == ((ArrayList<VariabilityNode>)parent.children).get(parent.children.size()-1);
		}
		
		public String id;
		public Type type;
		public Semantic sem;
		public Variability variability;
		public VariabilityNode parent;
		public Collection<VariabilityNode> children;
		int value;
		public Map<String, Integer> prices;
		public Domain domain;
	}
	
	public static class ConstraintNode {
		
		public enum Type {
			AND,
			OR,
			LEAF,
			NOT,
			REQ,
			IFF,
		}
		
		public static ConstraintNode AND(ConstraintNode... constraintNodes) { return new ConstraintNode(Type.AND, constraintNodes); }
		public static ConstraintNode OR(ConstraintNode... constraintNodes) { return new ConstraintNode(Type.OR, constraintNodes); }
		public static ConstraintNode NOT(ConstraintNode... constraintNodes) { return new ConstraintNode(Type.NOT, constraintNodes); }
		public static ConstraintNode REQ(ConstraintNode... constraintNodes) { return new ConstraintNode(Type.REQ, constraintNodes); }
		public static ConstraintNode IFF(ConstraintNode... constraintNodes) { return new ConstraintNode(Type.IFF, constraintNodes); }
		
		public static List<ConstraintNode> convertToSPLOT_CNF(List<ConstraintNode> nodes) {
			List<ConstraintNode> res = new ArrayList<ConstraintNode>();
			for(ConstraintNode node : nodes) {
				switch(node.type) {
				case AND:
					/*if(node.parent == null) {
						ConstraintNode n = ConstraintNode.OR();
						for(ConstraintNode c : node.nodes) {
							assert(c.type == Type.LEAF || (c.type == Type.NOT && c.nodes.size() == 1 && c.nodes.get(1).type == Type.LEAF));
							if(c.type == Type.LEAF)
								n.addConstraint(ConstraintNode.NOT(c));
							else
								n.addConstraint(n.nodes.get(0));
						}
						res.add(n);
					}*/
					break;
				case IFF:
					if(node.nodes.get(1).type==Type.AND) {
						for(ConstraintNode n : node.nodes.get(1).nodes) {
							res.add(ConstraintNode.OR(ConstraintNode.NOT(node.nodes.get(0)), n));
							res.add(ConstraintNode.OR(node.nodes.get(0), ConstraintNode.NOT(n)));
						}
					}else {
						res.add(ConstraintNode.OR(ConstraintNode.NOT(node.nodes.get(0)), node.nodes.get(1)));
						res.add(ConstraintNode.OR(node.nodes.get(0), ConstraintNode.NOT(node.nodes.get(1))));
					}
					break;
				case LEAF:
				case NOT:
					assert(node.nodes.size() == 1);
					if(node.nodes.get(0).type==Type.AND) {
						ConstraintNode c = ConstraintNode.OR();
						for(ConstraintNode n : node.nodes.get(0).nodes) {
							c.addConstraint(ConstraintNode.NOT(n));
						}
						res.add(c);
					}
					break;
				case OR:
					res.add(node);
					break;
				case REQ:
					if(node.nodes.get(1).type==Type.AND) {
						for(ConstraintNode n : node.nodes.get(1).nodes)
							res.add(ConstraintNode.OR(ConstraintNode.NOT(node.nodes.get(0)), n));
					} else {
						res.add(ConstraintNode.OR(ConstraintNode.NOT(node.nodes.get(0)), node.nodes.get(1)));
					}
					break;
				default:
					
					break;
				}
			}
			return res;
		}
		
		public static ConstraintNode LEAF(VariabilityNode vn) {
			return new ConstraintNode(vn);
		}
		
		public ConstraintNode(Type type, ConstraintNode... constraintNodes) {
			this.type = type;
			nodes = new ArrayList<ConstraintNode>();
			addConstraints(constraintNodes);
		}
		
		public ConstraintNode(VariabilityNode vn) {
			if(vn == null)
				assert(false);
			this.id = vn.id;
			this.type = Type.LEAF;
			this.value = vn;
		}
		
		public void addConstraints(ConstraintNode... constraintNodes) {
			for(ConstraintNode cn : constraintNodes)
				nodes.add(cn);
		}
		
		public ConstraintNode addConstraint(ConstraintNode c) {
			nodes.add(c);
			assert(c.parent == null);
			c.parent = this;
			return c;
		}
		
		public String toString() {
			String res = "";
			switch(type) {
			case AND:
				res += " AND (";
				for(ConstraintNode n : nodes)
					res+=n;
				res += ")";
				break;
			case IFF:
				assert(false);
				break;
			case LEAF:
				res += " "+id+" ";
				break;
			case NOT:
				res += "!(" + nodes.get(0) + ")";
				break;
			case OR:
				res += " OR (";
				for(ConstraintNode n : nodes)
					res+=n;
				res += ")";
				break;
			case REQ:
				assert(false);
				break;
			default:
				assert(false);
				break;
			
			}
			return res;
		}
		
		public Type type;
		public String id;
		public VariabilityNode value;
		public ConstraintNode parent;
		public List<ConstraintNode> nodes;
	}
	
	private static class TreeBuilder implements application.Visitor, platform.Visitor, deployment.Visitor {

		private DesignSpace ds;
		private VariabilityNode current;
		private boolean parent;
		
		public TreeBuilder(DesignSpace ds) {
			this.ds = ds;
		}
		
		public VariabilityNode build() {
			VariabilityNode root = new VariabilityNode("DesignSpaceVariability", null, Type.GROUP, Variability.MANDATORY, Domain.DS);
			current = root.createNode("ApplicationVariability", Type.GROUP, Variability.MANDATORY, Domain.APP);
			ds.app.accept(this);
			current = root.createNode("PlatformVariability", Type.GROUP, Variability.MANDATORY, Domain.PLT);
			ds.plt.accept(this);
			current = root.createNode("DeploymentVariability", Type.GROUP, Variability.MANDATORY, Domain.MAP);
			ds.map.accept(this);
			return root;
		}

		@Override
		public void visit(Component c) {
			if(!(parent || c.isOptional() || c.isConfigurable() || !c.children.isEmpty() /*|| c.cost > 0*/))
				return;
			
			VariabilityNode n = current;
			VariabilityNode cn = current;
			
			if(parent || c.isOptional() || c.isConfigurable() /*|| c.cost > 0*/){
				cn = n.createNode(c.id, ((c.isOptional || parent) && !c.children.isEmpty()) || c.isConfigurable()?Type.GROUP:Type.LEAF, (c.isOptional?Variability.OPT:Variability.MANDATORY));
				if(c.cost != 0) cn.prices.put("cost", c.cost);
				
				if(c.isConfigurable()) {
					if(c.getClockOwner() == c) {
						VariabilityNode fn = cn.createNode(c.id+"_frequency", Type.XOR);
						for(int f : c.getFrequencies()) {
							fn.createNode(c.id+"_frequency_"+Integer.toString(f), f);
						}
					}
				}
			}
			
			for(Resource r: c.getResources()) {
				if(c.isOptional() || parent)
					parent = true;
				current = cn;
				r.accept(this);
			}
			parent = false;
		}
		
		@Override
		public void visit(Memory m) {
			if(parent || m.isOptional() /*|| m.cost > 0*/) {
				VariabilityNode mn = current.createNode(m.id, Type.LEAF, m.isOptional?Variability.OPT:Variability.MANDATORY);
				if(m.cost != 0) mn.prices.put("cost", m.cost);
			}
		}

		@Override
		public void visit(Processor p) {
			if(parent || p.isOptional() /*|| p.cost > 0*/) {
				VariabilityNode pn = current.createNode(p.id, Type.LEAF, p.isOptional?Variability.OPT:Variability.MANDATORY);
				if(p.cost != 0) pn.prices.put("cost", p.cost);
			}
		}

		@Override
		public void visit(Storage s) {
			if(!(parent || s.isOptional() || s.isConfigurable() /*|| s.cost > 0)*/))
				return;
			
			VariabilityNode n = current.createNode(s.id, (s.isConfigurable()?Type.GROUP:Type.LEAF), (s.isOptional?Variability.OPT:Variability.MANDATORY));
			if(s.cost != 0) n.prices.put("cost", s.cost);
			
			if(s.capacities.size()>1) {
				VariabilityNode c_xor = n.createNode(s.id+"_capacity", Type.XOR);
				for(Capacity c : s.capacities) {
					VariabilityNode cn = c_xor.createNode(s.id+"_capacity_"+Integer.toString(c.value), c.value);
					if(c.cost != 0) cn.prices.put("cost", c.cost);
				}
			}
			
			if(s.getFrequencies().size()>1) {
				VariabilityNode f_xor = n.createNode(s.id+"_frequency", Type.XOR);
				for(int f : s.getFrequencies()) {
					f_xor.createNode(s.id+"_frequency_"+f, f);
				}
			}
		}

		@Override
		public void visit(DataSource d) {
			VariabilityNode	n = current;
			if(d.isOptional /*|| d.qualityLoss > 0*/)
					n = current.createNode(d.id, (d.isVariable()?Type.GROUP:Type.LEAF), (d.isOptional?Variability.OPT:Variability.MANDATORY));
			
			
			//assume that only PROPERTY is size
			if(d.isVariable()) {
				n = n.createNode(d.id+"_size", Type.XOR);
				for(DataSource.Size s : d.sizes) {
					VariabilityNode sn = n.createNode(d.id+"_size_"+Integer.toString(s.value), s.value);
					if(s.qualityLoss != 0) sn.prices.put("qualityLoss", s.qualityLoss);
				}
			}
		}

		@Override
		public void visit(Path p) {
			if(p.isOptional)
				current.createNode(p.id, Type.LEAF, Variability.OPT);
		}

		@Override
		public void visit(Task t) {
			if(t.isOptional /*|| t.qualityLoss > 0*/) {
				VariabilityNode tn = current.createNode(t.id, Type.LEAF, (t.isOptional?Variability.OPT:Variability.MANDATORY));
				if(t.qualityLoss != 0) tn.prices.put("qualityLoss", t.qualityLoss);
			}
		}

		@Override
		public void visit(Application a) {
			for(Element e : a.getElements())
				e.accept(this);
			
			for(Alternatives alt : a.alternatives.values()) {
				alt.accept(this);
			}
		}

		@Override
		public void visit(Alternatives alt) {
			if(!alt.splits.isEmpty()) {
				VariabilityNode choice = current.createNode(alt.path+"_To", Type.XOR, (alt.path.isOptional?Variability.OPT:Variability.MANDATORY));
				for(Map.Entry<Element, Collection<Element>> entry : alt.splits.entrySet())
					choice.createNode(alt.path.id+"_To_"+entry.getKey().id);
			}
			if(!alt.joins.isEmpty()) {
				VariabilityNode choice = current.createNode(alt.path+"_From", Type.XOR, (alt.path.isOptional?Variability.OPT:Variability.MANDATORY));
				for(Map.Entry<Element, Collection<Element>> entry : alt.joins.entrySet())
					choice.createNode(alt.path.id+"_From_"+entry.getKey().id);
			}
		}

		@Override
		public void visit(Mapping m) {
			for(DataSourceMapping dm : m.dms.values())
				dm.accept(this);
			for(PathMapping pm : m.pms.values())
				pm.accept(this);
			for(TaskMapping tm : m.tms.values())
				tm.accept(this);
		}

		//TODO all mapping can be generalized or abstracted
		@Override
		public void visit(DataSourceMapping dm) {
			VariabilityNode n = current.createNode(dm.id, Type.XOR, (dm.dataSource.isOptional?Variability.OPT:Variability.MANDATORY));
			for(DataSourceMapping.Choice c : dm.choices.values())
				n.createNode(c.id);
		}

		@Override
		public void visit(PathMapping pm) {
			VariabilityNode n = current.createNode(pm.id, Type.XOR, (pm.path.isOptional?Variability.OPT:Variability.MANDATORY));
			for(PathMapping.Choice c : pm.choices.values()) {
				n.createNode(c.id);
			}
		}

		@Override
		public void visit(TaskMapping tm) {
			VariabilityNode n = current.createNode(tm.id, Type.XOR, (tm.task.isOptional?Variability.OPT:Variability.MANDATORY));
			for(TaskMapping.Choice c : tm.choices.values()) {
				n.createNode(c.id);
			}
		}
		
	}
	
	private static class ConstraintsBuilder implements application.Visitor, platform.Visitor, deployment.Visitor {

		private DesignSpace ds;
		private VariabilityNode root;
		private Map<String, List<ConstraintNode>> constraints;
		
		public ConstraintsBuilder(DesignSpace ds, VariabilityNode root) {
			this.ds = ds;
			this.root = root;
			constraints = new HashMap<>();
			constraints.put("Flow", new ArrayList<>());
			constraints.put("Application Deployment", new ArrayList<>());
			constraints.put("Platform Deployment", new ArrayList<>());
			constraints.put("Resources", new ArrayList<>());
			constraints.put("NodeMapping", new ArrayList<>());
			constraints.put("PathMapping", new ArrayList<>());
		}
		
		public Map<String, List<ConstraintNode>> build() {
			ds.app.accept(this);
			ds.plt.accept(this);
			ds.map.accept(this);
			return constraints;
		}
		
		@Override
		public void visit(Component c) {
			_visit(c);
			for(Resource r : c.children)
				r.accept(this);
		}

		private void _visit(Resource r) {
			if(!r.requires.isEmpty() || !r.excludes.isEmpty()) {
				
				ConstraintNode and = ConstraintNode.AND();
				for(Resource req : r.requires)
					and.addConstraint(ConstraintNode.LEAF(root.get(req.id)));
				if(!and.nodes.isEmpty()) constraints.get("Resources").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(r.id)), and));
				
				and = ConstraintNode.AND();
				for(Resource exc : r.excludes)
					and.addConstraint(ConstraintNode.NOT(ConstraintNode.LEAF(root.get(exc.id))));
				if(!and.nodes.isEmpty()) constraints.get("Resources").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(r.id)), and));
				
			}
		}
		
		@Override
		public void visit(Memory m) {
			_visit(m);
		}

		@Override
		public void visit(Processor p) {
			_visit(p);
		}

		@Override
		public void visit(Storage s) {
			_visit(s);
		}

		@Override
		public void visit(DataSource d) {
		}

		@Override
		public void visit(Path p) {
		}

		@Override
		public void visit(Task t) {
		}

		@Override
		public void visit(Application a) {
			for(Element e : a.getElements())
				e.accept(this);
			
			for(Alternatives alt : a.alternatives.values()) {
				alt.accept(this);
			}
		}

		@Override
		public void visit(Alternatives alt) {
			if(!alt.splits.isEmpty()) {
				for(Map.Entry<Element, Collection<Element>> entry : alt.splits.entrySet()) {
					ConstraintNode and = ConstraintNode.AND();
					for(Element e : entry.getValue())
						and.addConstraint(ConstraintNode.LEAF(root.get(e.id)));
					constraints.get("Flow").add(ConstraintNode.IFF(ConstraintNode.LEAF(root.get(alt.path.id+"_To_"+entry.getKey())), and));
				}
			}
			if(!alt.joins.isEmpty()) {
				for(Map.Entry<Element, Collection<Element>> entry : alt.joins.entrySet()) {
					ConstraintNode and = ConstraintNode.AND();
					for(Element e : entry.getValue())
						and.addConstraint(ConstraintNode.LEAF(root.get(e.id)));
					constraints.get("Flow").add(ConstraintNode.IFF(ConstraintNode.LEAF(root.get(alt.path.id+"_From_"+entry.getKey())), and));
				}
			}
		}

		@Override
		public void visit(Mapping m) {
			for(DataSourceMapping dm : m.dms.values())
				dm.accept(this);
			for(PathMapping pm : m.pms.values())
				pm.accept(this);
			for(TaskMapping tm : m.tms.values())
				tm.accept(this);
		}

		//TODO all mapping can be generalized or abstracted
		@Override
		public void visit(DataSourceMapping dm) {
			if(dm.dataSource.isOptional)
				constraints.get("Application Deployment").add(ConstraintNode.IFF(ConstraintNode.LEAF(root.get(dm.dataSource.id)), ConstraintNode.LEAF(root.get(dm.id))));
			for(DataSourceMapping.Choice c : dm.choices.values()) {
				if(c.storage.isOptional())
					constraints.get("Platform Deployment").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), ConstraintNode.LEAF(root.get(c.storage.id))));
				ConstraintNode and = ConstraintNode.AND();
				for(Collection<PathMapping.Choice> pmcs : c.getRelatedPathMappings().values()) {
					ConstraintNode or = ConstraintNode.OR();
					for(PathMapping.Choice pmc: pmcs)
						or.addConstraint(ConstraintNode.LEAF(root.get(pmc.id)));
					if(!or.nodes.isEmpty()) and.addConstraint(or);
				}
				if(!and.nodes.isEmpty()) constraints.get("NodeMapping").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), and));
			}
		}

		@Override
		public void visit(PathMapping pm) {
			if(pm.path.isOptional)
				constraints.get("Application Deployment").add(ConstraintNode.IFF(ConstraintNode.LEAF(root.get(pm.path.id)), ConstraintNode.LEAF(root.get(pm.id))));
			for(PathMapping.Choice c : pm.choices.values()) {
				if(c.memory.isOptional())
					constraints.get("Platform Deployment").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), ConstraintNode.LEAF(root.get(c.memory.id))));
				ConstraintNode and = ConstraintNode.AND();
				ConstraintNode or = ConstraintNode.OR();
				
				for(DataSourceMapping.Choice dmc: c.flashers)
					or.addConstraint(ConstraintNode.LEAF(root.get(dmc.id)));
				for(TaskMapping.Choice pmc: c.producers)
					or.addConstraint(ConstraintNode.LEAF(root.get(pmc.id)));
				if(!or.nodes.isEmpty()) and.addConstraint(or);
				
				or = ConstraintNode.OR();
				for(TaskMapping.Choice pmc: c.consumers)
					or.addConstraint(ConstraintNode.LEAF(root.get(pmc.id)));
				
				if(!or.nodes.isEmpty()) and.addConstraint(or);
				if(!and.nodes.isEmpty()) constraints.get("PathMapping").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), and));
			}
		}

		@Override
		public void visit(TaskMapping tm) {
			if(tm.task.isOptional)
				constraints.get("Application Deployment").add(ConstraintNode.IFF(ConstraintNode.LEAF(root.get(tm.task.id)), ConstraintNode.LEAF(root.get(tm.id))));
			for(TaskMapping.Choice c : tm.choices.values()) {
				if(c.processor.isOptional())
					constraints.get("Platform Deployment").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), ConstraintNode.LEAF(root.get(c.processor.id))));
				ConstraintNode and = ConstraintNode.AND();
				for(Collection<PathMapping.Choice> pmcs : c.getRelatedPathMappings().values()) {
					ConstraintNode or = ConstraintNode.OR();
					for(PathMapping.Choice pmc: pmcs)
						or.addConstraint(ConstraintNode.LEAF(root.get(pmc.id)));
					if(!or.nodes.isEmpty()) and.addConstraint(or);
				}
				if(!and.nodes.isEmpty()) constraints.get("NodeMapping").add(ConstraintNode.REQ(ConstraintNode.LEAF(root.get(c.id)), and));
			}
		}
		
	}
	
	public VariabilityTree(DesignSpace ds) {
		root = new TreeBuilder(ds).build();
		constraints = new ConstraintsBuilder(ds, root).build();
		prunings = new ArrayList<ConstraintNode>();
	}
	
	/*public void removeDesigns(Collection<String> designList) {
		for(String dl : designList) {
			String[] designChoices = dl.split(" ");
			ConstraintNode c = ConstraintNode.AND();
			for(String dc : designChoices) {
				if(dc.startsWith("!"))
					continue;
				c.addConstraint(ConstraintNode.LEAF(root.get(dc)));
			}
			prunings.add(c);
		}
	}*/
	
	public void removeDesigns(Collection<String> designList) {
		for(String dl : designList) {
			String[] designChoices = dl.split(" ");
			ConstraintNode c = ConstraintNode.NOT();
			prunings.add(c);
			c = c.addConstraint(ConstraintNode.AND());
			for(String dc : designChoices) {
				if(dc.startsWith("!"))
					continue;
				c.addConstraint(ConstraintNode.LEAF(root.get(dc)));
			}
			
		}
	}
	
	public VariabilityNode root;
	public Map<String, List<ConstraintNode>> constraints;
	public List<ConstraintNode> prunings;
}
