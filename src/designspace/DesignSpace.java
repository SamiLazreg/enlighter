package designspace;

import application.Application;
import deployment.Mapping;
import platform.Component;

public class DesignSpace {

	public DesignSpace(Application app, Mapping map, Component plt) {
		this.app = app;
		this.map = map;
		this.plt = plt;
		tree = new VariabilityTree(this);
		nbDesigns = 0;
	}
	
	public Application app;
	public Mapping map;
	public Component plt;
	public VariabilityTree tree;
	public double nbDesigns;
}
