package deployment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import application.DataSource;
import application.Application;
import application.Node;
import application.Path;
import application.Task;
import deployment.PathMapping.Choice;
import platform.Component;
import platform.Memory;
import platform.Processor;
import platform.Storage;
import platform.Processor.Port;

public class MappingAlgorithm {

	public MappingAlgorithm() {
		tms = new HashMap<Task, TaskMapping>();
		pms = new HashMap<Path, PathMapping>();
		dms = new HashMap<DataSource, DataSourceMapping>();
	}
	
	public Mapping map(Application g, Component c) {
		//beginning with input data that will feed all tasks
		for(DataSource ds : g.getDataSources()) {
			addDataPathMappings(ds, c.getStorages());
		}
	
		
		/*then for each task, i)add tm(t) ii)ref availlable intput of tm(t) pm(t_{i0,n}), 
		 * iii)produce pm(t_{o0,m}*/ 
		for(Task t : g.getSortedTasks()) {
			addTaskMappings(t, getProcessors(c, t));
			
			refInputsDataPathMappings(t);
			addOutputsDataPathMappings(t);
		}
		
		//ITSME
		/*for(PathMapping pm : pms.values()) {
			for(PathMapping.Choice choice : pm.choices.values())
				System.out.println(choice);
		}
		
		//ITSME
		for(TaskMapping tm : tms.values()) {
			for(TaskMapping.Choice choice : tm.choices.values()) {
				System.out.println(choice);
			}
		}*/
		
		boolean pmcs = removeUselessPathMappingChoices();
		boolean tmcs = removeUselessTaskMappingChoices();
		do {
			pmcs = removeUselessPathMappingChoices();
			tmcs = removeUselessTaskMappingChoices();
			System.out.println("pruned\n");
		}while(tmcs || pmcs);
		
		addMappingChoiceDependenciesAndResponsibilities();
		return new Mapping(dms, tms, pms);
	}
	
	private void addMappingChoiceDependenciesAndResponsibilities() {
		
		for(PathMapping pm : pms.values()) {
			for(PathMapping.Choice pmc : pm.choices.values()) {
				pmc.consumers.clear();
				pmc.flashers.clear();
				pmc.producers.clear();
			}
		}
		
		for(DataSourceMapping dm : dms.values()) {
			for(DataSourceMapping.Choice dmc : dm.choices.values()) {
				
				dmc.resps.clear();
				
				for(Node.Argument out : dmc.dataSource.outputs.values()){
					//add pm of o.p if absent
					Path path = out.path;
					assert(pms.containsKey(path));
					//for each flachable memory add dmc and pmc accordingly
					PathMapping pm = pms.get(path);
					//add path mapping choices through dependency mapping 
					PathMapping.Choice pmc = pm.addDependencyToFlasher(dmc, dmc.storage);
					//add resp of pmc to the related dsc
					dmc.addResponsibility(out.id, pmc);
				}
			}
		}
		
		for(TaskMapping tm : tms.values()) {
			for(TaskMapping.Choice tmc : tm.choices.values()) {
				
				tmc.deps.clear();
				tmc.resps.clear();
				
				for(Node.Argument in : tmc.task.inputs.values()) {
					PathMapping pm = pms.get(in.path);
					//ref pm(I(t)) inter I(p) tmc(t,p)in tm(t)
					@SuppressWarnings("unchecked")
					Collection<Memory> inter = (Collection<Memory>) MathUtilities.intersection(pm.getMemories(), tmc.processor.inputs.get(in.id).memories);
					assert(inter != null);
					Collection<PathMapping.Choice> pmcs = pm.addDependenciesToConsumer(tmc, inter);
					tmc.addDependencies(in.id, pmcs);
				}
				for(Node.Argument out : tmc.task.outputs.values()) {
					PathMapping pm = pms.get(out.path);
					//ref pm(I(t)) inter I(p) tmc(t,p)in tm(t)
					@SuppressWarnings("unchecked")
					Collection<Memory> inter = (Collection<Memory>) MathUtilities.intersection(pm.getMemories(), tmc.processor.outputs.get(out.id).memories);
					if(inter != null);
						//assert(false);
					Collection<PathMapping.Choice> pmcs = pm.addDependenciesToProducer(tmc, inter);
					tmc.addResponsibilities(out.id, pmcs);
				}
			}
		}
	}
	
	private void addDataPathMappings(DataSource ds, Collection<Memory> ms) {
		//add dm
		dms.put(ds, new DataSourceMapping(ds));
		//for each o add pm
		for(Node.Argument out : ds.outputs.values()){
			//add pm of o.p if absent
			Path path = out.path;
			if(!pms.containsKey(path))
				pms.put(path, new PathMapping(path));
			//for each flachable memory add dmc and pmc accordingly
			PathMapping pm = pms.get(path);
			for(Memory m : ms) {
				//add dmc
				DataSourceMapping.Choice dsc = new DataSourceMapping.Choice(ds, m);
				dms.get(ds).choices.put(m, dsc);
				//add path mapping choices through dependency mapping 
				PathMapping.Choice pmc = pm.addMappingChoice(dsc, m);
				pm.addDependencyToFlasher(dsc, m);
				//add resp of pmc to the related dsc
				dsc.addResponsibility(out.id, pmc);
			}
		}
	}
	
	private void refInputsDataPathMappings(Task t){
		//problem: if missing input path mappings
		for(Map.Entry<String, Node.Argument> in : t.inputs.entrySet()) {
			System.out.println(in.getKey()+" : "+in.getValue().path);
			if(!pms.containsKey(in.getValue().path))
				assert(false);
		}
		//for each tmc of t
		TaskMapping tm = tms.get(t);
		Collection<Processor> toDel = new ArrayList<>();
		for(TaskMapping.Choice c : tm.choices.values()) {
			//for each in I(t)
			for(Node.Argument in : t.inputs.values()){
				//get pm(I(t))
				PathMapping pm = pms.get(in.path);
				
				//ref pm(I(t)) inter I(p) tmc(t,p)in tm(t)
				
				
				if(pm == null || pm.getMemories() == null || c.processor.inputs.get(in.id) == null || c.processor.inputs.get(in.id).memories == null)
					assert(false);
				
				Collection<Memory> pm_m = pm.getMemories();
				Collection<Memory> p_ins = c.processor.inputs.get(in.id).memories;
						
				
				@SuppressWarnings("unchecked")
				Collection<Memory> inter = (Collection<Memory>) MathUtilities.intersection(pm.getMemories(), c.processor.inputs.get(in.id).memories);
				if(inter!=null) {
					pm.addMappingChoices(c, inter);
					Collection<PathMapping.Choice> pmcs = pm.addDependenciesToConsumer(c, inter);
					c.addDependencies(in.id, pmcs);
				}
				//if inter is empty then remove tmc 
				//i.e remove the proc that cannot read and write accordingly
				else
					toDel.add(c.processor);	
			}
		}
		for(Processor p : toDel)
			tm.choices.remove(p);	
	}
	
	private void addOutputsDataPathMappings(Task t){
		
		for(Map.Entry<String, Node.Argument> out : t.outputs.entrySet()){
			Path path = out.getValue().path;
			if(!pms.containsKey(path))
				pms.put(path, new PathMapping(path));
		}
		
		TaskMapping tm = tms.get(t);
		for(TaskMapping.Choice c : tm.choices.values()) {
			for(Node.Argument out : t.outputs.values()){
				
				Path path = out.path;
				PathMapping pm = pms.get(path);
				
				Collection<PathMapping.Choice> pmcs = pm.addMappingChoices(c, c.processor.outputs.get(out.id).memories);
				pm.addDependenciesToProducer(c, c.processor.outputs.get(out.id).memories);
				c.addResponsibilities(out.id, pmcs);
			}
		}
	}

	private Collection<Processor> getProcessors(Component c, Task t) {
		Collection<Processor> res = new ArrayList<Processor>();
		for(Processor p : c.getProcessors()) {
			if(p.function.equals(t.function)) {
				boolean param = true;
				for(Map.Entry<String, Processor.Port> ins : p.inputs.entrySet())
					if(!t.inputs.containsKey(ins.getKey())) {
						param = false;
						break;
					}
				
				if(!param)
					continue;
				
				for(Map.Entry<String, Processor.Port> outs : p.outputs.entrySet())
					if(!t.outputs.containsKey(outs.getKey())) {
						param = false;
						break;
					}
				
				if(!param)
					continue;
				
				res.add(p);
			}
		}
		return res;
	}
	
	private void addTaskMappings(Task t, Collection<Processor> ps) {
		if(!tms.containsKey(t))
			tms.put(t, new TaskMapping(t));
		for(Processor p : ps)
			tms.get(t).choices.put(p, new TaskMapping.Choice(t,p));
	}
	
	private boolean removeUselessPathMappingChoices() {
		boolean res = false;
		for(PathMapping pm : pms.values()) {
				Collection<PathMapping.Choice> tmp = new ArrayList<PathMapping.Choice>(pm.choices.values());
				for(PathMapping.Choice c : tmp)
					if((c.producers.isEmpty() && c.flashers.isEmpty()) || c.consumers.isEmpty()) {
						pm.choices.remove(c.memory);
						assert(!pm.choices.isEmpty());
						res = true;
					}
		}
		return res;
	}
	
	private boolean removeUselessTaskMappingChoices() {
		boolean res = false;
		for(TaskMapping tm : tms.values()) {
			Collection<TaskMapping.Choice> tmp = new ArrayList<TaskMapping.Choice>(tm.choices.values());
			for(TaskMapping.Choice c : tmp) {
				boolean del = false;
				for(Port in : c.processor.inputs.values())
					if(MathUtilities.intersection(in.memories, pms.get(tm.task.inputs.get(in.id).path).getMemories()) == null) {
						del = true;
						break;
					}
				if(!del)
					for(Port out : c.processor.outputs.values())
						if(MathUtilities.intersection(out.memories, pms.get(tm.task.outputs.get(out.id).path).getMemories()) == null) {
							del = true;
							break;
						}
				
				if(del) {
					c.remove();
					tm.choices.remove(c.processor);
					res = true;
					//assert(!tm.choices.isEmpty());
				}
			}
		}
		return res;
	}
	
	Map<Task, TaskMapping> tms;
	Map<Path, PathMapping> pms;
	Map<DataSource, DataSourceMapping> dms;
}
