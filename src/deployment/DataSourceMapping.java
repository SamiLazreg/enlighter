package deployment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import application.DataSource;
import application.Task;
import deployment.TaskMapping.Choice;
import designspace.VariabilityTree.VariabilityNode.Variability;
import platform.Memory;
import platform.Processor;
import platform.Storage;

public class DataSourceMapping {

	public static class Choice {
		
		public Choice(DataSource ds, Memory m) {
			this.id = ds.id+"_On_"+m.id;
			this.dataSource = ds;
			this.storage = m;
			resps = new HashMap<String, Collection<PathMapping.Choice>>();
		}
		
		public String toString() {
			String res = dataSource+" on "+storage;
			for(Entry<String, Collection<PathMapping.Choice>> r : resps.entrySet()) {
				res+="\n "+r.getKey()+" responsible for: ";
				for(PathMapping.Choice c: r.getValue())
					res+="\n\t"+c;
			}
			return res;
		}
		
		public void addResponsibility(String port, PathMapping.Choice c) {
			if(!resps.containsKey(port))
				resps.put(port, new ArrayList<PathMapping.Choice>());
			resps.get(port).add(c);
			
		}
		
		public Map<String, Collection<PathMapping.Choice>> getRelatedPathMappings() {
			return resps;
		}
		
		public void remove() {
			for(Collection<PathMapping.Choice> cs : resps.values()) {
				for(PathMapping.Choice c : cs) {
					c.flashers.remove(this);
				}
			}
		}
		
		public String id;
		public DataSource dataSource;
		public Memory storage;
		public Map<String, Collection<PathMapping.Choice>> resps;
	}
	
	public DataSourceMapping(DataSource dataSource) {
		this.id = dataSource+"_On";
		this.dataSource = dataSource;
		choices = new HashMap<Memory, Choice>();
	}
	
	public Collection<Memory> getStorage() {
		return choices.keySet();
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String id;
	public DataSource dataSource;
	public Map<Memory, Choice> choices;
}
