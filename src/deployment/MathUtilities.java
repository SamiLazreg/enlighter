package deployment;

import java.util.Collection;
import java.util.HashSet;

public class MathUtilities {

	public static Collection<?> intersection(Collection<?> a, Collection<?> b){
		Collection<Object> res = new HashSet<Object>(a);
		res.retainAll(b);
		if(!res.isEmpty())
			return res;
		return null;
	}
	
	public static Collection<?> difference(Collection<?> a, Collection<?> b){
		Collection<Object> res = new HashSet<Object>(a);
		res.removeAll(b);
		if(!res.isEmpty())
			return res;
		return null;
	}
}
