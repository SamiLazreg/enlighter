package deployment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import application.Task;
import deployment.PathMapping.Choice;
import platform.Memory;
import platform.Processor;
import platform.Processor.Port;

public class TaskMapping {

	public static class Choice {
		
		public Choice(Task t, Processor p) {
			this.id = t.id+"_On_"+p.id;
			this.task = t;
			this.processor = p;
			deps = new HashMap<String, Collection<PathMapping.Choice>>();
			resps = new HashMap<String, Collection<PathMapping.Choice>>();
		}
		
		public String toString() {
			String res = task+" on "+processor;
			
			for(Entry<String, Collection<PathMapping.Choice>> d : deps.entrySet()) {
				res+="\n\t\t "+d.getKey()+" depends on: ";
				for(PathMapping.Choice c : d.getValue())
					res+="\n\t\t\t"+c.path+" on "+c.memory+" or ";
			}
			for(Entry<String, Collection<PathMapping.Choice>> r : resps.entrySet()) {
				res+="\n\t\t "+r.getKey()+" responsible for: ";
				for(PathMapping.Choice c: r.getValue())
					res+="\n\t\t\t"+c.path+" on "+c.memory+" or ";
			}
			return res;
		}
		
		public Map<String, Collection<PathMapping.Choice>> getRelatedPathMappings(){
			Map<String, Collection<PathMapping.Choice>> res = new HashMap<String, Collection<PathMapping.Choice>>();
			res.putAll(deps);
			res.putAll(resps);
			return res;
		}
		
		public void addDependencies(String port, Collection<PathMapping.Choice> cs) {
			for(PathMapping.Choice c : cs)
				addDependency(port, c);
		}
		
		public void addDependency(String port, PathMapping.Choice c) {
			if(!deps.containsKey(port))
				deps.put(port, new ArrayList<PathMapping.Choice>());
			deps.get(port).add(c);
		}
		
		public void addResponsibilities(String port, Collection<PathMapping.Choice> cs) {
			for(PathMapping.Choice c : cs)
				addResponsibility(port, c);
		}
		
		public void addResponsibility(String port, PathMapping.Choice c) {
			if(!resps.containsKey(port))
				resps.put(port, new ArrayList<PathMapping.Choice>());
			resps.get(port).add(c);
		}
		
		public void remove() {
			for(Collection<PathMapping.Choice> cs : deps.values()) {
				for(PathMapping.Choice c : cs) {
					c.consumers.remove(this);
					c.producers.remove(this);
				}
			}
		}
		
		public String id;
		public Task task;
		public Processor processor;
		public Map<String, Collection<PathMapping.Choice>> deps;
		public Map<String, Collection<PathMapping.Choice>> resps;
	}
	
	public TaskMapping(Task task) {
		this.id = task.id+"_On";
		this.task = task;
		choices = new HashMap<Processor, Choice>();
	}
	
	public Collection<Processor> getProcessors() {
		return choices.keySet();
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String toString() {
		String res = "Task Mapping "+task;
		for(Choice c : choices.values())
			res+="\n\t on "+c;
		return res;
	}
	
	public String id;
	public Task task;
	public Map<Processor, Choice> choices;
}
