package deployment;

public interface Visitor {
	public void visit(Mapping m);
	public void visit(DataSourceMapping dm);
	public void visit(PathMapping pm);
	public void visit(TaskMapping tm);
}
