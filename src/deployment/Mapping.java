package deployment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import application.DataSource;
import application.Element;
import application.Path;
import application.Task;
import platform.Component;
import platform.Resource;

public class Mapping {

	public Mapping(Map<DataSource, DataSourceMapping> dms, Map<Task, TaskMapping> tms, Map<Path, PathMapping> pms) {
		this.dms = dms;
		this.tms = tms;
		this.pms = pms;
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	/*public Map<? extends Element, Collection<? extends Resource>> getElementsOnResources() {
		Map<Element, Collection<Resource>> res = new HashMap<Element, Collection<Resource>>();
		for(DataSourceMapping dm : dms.values())
			res.put(dm.dataSource, dm.getStorage());
		
	}*/
	
	public Map<DataSource, DataSourceMapping> dms;
	public Map<Task, TaskMapping> tms;
	public Map<Path, PathMapping> pms;
}
