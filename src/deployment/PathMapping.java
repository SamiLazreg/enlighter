package deployment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import application.DataSource;
import application.Path;
import platform.Memory;

public class PathMapping {
	
	public static class Choice {
		
		public Choice(Path p, Memory m) {
			this.id = p.id+"_On_"+m.id;
			this.path = p;
			this.memory = m;
			consumers = new ArrayList<TaskMapping.Choice>();
			producers = new ArrayList<TaskMapping.Choice>();
			flashers = new ArrayList<DataSourceMapping.Choice>();
		}
		
		public String toString() {
			String res = path+" on "+memory;
			
			res+="\n\t\t produced by: ";
			for(TaskMapping.Choice c: producers)
				res+="\n\t\t\t"+c.task+" on "+c.processor+" and";
			
			res+="\n\t\t flashed by: ";
			for(DataSourceMapping.Choice c: flashers)
				res+="\n\t\t\t"+c.dataSource+" on "+c.storage+" and";
			
			res+="\n\t\t consumed by: ";
			for(TaskMapping.Choice c : consumers)
				res+="\n\t\t\t"+c.task+" on "+c.processor+" and";
		
			return res;
		}
		
		public String id;
		public Path path;
		public Memory memory;
		public Collection<TaskMapping.Choice> consumers;
		public Collection<TaskMapping.Choice> producers;
		public Collection<DataSourceMapping.Choice> flashers;
	}
	
	public PathMapping(Path path) {
		this.id = path.id+"_On";
		this.path = path;
		choices = new HashMap<Memory, Choice>();
	}
	
	public Collection<Memory> getMemories() {
		return choices.keySet();
	}
	
	public Collection<PathMapping.Choice> addMappingChoices(TaskMapping.Choice c, Collection<Memory> ms) {
		Collection<PathMapping.Choice> res = new HashSet<PathMapping.Choice>();
		for(Memory m : ms)
			res.add(addMappingChoice(c, m));
		return res;
	}
	
	public PathMapping.Choice addMappingChoice(TaskMapping.Choice c, Memory m) {
		if(!choices.containsKey(m))
			choices.put(m, new Choice(path, m));
		return choices.get(m);
	}
	
	public Collection<PathMapping.Choice> addMappingChoices(DataSourceMapping.Choice c, Collection<Memory> ms) {
		Collection<PathMapping.Choice> res = new HashSet<PathMapping.Choice>();
		for(Memory m : ms)
			res.add(addMappingChoice(c, m));
		return res;
	}
	
	public PathMapping.Choice addMappingChoice(DataSourceMapping.Choice c, Memory m) {
		if(!choices.containsKey(m))
			choices.put(m, new Choice(path, m));
		return choices.get(m);
	}
	
	public Collection<PathMapping.Choice> addDependenciesToProducer(TaskMapping.Choice c, Collection<Memory> ms) {
		Collection<PathMapping.Choice> res = new HashSet<PathMapping.Choice>();
		for(Memory m : ms)
			res.add(addDependencyToProducer(c, m));
		return res;
	}
	
	public PathMapping.Choice addDependencyToProducer(TaskMapping.Choice c, Memory m) {
		assert(choices.containsKey(m));
		choices.get(m).producers.add(c);
		return choices.get(m);
	}
	
	public Collection<PathMapping.Choice> addDependenciesToConsumer(TaskMapping.Choice c, Collection<Memory> ms) {
		Collection<PathMapping.Choice> res = new HashSet<PathMapping.Choice>();
		for(Memory m : ms)
			res.add(addDependencyToConsumer(c, m));
		return res;
	}
	
	public PathMapping.Choice addDependencyToConsumer(TaskMapping.Choice c, Memory m) {
		assert(choices.containsKey(m));
		choices.get(m).consumers.add(c);
		return choices.get(m);
	}
	
	public Collection<PathMapping.Choice> addDependenciesToFlasher(DataSourceMapping.Choice c, Collection<Memory> ms) {
		Collection<PathMapping.Choice> res = new HashSet<PathMapping.Choice>();
		for(Memory m : ms)
			res.add(addDependencyToFlasher(c, m));
		return res;
	}
	
	public PathMapping.Choice addDependencyToFlasher(DataSourceMapping.Choice c, Memory m) {
		assert(choices.containsKey(m));
		choices.get(m).flashers.add(c);
		return choices.get(m);
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String toString() {
		String res = "Path Mapping "+path;
		for(Choice c : choices.values())
			res+="\n\t on "+c;
		return res;
	}
	
	public String id;
	public Path path;
	public Map<Memory, Choice> choices;

}
