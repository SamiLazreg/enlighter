package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Path extends Element implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 44549278923978810L;
	public Path(String id) {
		super(id, Type.PATH);
		inputs = new ArrayList<Node>();
		outputs = new ArrayList<Node>();
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Collection<Node> inputs;
	public Collection<Node> outputs;
	
	
}
