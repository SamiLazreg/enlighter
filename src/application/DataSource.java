package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class DataSource extends Node{
	
	public static class Size implements Serializable{

		public Size(String id, int value, int qualityLoss) {
			this.id = id;
			this.value = value;
			this.qualityLoss = qualityLoss;
		}
		
		public String id;
		public int value;
		public int qualityLoss;
	}

	public DataSource(String id) {
		super(id, Node.Type.DATA_SOURCE);
		sizes = new ArrayList<Size>();
	}
	
	public DataSource addSize(String id, int value, int quality) {
		sizes.add(new Size(id, value, quality));
		return this;
	}
	
	public boolean isVariable() {
		return sizes.size() > 1;
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String toString() {
		return id;
	}
	
	public Collection<Size> sizes;
}
