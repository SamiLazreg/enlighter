package application;

public interface Visitor {

	public void visit(DataSource d);
	public void visit(Path p);
	public void visit(Task t);
	public void visit(Application a);
	public void visit(Alternatives alt);
}
