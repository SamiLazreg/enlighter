package application;

import java.io.Serializable;

import application.Visitor;

public abstract class Element implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1688709303810061039L;

	public enum Type{
		PATH,
		NODE,
		GROUP;
	}
	
	public Element(String id, Type type) {
		this.id = id;
		this.type = type;
		this.isOptional = false;
		this.qualityLoss = 0;
	}

	public Element setQualityLoss(int qualityLoss) {
		this.qualityLoss = qualityLoss;
		return this;
	}
	
	public abstract void accept(Visitor v);
	
	public String toString() {
		return id;
	}
	
	public String id;
	public Type type;
	public boolean isOptional;
	public int qualityLoss;
	
	public Application parent;
}
