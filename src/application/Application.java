package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;
import java.util.stream.Collectors;

import application.DataSource.Size;

public class Application extends Element implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7356002887531213535L;
	
	public static class Statistics {
		public int nbNodes;
		public int nbVariableNodes;
		public float ratioVariableNode;
		public int nbTasks;
		public float ratioTaskNodes;
		public int nbData;
		public float ratioDataNodes;
		public int nbDataSizes;
		public float avgNbDataSizes;
		public float avgDataSizes;
		
		public int nbPaths;
		public int nbInputPaths;
		//public float avgInputs;
		//public int nbOutputPaths;
		//public float avgOutputs;
		//public float ratioVariablePaths;
		public float avgNbSplitPaths;
		public float avgNbJoinPaths;
		
		public int nbLeaf;
		public float avgNbLeaf;
		public int nbOrphan;
		public float avgNbOrphan;
		public float avgDepth;
		
		public boolean satisfyAt(Statistics s, float percent) {
			
			/*System.out.println(Math.abs((s.avgDataSizes / (float)avgDataSizes -  1)) < percent);
			System.out.println(Math.abs((s.avgDepth / (float)avgDepth -  1)) < percent);
			System.out.println(Math.abs((s.avgNbDataSizes / (float)avgNbDataSizes -  1)) < percent);
			System.out.println(Math.abs((s.avgNbJoinPaths / (float)avgNbJoinPaths -  1)) < percent);
			System.out.println(Math.abs((s.avgNbLeaf / (float)avgNbLeaf -  1)) < percent);
			System.out.println(Math.abs((s.avgNbOrphan / (float)avgNbOrphan -  1)) < percent);
			System.out.println(Math.abs((s.avgNbSplitPaths / (float)avgNbSplitPaths -  1)) < percent);
			System.out.println(Math.abs((s.nbData / (float)nbData -  1)) < percent);
			System.out.println(Math.abs((s.nbDataSizes / (float)nbDataSizes -  1)) < percent);
			System.out.println(Math.abs((s.nbInputPaths / (float)nbInputPaths -  1)) < percent);
			System.out.println(Math.abs((s.nbLeaf / (float)nbLeaf -  1)) < percent);
			System.out.println(Math.abs((s.nbNodes / (float)nbNodes -  1)) < percent);
			System.out.println(s.nbNodes +" and ... "+ nbNodes);
			System.out.println(Math.abs((s.nbOrphan / (float)nbOrphan -  1)) < percent);
			System.out.println(Math.abs((s.nbPaths / (float)nbPaths -  1)) < percent);
			System.out.println(Math.abs((s.nbTasks / (float)nbTasks -  1)) < percent);
			System.out.println(Math.abs((s.nbVariableNodes / (float)nbVariableNodes -  1)) < percent);
			System.out.println(Math.abs((s.ratioDataNodes / (float)ratioDataNodes -  1)) < percent);
			System.out.println(Math.abs((s.ratioTaskNodes / (float)ratioTaskNodes -  1)) < percent);
			System.out.println(Math.abs((s.ratioVariableNode / (float)ratioVariableNode -  1)) < percent);*/
			
			System.out.println(Math.max(s.avgDataSizes, avgDataSizes) - Math.min(s.avgDataSizes, avgDataSizes));
			System.out.println(Math.abs((s.avgDepth / (float)avgDepth -  1)));
			System.out.println(Math.abs((s.avgNbDataSizes / (float)avgNbDataSizes -  1)));
			System.out.println(Math.abs((s.avgNbJoinPaths / (float)avgNbJoinPaths -  1)));
			System.out.println(Math.abs((s.avgNbLeaf / (float)avgNbLeaf -  1)));
			System.out.println(Math.abs((s.avgNbOrphan / (float)avgNbOrphan -  1)));
			System.out.println(Math.abs((s.avgNbSplitPaths / (float)avgNbSplitPaths -  1)));
			System.out.println(Math.abs((s.nbData / (float)nbData -  1)));
			System.out.println(Math.abs((s.nbDataSizes / (float)nbDataSizes -  1)));
			System.out.println(Math.abs((s.nbInputPaths / (float)nbInputPaths -  1)));
			System.out.println(Math.abs((s.nbLeaf / (float)nbLeaf -  1)));
			System.out.println(Math.abs((s.nbNodes / (float)nbNodes -  1)));
			System.out.println(s.nbNodes +" and ... "+ nbNodes);
			System.out.println(Math.abs((s.nbOrphan / (float)nbOrphan -  1)));
			System.out.println(Math.abs((s.nbPaths / (float)nbPaths -  1)));
			System.out.println(Math.abs((s.nbTasks / (float)nbTasks -  1)));
			System.out.println(Math.abs((s.nbVariableNodes / (float)nbVariableNodes -  1)));
			System.out.println(Math.abs((s.ratioDataNodes / (float)ratioDataNodes -  1)));
			System.out.println(Math.abs((s.ratioTaskNodes / (float)ratioTaskNodes -  1)));
			System.out.println(Math.abs((s.ratioVariableNode / (float)ratioVariableNode -  1)));
			
			boolean a = Math.abs((s.avgDataSizes / (float)avgDataSizes) - 1) < percent;
			boolean b = Math.abs((s.avgDepth / (float)avgDepth - 1 )) < percent;
			boolean c = Math.abs((s.avgNbDataSizes / (float)avgNbDataSizes - 1 )) < percent;
			boolean d = Math.abs((s.avgNbJoinPaths / (float)avgNbJoinPaths - 1 )) < percent;
			boolean e = Math.abs((s.avgNbLeaf / (float)avgNbLeaf - 1 )) < percent;
			boolean f = Math.abs((s.avgNbOrphan / (float)avgNbOrphan - 1 )) < percent;
			boolean g = Math.abs((s.avgNbSplitPaths / (float)avgNbSplitPaths - 1 )) < percent;
			boolean h = Math.abs((s.nbData / (float)nbData - 1 )) < percent;
			boolean i = Math.abs((s.nbDataSizes / (float)nbDataSizes - 1 )) < percent;
			boolean j = Math.abs((s.nbInputPaths / (float)nbInputPaths - 1 )) < percent;
			boolean k = Math.abs((s.nbLeaf / (float)nbLeaf - 1 )) < percent;
			boolean l = Math.abs((s.nbNodes / (float)nbNodes - 1 )) < percent;
			boolean m = Math.abs((s.nbOrphan / (float)nbOrphan - 1 )) < percent;
			boolean n = Math.abs((s.nbPaths / (float)nbPaths - 1 )) < percent;
			boolean o = Math.abs((s.nbTasks / (float)nbTasks - 1 )) < percent;
			boolean p = Math.abs((s.nbVariableNodes / (float)nbVariableNodes - 1 )) < percent;
			boolean q = Math.abs((s.ratioDataNodes / (float)ratioDataNodes - 1 )) < percent;
			boolean r = Math.abs((s.ratioTaskNodes / (float)ratioTaskNodes - 1 )) < percent;
			boolean u = Math.abs((s.ratioVariableNode / (float)ratioVariableNode - 1 )) < percent;
			
			return a && b && c && d && e && f && g && h && i && j && k && l && m && n && o && p && q && r && u; 
		}
	}
	
	

	public Application(String id) {
		super(id, Type.GROUP);
		elements = new ArrayList<Element>();
		alternatives = new HashMap<Path, Alternatives>();
	}

	public DataSource addDataSource(String id) {
		DataSource res = new DataSource(id);
		elements.add(res);
		return res;
	}
	
	public Task addTask(String id, String function) {
		Task res = new Task(id, function);
		elements.add(res);
		return res;
	}
	
	public Path addPath(String id) {
		Path res = new Path(id);
		elements.add(res);
		return res;
	}
	
	public Collection<? extends Element> getElements(){
		return elements; 
	}
	
	public Application split(Path p, Element e, Element... alt) {
		if(!alternatives.containsKey(p))
			alternatives.put(p, new Alternatives(p));
		
		Collection<Element> res = new ArrayList<Element>();
		e.isOptional = true;
		for(Element elem : alt) {
			elem.isOptional = true;
			res.add(elem);
		}
		
		alternatives.get(p).splits(e, res);
		return this;
		
	}
	
	public Application split(Path p, Element e, Collection<Element> alt) {
		if(!alternatives.containsKey(p))
			alternatives.put(p, new Alternatives(p));
		
		Collection<Element> res = new ArrayList<Element>();
		e.isOptional = true;
		for(Element elem : alt) {
			elem.isOptional = true;
			res.add(elem);
		}
		
		alternatives.get(p).splits(e, res);
		return this;
		
	}
	
	public Application join(Path p, Element e, Element... alt) {
		if(!alternatives.containsKey(p))
			alternatives.put(p, new Alternatives(p));
		
		Collection<Element> res = new ArrayList<Element>();
		for(Element elem : alt) {
			elem.isOptional = true;
			res.add(elem);
		}
		
		alternatives.get(p).joins(e, res);
		return this;
	}
	
	public Application join(Path p, Element e, Collection<Element> alt) {
		if(!alternatives.containsKey(p))
			alternatives.put(p, new Alternatives(p));
		
		Collection<Element> res = new ArrayList<Element>();
		for(Element elem : alt) {
			elem.isOptional = true;
			res.add(elem);
		}
		
		alternatives.get(p).joins(e, res);
		return this;
	}
	
	public Collection<Node> getNodes(){
		Collection<Node> res = new ArrayList<Node>();
		for(Element e : elements)
			if(e.type == Type.NODE)
				res.add((Node)e);
		return res;
	}
	
	public Collection<Path> getPaths(){
		Collection<Path> res = new ArrayList<Path>();
		for(Element e : elements)
			if(e.type == Type.PATH)
				res.add((Path)e);
		return res;
	}
	
	public Collection<DataSource> getDataSources(){
		Collection<DataSource> res = new ArrayList<DataSource>();
		for(Element e : elements)
			if(e.type == Type.NODE && ((Node)e).type == Node.Type.DATA_SOURCE)
				res.add((DataSource)e);
		return res;
	}
	
	public Collection<Task> getTasks(){
		Collection<Task> res = new ArrayList<Task>();
		for(Element e : elements)
			if(e.type == Type.NODE && ((Node)e).type == Node.Type.TASK)
				res.add((Task)e);
		return res;
	}
	
	public Collection<Task> getSortedTasks(){
		Collection<Node> nodes = getSortedApplication().stream().filter(t -> t.type == Node.Type.TASK).collect(Collectors.toList());
		Collection<Task> tasks = new ArrayList<>();
		for(Node n : nodes)
			tasks.add((Task)n);
		return tasks;
	}

	public boolean isVariable(DataSource d) {
		return getOptionalElements().contains(d);
	}
	
	public Collection<Element> getOptionalElements(){
		return elements.stream().filter(e -> e.isOptional).collect(Collectors.toList());
	}
	
	public Collection<Node> getSortedApplication(){
		Collection<Node> nodes = getNodes();
		Deque<Node> result = new LinkedList<>();
		Map<Node, Integer> colors = new HashMap<>();
		
		for(Node n : nodes) colors.put(n, 0);
		
		for(Node n : nodes)
			if(colors.get(n) == 0)
				_topologicalSort(nodes, result, n, colors);
				
		return result;
	}
	
	private void _topologicalSort(Collection<Node> nodes, Deque<Node> result, Node n, Map<Node, Integer> colors) {
		colors.put(n, 1);
		for(Node children : n.getNexts())
			if(colors.get(children) == 0)
				_topologicalSort(nodes, result, children, colors);
		colors.put(n, 2);
		result.addFirst(n);
	}
	
	public void printNodeMatrix() {
		Collection<Node> nodes = getNodes();
		System.out.print("   ");
		for(Node m : nodes) 
			System.out.print(m.id+":");
		System.out.println();
		
		for(Node m : nodes) {
			System.out.print(m.id+": ");
			for(Node n : nodes) {
				System.out.print(m.isConnectedTo(n)+" ");
			}
			System.out.println();
		}
	}
	
	public void print() {
		Collection<Node> nodes = getNodes();
		System.out.print("\t");
		for(Node m : nodes) 
			System.out.print(m.id+":\t");
		System.out.println();
		
		for(Node m : nodes) {
			Collection<Path> paths = new ArrayList<>();
			for(Node.Argument a : m.outputs.values())
				paths.add(a.path);
			
			System.out.print(m.id+":\t");
			for(Node n : nodes) {
				Path connect = null;
				for(Path p : paths)
					for(Node o : p.outputs)
						if(o == n)
							connect = p;
				if(connect != null && m != n)
					System.out.print(connect.id+"\t");
				else
					System.out.print("o\t");
			}
			System.out.println();
		}
	}
	
public Statistics statistics() {
		
		Statistics s = new Statistics();
		
		s.nbNodes = getNodes().size();
		s.nbTasks = getTasks().size();
		s.ratioTaskNodes = s.nbNodes/(float)s.nbTasks;
		s.nbData = getDataSources().size();
		s.ratioDataNodes = s.nbNodes/(float)s.nbData;
		
		for(Task t : getTasks())
			if(t.isOptional)
				s.nbVariableNodes++;
		for(DataSource d : getDataSources())
			if(d.isOptional)
				s.nbVariableNodes++;
		
		s.ratioVariableNode = s.nbNodes/(float)s.nbVariableNodes;
		
		float accDataSizes = 0;
		for(DataSource d : getDataSources()) {
			int sizes = 0;
			for(Size ds : d.sizes) {
				sizes += ds.value;
			}
			accDataSizes += sizes/(float)d.sizes.size();
			s.nbDataSizes += d.sizes.size();
		}
		s.avgDataSizes = accDataSizes / s.nbData;
		s.avgNbDataSizes = s.nbDataSizes / s.nbData;
		
		s.nbPaths = getPaths().size();
		int nbSplit = 0;
		int nbJoin = 0;
		for(Path p : getPaths()) {
			if(p.inputs.size() > 1)
				nbJoin++;
			if(p.outputs.size() > 1)
				nbSplit++;
		}
		s.avgNbSplitPaths = nbSplit / s.nbPaths;
		s.avgNbJoinPaths = nbJoin / s.nbPaths;
		
		Collection<Node> nodes = getSortedApplication();
		for(Node n : nodes) {
			int depth = 0;
			for(Node c : n.getNexts())
				depth += _computeTotalDepth(c, depth+1);
		}
		
		s.avgDepth /= s.nbNodes;
		
		for(Node n : nodes) {
			if(n.inputs.isEmpty())
				s.nbOrphan++;
			if(n.outputs.isEmpty())
				s.nbLeaf++;
		}
		
		s.avgNbOrphan = s.nbOrphan / s.nbNodes;
		s.avgNbLeaf = s.nbLeaf / s.nbNodes;
		
		return s;
	}
	
	
	
	private int _computeTotalDepth(Node c, int i) {
		int acc = 0;
		for(Node cc : c.getNexts())
			acc += _computeTotalDepth(cc, i+1);
		return acc+i;
	}
	
	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Collection<Element> elements;
	public Map<Path, Alternatives> alternatives;
}
