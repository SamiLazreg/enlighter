package application;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import deployment.MathUtilities;

public class Alternatives implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1937156960071688730L;
	public Alternatives(Path path) {
		this.path = path;
		splits = new HashMap<Element, Collection<Element>>();
		joins = new HashMap<Element, Collection<Element>>();
	}
	
	public Collection<Element> getOptionalElements(){
		Collection<Element> res = new HashSet<Element>();
		for(Map.Entry<Element, Collection<Element>> split : splits.entrySet()) {
			res.addAll(split.getValue());
		}
		for(Map.Entry<Element, Collection<Element>> join : joins.entrySet()) {
			res.addAll(join.getValue());
		}
		return res;
	}
	
	public void splits(Element e, Collection<? extends Element> elements) {
		if(!splits.containsKey(e))
			splits.put(e, new HashSet<Element>());
		splits.get(e).addAll(elements);
	}
	
	public void joins(Element e, Collection<? extends Element> elements) {
		if(!joins.containsKey(e))
			joins.put(e, new HashSet<Element>());
		joins.get(e).addAll(elements);
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public Path path;
	public Map<Element, Collection<Element>> splits;
	public Map<Element, Collection<Element>> joins;
}
