package application;

import java.io.Serializable;

public class Task extends Node implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4968058281383664887L;

	public Task(String id, String function) {
		super(id, Node.Type.TASK);
		this.function = function;
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}
	
	public String function;
}
