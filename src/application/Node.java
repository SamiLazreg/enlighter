package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class Node extends Element implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -433969933899261244L;
	public enum Type{
		DATA_SOURCE,
		DATA_SINK,
		TASK;
	}
	
	public class Argument implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -2385463842127024260L;
		public Argument(String id, Path path) {
			this.id = id;
			this.path = path;
		}
		
		public String id;
		public Path path;
	}
	
	public Node(String id, Type type) {
		super(id, Element.Type.NODE);
		this.type = type;
		inputs = new HashMap<String, Argument>();
		outputs = new HashMap<String, Argument>();
	}
	
	public Node connect(Path path, String argument) {
		if(!inputs.containsKey(argument)) {
			inputs.put(argument, new Argument(argument, path));
		}
		else {
			inputs.get(argument).path.outputs.remove(this);
			inputs.get(argument).path = path;
		}
		path.outputs.add(this);
		return this;
	}
	
	public Node connect(String argument, Path path) {
		if(!outputs.containsKey(argument)) {
			outputs.put(argument, new Argument(argument, path));
		}
		else {
			outputs.get(argument).path.inputs.remove(this);
			outputs.get(argument).path = path;
		}
		path.inputs.add(this);
		return this;
	}
	
	public Collection<Node> getNexts(){
		Collection<Node> res = new ArrayList<>();
		for(Argument a : outputs.values()) {
			res.addAll(a.path.outputs);
		}
		return res;
	}
	
	public boolean isConnectedTo(Node n) {
		return getNexts().contains(n);
	}
	
	public int countConnectedTo(Node n) {
		Collection<Node> res = getNexts();
		if(!isConnectedTo(n))
			return 0;
		int count = 0;
		for(Node r : res) {
			if(r.equals(this))
				++count;
		}
		return count;
		
	}
	
	public Type type;
	
	public Map<String, Argument> inputs;
	public Map<String, Argument> outputs;
}
